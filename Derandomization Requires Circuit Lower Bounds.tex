\documentclass[aspectratio=169]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}


%\usepackage{beamerthemesplit}
%\usepackage[orientation=landscape,size=custom,width=16,height=9,scale=0.5,debug]{beamerposter}
%\usetheme{Antibes}
\usecolortheme{spruce}

\newcommand{\ma}[1]{\ensuremath{#1}}

\newcommand{\T}{\ma{\mathsf{T}}}

\newcommand{\bra}[1]{\langle #1\rangle}
\newcommand{\sqbra}[1]{[ #1]}
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\eps}{\varepsilon}
\newcommand<>{\red}[1]{{\color#2{red} #1}}
\newcommand<>{\blue}[1]{{\color#2{blue} #1}}
\newcommand<>{\green}[1]{{\color#2[rgb]{0,0.5,0} #1}}

\newcommand{\PP}{\mathbf{P_{/poly}}}
\newcommand{\AlgPP}{\mathbf{AlgP_{/poly}}}
\newcommand{\EXP}{\mathbf{EXP}}
\newcommand{\NEXP}{\mathbf{NEXP}}

\newcommand{\ft}[1]{\frametitle{\textsc{#1}}}
\newcommand{\fst}[1]{\framesubtitle{\textsc{#1}}}

\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}
\newtheorem{remark}{Remark}

\defbeamertemplate*{title page}{customized}[1][]
{
  \usebeamerfont{title}\inserttitle\par
  \bigskip
  \usebeamerfont{subtitle}\insertsubtitle\par
  \bigskip
  \bigskip
  \usebeamerfont{author}\blue{\insertauthor}\par
  \usebeamerfont{institute}\blue{\insertinstitute}\par
  \usebeamerfont{date}\blue{\insertdate}\par
}
\beamertemplatenavigationsymbolsempty

\title{Derandomization Requires Circuit Lower Bounds}
\author{Talk given by Xuangui Huang}
\date{March 25, 2015}

\begin{document}
\frame{\titlepage}

\begin{frame}
	\ft{Derandomization Requires Circuit Lower Bounds}
	\begin{itemize}
		\item In the last talk it is shown that circuit lower bounds imply derandomization.
		\only<2->{
		\item In this talk we show that derandomization implies lower bounds:
		\begin{itemize}
			\item Proving a specific problem from $\mathbf{BPP}$ to be in $\mathbf{P}$ will imply \red{super-polynomial} lower bounds for either \blue{Boolean} or \blue{algebraic circuits}.
		\end{itemize}
		}
	\end{itemize}
	\only<1>{	
	\begin{theorem}[20.7]
		\begin{enumerate}
			\item If there exists $f \in \mathbf{DTIME}(2^{O(n)})$ and $c \in \mathbb{N^+}$ such that $\mathtt{H_{wrs}}(f) \geq 2^{\frac{n}{c}}$, then $\mathbf{BPP} = \mathbf{P}$.
			\item If there exists $f \in \mathbf{DTIME}(2^{O(n)})$ and $c \in \mathbb{N^+}$ such that $\mathtt{H_{wrs}}(f) \geq 2^{n^{\frac{1}{c}}}$, then $\mathbf{BPP} \subseteq \mathbf{QuasiP} = \mathbf{DTIME}(2^{polylog(n)})$.
			\item If there exists $f \in \mathbf{DTIME}(2^{O(n)})$ and $\mathtt{H_{wrs}}(f) \geq n^{{\omega(1)}}$, then $\mathbf{BPP} \subseteq \mathbf{SUBEXP} = \cap_{\epsilon > 0} \mathbf{DTIME}(2^{n^{\epsilon}})$.
		\end{enumerate}
	\end{theorem}
	}
	\only<2->{
	\begin{theorem}[20.17]
		If $\mathtt{ZEROP} \in \mathbf{P}$ then either $\mathbf{NEXP} \not\subseteq \mathbf{P_{/poly}}$ or $\mathtt{perm} \not\in \mathbf{AlgP_{/poly}}$.
	\end{theorem}
	}
	\only<3->{
	\begin{itemize}
		\item V. Kabanets, R. Impagliazzo, \textit{Derandomizing polynomial identity tests means proving circuit lower bounds}, STOC '03.
		\item The above theorem is known to be true even if its hypothesis is relaxed to $\mathtt{ZERO} \in \cap_{\delta > 0}\mathbf{NTIME}(2^{n^{\delta}})$.
	\end{itemize}
	}
\end{frame}

\begin{frame}
	\ft{Preliminaries}
	Algebraic circuits are analogues of Boolean circuits over a field $\mathbb{F}$, which will be $\mathbb{Z}$ here, using \red{$+$, $-$ and $\times$ gates}.
	\begin{definition}
		The class $\mathbf{AlgP_{/poly}}$ contains all \blue{polynomially bounded degree families} of polynomials that are computable by algebraic circuits (using $+$, $-$ and $\times$ gates) with \red{polynomial size} and \red{polynomial degree}.
	\end{definition}

	\pause
	\begin{definition}[Polynomial Identity Testing]
		$\mathtt{ZEROP}$ is the set of algebraic circuits that compute the zero polynomial.
	\end{definition}
	\pause
	\begin{itemize}
		\item By Schwartz-Zippel Lemma and ``fingerprinting'' (hashing) we can prove $\mathtt{ZEROP} \in \mathbf{BPP}$.
	\end{itemize}
\end{frame}

\begin{frame}
	\ft{Derandomization Requires Circuit Lower Bounds}
	\begin{theorem}[20.17]
		If $\mathtt{ZEROP} \in \mathbf{P}$ then either $\mathbf{NEXP} \not\subseteq \mathbf{P_{/poly}}$ or $\mathtt{perm} \not\in \mathbf{AlgP_{/poly}}$.
	\end{theorem}
	\pause
	\begin{lemma}[20.18]
		If $\EXP \subseteq \PP$ then $\EXP = \mathbf{MA}$.
	\end{lemma}
	\begin{lemma}[20.19]
		If $\mathtt{ZEROP} \in \mathbf{P}$ and $\mathtt{perm} \in \mathbf{AlgP_{/poly}}$ then $\mathbf{P}^{\mathtt{perm}} \subseteq \mathbf{NP}$. 
	\end{lemma}
	\begin{lemma}[20.20]
		If $\NEXP \subseteq \PP$ then $\NEXP = \EXP$.
	\end{lemma}
\end{frame}

\begin{frame}
	\begin{lemma}[20.18]
		If $\EXP \subseteq \PP$ then $\EXP = \mathbf{MA}$.
	\end{lemma}
	\pause
	\begin{proof}
		\begin{itemize}
			\item Suppose $\EXP \subseteq \PP$, then by Meyer's Theorem $\EXP = \mathbf{\Sigma}^p_2$.
			\only<3>{
			\begin{itemize}
				\item Suppose $L$ is computed by a $2^{\green{p(n)}}$-time-oblivious \green{$k$}-tape Turing Machine $M$.
				\item Calculating $z_i$ from $i$ is in $\EXP$, hence there is a \red{polynomial-size} (say, \green{$q(n)$}) circuit $C$ that computes $(i,z_i)$ from $i$.
				\item $x \in L$ iff
				\[\exists C \in \{0,1\}^{\green{q(n)}} \forall i, i_1, \cdots, i_k \in \{0,1\}^{\green{p(n)}} T(x,C(i), C(i_1), \cdots, C(i_k)) = 1.\]
			\end{itemize}
			}
			\item<4-> Therefore $\mathbf{\Sigma}^p_2 = \mathbf{PH} = \mathbf{PSPACE} = \mathbf{IP} = \EXP \subseteq \PP$.
			\item<5-> It is easy to see that $\mathbf{MA} \subseteq \mathbf{EXP}$.
			\item<6-> For every $L \in \EXP$ has an interactive proof, and we can just use the \blue{interactive proof} for $\mathtt{TQBF}$, \only<7->{for which the \red{prover} can be replaced by a \red{polynomial size circuit family $\{C_n\}$}.}
			\item<8-> The interactive proof can be carried out in one round:
		    \only<8->{
			\begin{itemize}
			    \item Given an input $x$ of length $n$, \green{\texttt{Merlin}} will send \green{\texttt{Arthur}} $C_n$.
			    \item \green{\texttt{Arthur}} simulates the original interactive proof for $L$.
			\end{itemize}
			}
			\item<8-> If $x \not\in L$, \red{no} prover described by a polynomial size circuit $C$ has a decent chance to convince the verifier.
		\end{itemize}
	\end{proof}
\end{frame}

\begin{frame}
	\begin{lemma}[20.19]
		If $\mathtt{ZEROP} \in \mathbf{P}$ and $\mathtt{perm} \in \mathbf{AlgP_{/poly}}$ then $\mathbf{P}^{\mathtt{perm}} \subseteq \mathbf{NP}$. 
	\end{lemma}
	\pause
	\begin{proof}
		\begin{itemize}
		    \item Suppose $\mathtt{ZEROP}$ has a polynomial-time algorithm $T$ and $\mathtt{perm}$ has algebraic circuits of \red{size $n^c$}.
		    \item Let $L$ be a language decided by an \red{$n^d$}-time TM $M$ using $\mathtt{perm}$ as oracle.
		    \item<2-> We can construct an $\mathbf{NP}$ machine $N$ for $L$ as follow:
		    \only<3->{
		    \begin{itemize}
		        \item Given input $x$ of length $n$, $M$ makes queries to $\mathtt{perm}$ of \red{size} at most \red{$m =n^d$}.
		        \item So $N$ will guess a sequence of $m$ \blue{algebraic circuits $C_1, \cdots, C_m$}, where $C_i$ has \red{size $i^c$}.
		        \item<4-> $N$ will check \red{inductively} that \red{$C_i$ computes $\mathtt{perm}_i$}, using \blue{downward self-reducibility} and $T$.
		        \item<5-> Then $N$ simulate $M^{\mathtt{perm}}$ using these circuits.
		    \end{itemize}
		    }
		\end{itemize}
	\end{proof}
\end{frame}

\begin{frame}
	\begin{lemma}[20.20]
		If $\NEXP \subseteq \PP$ then $\NEXP = \EXP$.
	\end{lemma}
	\pause
	\begin{proof}
		\begin{itemize}
			\item For the sake of contradiction, assume that $\EXP \subsetneqq \NEXP$ and $\NEXP \subseteq \PP$.
			\item Let $L \in \NEXP \setminus \EXP$, hence there exists a \green{constant $c>0$} and a \green{$2^{|x|^{10c}}$}-time computable relation $R$ such that
			\[x \in L \Leftrightarrow \exists y \in \red{\{0,1\}^{2^{|x|^c}}} R(x,y) \text{ holds}.\]
			\only<3>{
			\item The main idea here is the \blue{``easy witness''} method:
			\begin{itemize}
				\item Unless the input $x$ has a \blue{easy witness} $y$, any witness for $x$ can be used for derandomization.
				\item \blue{``easy''} means it can be computed by a \red{small circuit}.
			\end{itemize}
			}
		\end{itemize}
	\end{proof}
\end{frame}

\begin{frame}
	\begin{proof}
		\begin{itemize}
			\item For the sake of contradiction, assume that $\EXP \subsetneqq \NEXP$ and $\NEXP \subseteq \PP$.
			\only<1-10>{
			\item Let $L \in \NEXP \setminus \EXP$, hence there exists a \green{constant $c>0$} and a \green{$2^{|x|^{10c}}$}-time computable relation $R$ such that
			$x \in L \Leftrightarrow \exists y \in \red{\{0,1\}^{2^{|x|^c}}} R(x,y) \text{ holds}$.
			}
			\only<1-6>{
			\item For \red{every constant $D > 0$}, let $M_D$ be the following machine trying to decide $L$:
			\begin{itemize}
				\item Given input $x$ of length $n$, enumerate all possible Boolean circuits $C$ of \green{size $n^{100D}$} that take \red{$n^c$ inputs} and one output.
				\begin{itemize}
					\item<2-> Halt and output $1$ if $R(x, \mathtt{tt}(C))$ holds, where $\mathtt{tt}(C)$ is the \red{truth table} of the boolean function computed by $C$.
				\end{itemize}
				\item<2-> Output $0$ otherwise.
			\end{itemize} 
			\item<3-> $M_D$ runs in exponential time, \only<4->{hence there exists an \red{infinite sequence} of inputs \blue{$\chi_D = \{x_i\}_{i \in \mathbb{N}}$} on which $M_D(x_i)$ outputs $0$ but $x_i \in L$.
			\only<4>{
			\begin{itemize}
				\item Note that $M_D$ can only make one-sided errors.
			\end{itemize}
			}
			}
			\item<5-> This means for every $x \in \chi_D$, for every $y$ such that $R(x,y)$ holds, $y$ represents a \red{function on $n^c$ bits} that cannot be computed by \red{$n^{100D}$-size circuits}.
			\item<6-> We can use such $y$ to obtain an \red{$\ell^D$}-pseudorandom generator.
			}
			\only<7-10>{
			\item<7-> For \red{every constant $D > 0$} there exists an \red{infinite sequence} of inputs \blue{$\chi_D = \{x_i\}_{i \in \mathbb{N}}$}, we can use it to get $y$ then obtain an \red{$\ell^D$}-pseudorandom generator.
			\item<8-> Every language $L' \in \EXP$ has a proof system where \texttt{Merlin} sends a proof then \texttt{Arthur} verifies using a \blue{probabilistic algorithm} of at most \green{$n^{D'}$} steps for \green{some constant $D'$}.
			\only<8>{
				\begin{lemma}[20.18]
					If $\EXP \subseteq \PP$ then $\EXP = \mathbf{MA}$.
				\end{lemma}
			}
			\only<9->{
			\begin{itemize}
				\item For $x' \in \chi_{D'}$ where $x'$ has the same \red{length} as input $x$, we can replace \texttt{Arthur} by \red{nondeterministic $poly(n^D)2^{n^{10c}}$ time} algorithm without tossing coins:
				\item[] \texttt{Arthur} will \red{guess} a string $y$ such that $R(x',y)$ holds and then use it to derandomize. 
				%\item $x'$ can be supplied by allowing \red{$n$ bits of advice}.
			\end{itemize}
			}
			\item<10-> There is a \red{constant $c$} such that \red{every} language in $\EXP$ can be decided on \blue{infinitely many} input \red{lengths} by an $\mathbf{NTIME}(2^{n^c})$ time algorithm using \red{$n$ bits of advice} (to supply such $x'$).
			}
			\only<11->{
			\item<11-> There is a \red{constant $c$} such that \red{every} language in $\EXP$ can be decided on \blue{infinitely many} inputs \red{lengths} by an $\mathbf{NTIME}(2^{n^c})$ time algorithm using \red{$n$ bits of advice}.
			\item<12-> Then by using an \blue{universial Nondeterministic TM},  we can show that there is a \red{constant $c'$} such that \red{every} language in $\EXP$ can be decided on \blue{infinitely many} inputs \red{lengths} by a size $n^{c'}$ circuit family.
			\item<13-> But using \blue{standard diagonalization} we can come up with a language in $\mathbf{DTIME}(2^{O(n^{c'})}) \subseteq \EXP$ that cannot be computed by such a circuit family on \red{almost every input lengths}.
			\only<13>{
			\begin{itemize}
				\item By counting argument, for $n$ sufficiently larger there are Boolean functions on $n$ bits computable by \red{$2n^{c'}$}-sized circuits but not computable by $n^{c'}$-sized circuits.
				\item Just brute-force seach for the first \red{$2n^{c'}$-sized} circuit on $n$ bits that is not equivalent to any $n^{c'}$-sized circuits, then evaluate input $x$ on that circuit.
			\end{itemize}
			}
			}
		\end{itemize}
	\end{proof}
\end{frame}

\begin{frame}
	\ft{Derandomization Requires Circuit Lower Bounds}
	\begin{lemma}[20.18]
		If $\EXP \subseteq \PP$ then $\EXP = \mathbf{MA}$.
	\end{lemma}
	\begin{lemma}[20.19]
		If $\mathtt{ZEROP} \in \mathbf{P}$ and $\mathtt{perm} \in \mathbf{AlgP_{/poly}}$ then $\mathbf{P}^{\mathtt{perm}} \subseteq \mathbf{NP}$. 
	\end{lemma}
	\begin{lemma}[20.20]
		If $\NEXP \subseteq \PP$ then $\NEXP = \EXP$.
	\end{lemma}
    \begin{theorem}[20.17]
		If $\mathtt{ZEROP} \in \mathbf{P}$ then either $\mathbf{NEXP} \not\subseteq \mathbf{P_{/poly}}$ or $\mathtt{perm} \not\in \mathbf{AlgP_{/poly}}$.
	\end{theorem}
\end{frame}

\begin{frame}
	\ft{Derandomization Requires Circuit Lower Bounds}
    \begin{theorem}[20.17]
		If $\mathtt{ZEROP} \in \mathbf{P}$ then either $\mathbf{NEXP} \not\subseteq \mathbf{P_{/poly}}$ or $\mathtt{perm} \not\in \mathbf{AlgP_{/poly}}$.
	\end{theorem}
	\begin{proof}
	\begin{itemize}
	    \item For the sake of contradiction, assume $\mathtt{ZEROP} \in \mathbf{P}$, $\mathbf{NEXP} \subseteq \mathbf{P_{/poly}}$ and $\mathtt{perm} \in \mathbf{AlgP_{/poly}}$.
	    \only<2>{
	    \item Therefore $\NEXP = \EXP = \mathbf{MA}$.
	    \begin{lemma}[20.18]
		    If $\EXP \subseteq \PP$ then $\EXP = \mathbf{MA}$.
	    \end{lemma}
		\begin{lemma}[20.20]
		    If $\NEXP \subseteq \PP$ then $\NEXP = \EXP$.
	    \end{lemma}
	    }
	    \only<3>{\item Therefore $\NEXP = \EXP = \mathbf{MA}~\red{= \mathbf{PH}}$.}
	    \only<4>{\item Therefore $\NEXP = \EXP = \mathbf{MA}  = \mathbf{PH}~\red{\subseteq \mathbf{P}^{\mathtt{perm}}}$.}
	    \only<5>{\item Therefore $\NEXP = \EXP = \mathbf{MA}  = \mathbf{PH} \subseteq \mathbf{P}^{\mathtt{perm}}~\red{\subseteq \mathbf{NP}}$, contradicting the \blue{Nondeterministic Time Hierarchy Theorem}.
	    \begin{lemma}[20.19]
		If $\mathtt{ZEROP} \in \mathbf{P}$ and $\mathtt{perm} \in \mathbf{AlgP_{/poly}}$ then $\mathbf{P}^{\mathtt{perm}} \subseteq \mathbf{NP}$. 
	    \end{lemma}
	    }
	\end{itemize}
	\end{proof}
\end{frame}

\begin{frame}
\centering
\Large
\textsc{Thank You!} \\
\textsc{Q \& A}
\end{frame}
\end{document}

