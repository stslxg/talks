\documentclass{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}


%\usepackage{beamerthemesplit}
%\usepackage[orientation=landscape,size=custom,width=16,height=9,scale=0.5,debug]{beamerposter}
%\usetheme{Antibes}
\usecolortheme{spruce}

\newcommand{\ma}[1]{\ensuremath{#1}}

\newcommand{\T}{\ma{\mathsf{T}}}

\newcommand{\bra}[1]{\langle #1\rangle}
\newcommand{\sqbra}[1]{[ #1]}
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\eps}{\varepsilon}
\newcommand<>{\red}[1]{{\color#2{red} #1}}
\newcommand<>{\blue}[1]{{\color#2{blue} #1}}
\newcommand<>{\green}[1]{{\color#2[rgb]{0,0.5,0} #1}}
\renewcommand{\aa}{\mathbf{a}}

\newcommand{\PP}{\mathbf{P_{/poly}}}
\newcommand{\AlgPP}{\mathbf{AlgP_{/poly}}}
\newcommand{\EXP}{\mathbf{EXP}}
\newcommand{\NEXP}{\mathbf{NEXP}}

\newcommand{\ft}[1]{\frametitle{\textsc{#1}}}
\newcommand{\fst}[1]{\framesubtitle{\textsc{#1}}}

\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}
\newtheorem{remark}{Remark}
%\newtheorem{example}{Example}


\beamertemplatenavigationsymbolsempty

\title{Multiparty Communication Complexity}
\author{Talk given by Xuangui Huang}
\date{March 26, 2015}

\begin{document}
\frame{\titlepage}

\begin{frame}
    \ft{Who Am I?}
    \centering
    \includegraphics[scale=0.2]{Multiparty-1.png}<1>
    \includegraphics[scale=0.16]{Multiparty-2.png}<2>
    \includegraphics[scale=0.2]{Multiparty-3.png}<3>
    \includegraphics[scale=0.18]{Multiparty-4.png}<4>
\end{frame}

\begin{frame}
    \ft{Multiparty Communication Complexity}
    \fst{Number On The Forehead}
    \begin{itemize}
        \item There are \red{$k$ players} and \red{$k$} strings of \red{length $n$} $x_1, \cdots, x_k$.
        \item Player $i$ gets all strings except $x_i$.
        \item They want to compute $f(x_1,\cdots, x_k)$ for a \red{fixed} $f$.
        \pause
        \item[]
        \item They have an \red{agreed-upon protocol}.
        \item All their communication is posted on a \red{``public blackboard''}.
        \item The last value sent should be $f(x_1,\cdots, x_k)$.
        \pause
        \item[]
        \item Denote by $C_k(f)$ the number of bits sent using the \blue{best} protocol.
    \end{itemize}
    \pause
    \begin{claim}
        $C_k(f) \leq n+1$.
    \end{claim}
\end{frame}

\begin{frame}
    \ft{Multiparty Communication Complexity}
    \fst{Number On The Forehead}
    \begin{example}[Parity of Majority Function]
        \[f(x_1, x_2, x_3) = \bigoplus_{i = 1}^n \text{Maj}(x_{1i}, x_{2i}, x_{3i}) .\]
    \end{example}
    \pause
    \begin{itemize}
        \item $C_3(f) = 3$.
        \pause
        \begin{itemize}
            \item Each player count the number of \blue{columns} that she can \red{determine} that the majority \red{is $1$}, and send the parity of this number.
            \item The final answer is the parity of the three player's bits.
        \end{itemize}
        \pause
        \item The majority of each \blue{column} is known by either \red{one or three} players, which is an \red{odd} number.
    \end{itemize}
\end{frame}

\begin{frame}
    \ft{Multiparty Communication Complexity}
    \fst{Number On The Forehead}
    \begin{example}[Generalized Inner Product]
        \[GIP_{k,n}(x_1, \cdots, x_k) = \bigoplus_{i=1}^n \bigwedge_{j=1}^k x_{ji} .\]
    \end{example}
    \pause
    \begin{example}[Generalized Inner Product]
        \[GIP_{k,n}(x_1, \cdots, x_k) = (-1)^{\sum_{i=1}^n \prod_{j=1}^k x_{ji}} .\] 
    \end{example}
\end{frame}

\begin{frame}
    \ft{Lower Bound Techniques for Multiparty CC}
    \begin{itemize}
        \item \blue{monochromatic cylinder intersection} partition number $\chi_k(f)$.
        \item \blue{$k$-party discrepancy} $Disc(f)$. 
    \end{itemize}
    \pause
    \begin{definition}[The Upper Bound for Discrepancy]
        Let a \blue{$(k,n)$-cube} be a suber $D$ of the form $\{a_1, a_1'\} \times \cdots \times \{a_k, a_k'\}$, where each $a_i, a_i' \in \{0,1\}^n$.  Then define 
        \[ \mathcal{E}(f) = \mathbf{E}_{D \text{ is }(k,n)\text{-cube}}[\prod_{\aa \in D} f(\aa)].\] 
    \end{definition}
\end{frame}

\begin{frame}
    \ft{Lower Bound Techniques for Multiparty CC}
    \begin{theorem}
        $C_k(f) \geq \log_2 \chi_k(f)$.
    \end{theorem}
    \begin{theorem}
        $\chi_k(f) \geq \frac{1}{Disc(f)}$.
    \end{theorem}
    \begin{theorem}
        $Disc(f) \leq (\mathcal{E}(f))^{\frac{1}{2^k}}$.
    \end{theorem}
\end{frame}

\begin{frame}
    \begin{theorem}
        $C_k(GIP_{k,n}) = \Omega(\frac{n}{4^k})$.
    \end{theorem}
    \begin{proof}
        \begin{itemize}
            \item<2-> $ GIP_{k,n}(x_1, \cdots, x_k) = \prod_{i=1}^n GIP_{k,1}(x_{1,i}, \cdots, x_{k,i})$.
            where we define $GIP_{k,1}(x_1, \cdots, x_k) = (-1)^{\prod_{j=1}^k x_i}$.
            \only<2>{
            \begin{example}[Generalized Inner Product]
                \[GIP_{k,n}(x_1, \cdots, x_k) = (-1)^{\sum_{i=1}^n \prod_{j=1}^k x_{ji}} .\] 
            \end{example}    
            } 
            \only<3>{
            \item Thus $\mathcal{E}(GIP_{k,n}) = \mathbf{E}_{D \text{ is }(k,n)\text{-cube}}[\prod_{\aa \in D} \prod_{i = 1}^n GIP_{k,1}(\aa_i)]$.
            \begin{definition}[The Upper Bound for Discrepancy]
            Let a \blue{$(k,n)$-cube} be a suber $D$ of the form $\{a_1, a_1'\} \times \cdots \times \{a_k, a_k'\}$, where each $a_i, a_i' \in \{0,1\}^n$.  Then define 
            \[ \mathcal{E}(f) = \mathbf{E}_{D \text{ is }(k,n)\text{-cube}}[\prod_{\aa \in D} f(\aa)].\] 
            \end{definition}
            }
            \only<4>{
            \item Thus $\mathcal{E}(GIP_{k,n}) = \red{\prod_{i = 1}^n} \mathbf{E}_{C \text{ is }(k,\red{1})\text{-cube}}[\prod_{\aa \in C}  GIP_{k,1}(\aa)] = \red{(\mathcal{E}(GIP_{k,1}))^n}$.
            }
            \only<5->{
            \item Thus $\mathcal{E}(GIP_{k,n}) = \prod_{i = 1}^n \mathbf{E}_{C \text{ is }(k,1)\text{-cube}}[\prod_{\aa \in C}  GIP_{k,1}(\aa)] = (\mathcal{E}(GIP_{k,1}))^n$.
            \only<6-7>{
            \item $\mathcal{E}(GIP_{k,1}) = \mathbf{E}_C[\prod_{\aa \in C} GIP_{k,1}(\aa)]$.
            	\begin{itemize}
            		\item Consider the following $(k,1)$-cube $C'$ in which for every $i$ $(a_i,a_i')$ is either \red{$(0,1)$ or $(1,0)$}.
            		\item $\Pr_C[C'] = \red{2^{-k}}$.
            		\item<7-> Only the all one vector $\aa$ makes $GIP_{k,1}(\aa) = -1$, all other vector $\mathbf{c} \in C'$ makes $GIP_{k,1}(\aa) = 1$.
            		\item<7-> Hence $\prod_{\aa \in C} GIP_{k,1}(\aa) = \red{-1}$
            	\end{itemize}
            }}
            \only<8->{
            \item $\mathcal{E}(GIP_{k,1}) = \mathbf{E}_C[\prod_{\aa \in C} GIP_{k,1}(\aa)] \leq \red{2^{-k}}\cdot\red{-1} + (1-2^{-k}) \cdot 1 \leq \blue{1 - 2^{-k}} $.
            \item<9-> Hence $\mathcal{E}(GIP_{k,n}) \leq (1-2^{-k})^n \leq e^{-\frac{n}{2^k}} = 2^{-\Omega(\frac{n}{2^k})}$.
            \item<10-> Therefore $Disc(GIP_{k,n}) \leq 2^{-\Omega(\frac{n}{4^k})}$, $C_k(GIP_{k,n}) = \Omega(\frac{n}{4^k})$.
            }
        \end{itemize}
    \end{proof}
\end{frame}

\begin{frame}
\centering
\Large
\textsc{Thank You!} \\
\textsc{Q \& A}
\end{frame}
\end{document}

