\documentclass[aspectratio=169]{beamer}
%\usepackage[utf8]{inputenc}

%\usepackage{concrete} % for the Concrete Math text fonts
\usepackage{euler}
\usepackage{beton}
\renewcommand\familydefault{ccr}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}
\usepackage{multirow}

%\usepackage{beamerthemesplit}
%\usepackage[orientation=landscape,size=custom,width=16,height=9,scale=0.5,debug]{beamerposter}
%\usetheme{Antibes}
\usecolortheme{spruce}

\newcommand{\ma}[1]{\ensuremath{#1}}

\newcommand{\T}{\ma{\mathsf{T}}}

\newcommand{\bra}[1]{\langle #1\rangle}
\newcommand{\sqbra}[1]{[ #1]}
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\eps}{\varepsilon}
\newcommand<>{\red}[1]{{\color#2{red} #1}}
\newcommand<>{\blue}[1]{{\color#2{blue} #1}}
\newcommand<>{\green}[1]{{\color#2[rgb]{0,0.5,0} #1}}

\newcommand{\PP}{\mathbf{P_{/poly}}}
\newcommand{\AlgPP}{\mathbf{AlgP_{/poly}}}
\newcommand{\EXP}{\mathbf{EXP}}
\newcommand{\NEXP}{\mathbf{NEXP}}

\newcommand{\ft}[1]{\frametitle{\textsc{#1}}}
\newcommand{\fst}[1]{\framesubtitle{\textsc{#1}}}

\newcommand{\cA}{\mathcal{A}}
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cG}{\mathcal{G}}
\newcommand{\cU}{\mathcal{U}}
\newcommand{\cE}{\mathcal{E}}
\newcommand{\cF}{\mathcal{F}}
\newcommand{\bZ}{\mathbb{Z}}

\newcommand{\Hol}[1]{\mathrm{Holant}(#1)}
\newcommand{\Pef}[1]{\mathrm{PerfMatch}(#1)}

\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}
\newtheorem{remark}{Remark}
\newtheorem{observation}{Observation}
\newtheorem{conjecture}{Conjecture}

\newcommand{\tuple}[1]{\left(#1\right)}
\newcommand{\tp}{\tuple}
\newcommand{\sharpP}{\#\mathbf{P}} \renewcommand{\P}{\mathbf{P}}

\def\*#1{\mathbf{#1}}
\def\+#1{\mathcal{#1}}
\def\-#1{\mathbb{#1}}

\usepackage{xifthen}

\renewcommand{\Pr}[2][]{ \ifthenelse{\isempty{#1}}
  {\mathbf{Pr}\left[#2\right]} {\mathop{\mathbf{Pr}}_{#1}\left[#2\right]} }
\newcommand{\E}[2][]{ \ifthenelse{\isempty{#1}}
  {\mathbb{E}\left[#2\right]}
  {\mathbb{E}_{#1}\left[#2\right]} }
\newcommand{\Var}[2][]{ \ifthenelse{\isempty{#1}}
  {\mathbf{Var}\left[#2\right]}
  {\mathbf{Var}_{#1}\left[#2\right]} }

\newcommand{\nc}{\newcommand}
\nc{\rnc}{\renewcommand}
\nc{\nev}{\newenvironment}


\newlength{\probwidth}
\setlength{\probwidth}{4cm}


\nc{\nprob}[4][7]{
\begin{center}
  \normalfont\fbox{
	 \addtolength{\probwidth}{#1cm}\parbox{\probwidth}{\textsc{#2}\\\hspace*{1.5em}
     \begin{tabular}[t]{
      rp{#1cm}}\textit{Instance:}&#3 \\
      \textit{Problem:}&#4
     \end{tabular}}}
\end{center}}

\beamertemplatenavigationsymbolsempty

\title{A Theory of Selective Prediction}
\subtitle{by Mingda Qiao, Gregory Valiant (arXiv:1902.04256)}
\author{Xuangui Huang}
\date{April 4, 2019}

\begin{document}
\frame{\titlepage}

%\begin{frame}
%\ft{Outline}
%\begin{enumerate}
%	\item \textsc{Background}
%	\item \textsc{Selective Prediction: General Formalization}
%	\item \textsc{Results}
%	\begin{itemize}
%		\item \textsc{Predicting Mean}
%		\item \textsc{Predicting Smooth Functions}
%		\item \textsc{Predicting Concatenation-Concave Functions}
%	\end{itemize}
%	\item \textsc{Algorithm \& Analysis}
%\end{enumerate}
%\end{frame}

\begin{frame}
\ft{Motivation}

\begin{center}
	\includegraphics[scale=0.35]{price.png}
\end{center}
\vspace*{-0.6cm}
\begin{itemize}
\item You know nothing about this market
\item Can you make an accurate prediction? \pause
\item You can choose a time point within the next $n$ days
\item You can choose the interval over which your prediction spans 
\end{itemize}
\end{frame}

\begin{frame}
\ft{Selective Prediction}
\fst{General Formalization: Game}
\begin{itemize}
	\item Given a family of $n$ functions $(f_1, \dots, f_n)$, where $f_m \colon X^m \to \-R$
	\item A sequence $x \in X^n$ is chosen \red{adversarially} at the beginning 
	\item At each time step $t \in \{0, \dots, n-1\}$:
	\begin{itemize}
		\item player can make a predict that $f_m(x_{t+1}, \dots, x_{t+m})$ is $\hat{\alpha}$ for some $1 \leq m \leq n-t$
		\item[] game terminates, incurs a loss of $\ell(\alpha, \hat{\alpha})$
		\item[] 
		\item otherwise $x_{t+1}$ is revealed
	\end{itemize}
	\item Must predict exactly once
	\item Goal: minimize the expected loss
\end{itemize}
\end{frame}

\begin{frame}
\ft{Selective Prediction}

\begin{itemize}
\item We consider squared loss $(\alpha - \hat{\alpha})^2$ and absolute loss $\vert \alpha - \hat{\alpha} \vert$ 
\item[]\pause
\item We make no distributional assumptions about the sequence
\item The sequence is \red{non-adaptive}
\item Loss is ``absolute'' instead of relative to some benchmarks \pause
\item The predictor make exactly one prediction
\item The predictor is entitled the power of choosing $t$ and $m$
\end{itemize}
\end{frame}

\begin{frame}
\ft{Selective Prediction}
\fst{Selectivity is Necessary}

\begin{proposition}
Suppose the prediction algorithm $A$, either:
\begin{itemize}
	\item always predicts at the same time $t$,
	\item always choose the same window length $m$,
	\item or chooses $t$ but must use $m = n-t$.
\end{itemize}
Then there exists a binary sequence of length $n$ on which $A$ has an expected squared loss of at least $\frac{1}{64}$.
\end{proposition}
\end{frame}

\begin{frame}
\ft{Results}
\fst{Predicting Arithmetic Mean \& General Functions}
\centering
\begin{tabular}{|c|c|}
\hline
Function Predicted & Expected Loss \\
\hline
mean & squared loss \\
$X = [0,1]$ & $\Theta\!\left(\frac{1}{\log n}\right)$ \\
\hline
$L$-smooth & absolute loss \\
$X = [0,1]$ & $O\!\left(\frac{L}{\sqrt{\log n}}\right)$ \\
\hline
concatenation-concave & squared loss \\
bounded in $[0,1]$ & $\Theta\!\left(\frac{1}{\log n}\right)$ \\
\hline
\end{tabular}
\end{frame}


\begin{frame}
\ft{Results}
\fst{Predicting $L$-smooth Functions}

\begin{definition}[Earth Mover's Distance]
For $x, y \in [0,1]^m$,
\[
EMD(x,y) = \int_0^1 \vert \+U(x)([0, \tau]) - \+U(y)([0, \tau]) \vert ~\mathrm{d}\tau ,
\]
where $\+U(x)$ is the uniform distribution over multiset $x$.
\end{definition}

\begin{definition}
A function $f \colon [0,1]^m \to \mathbb{R}$ is $L$-smooth iff $\vert f(x) - f(y) \vert \leq L \cdot EMD(x, y)$ for any $x, y \in \mathbb{R}^m$.
\end{definition}
\end{frame}


\begin{frame}
\ft{Results}
\fst{Predicting Concatenation-Concave Functions}

\begin{definition}
A function family $(f_m \colon X^m \to \mathbb{R})^n_{m = 1}$ is \emph{concatenation-concave} iff for any $x \in X^{m_1}$ and $y \in X^{m_2}$ with $m_1 + m_2 \leq n$, 
\[
f_{m_1 + m_2}(x, y) \geq \frac{m_1}{m_1 + m_2} f_{m_1}(x) + \frac{m_2}{m_1 + m_2} f_{m_2}(y)
\]
\end{definition}
\pause
\begin{itemize}
\item Arithmetic mean is concatenation-concave
\item ``Learnability'' of data sequence $(x_1, \dots, x_2)$: $\inf_{\ell \in \+L} \frac{1}{m} \sum_{i = 1}^m \ell(x_i)$
	\begin{itemize}
		\item model class $\+L$: a family of bounded loss functions mapping $X$ to $[0,1]$	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\ft{Results}
\fst{Predicting Arithmetic Mean, General Functions, \& More}
\centering
\begin{tabular}{|c|c|}
\hline
Function Predicted & Expected Loss \\
\hline
mean & squared loss \\
$X = [0,1]$ & $\Theta\!\left(\frac{1}{\log n}\right)$ \\
\hline
$L$-smooth & absolute loss \\
$X = [0,1]$ & $O\!\left(\frac{L}{\sqrt{\log n}}\right)$ \\
\hline
concatenation-concave & squared loss \\
bounded in $[0,1]$ & $\Theta\!\left(\frac{1}{\log n}\right)$ \\
\hline
\end{tabular}
\pause
\begin{itemize}
	\item \textsc{Fitting Unseen Data}: 
	\begin{itemize}
		\item output $\hat{\ell}$ that minimize the \emph{excess risk} $\frac{1}{m} \sum_{i = 1}^m \hat{\ell}(x_{t+i}) - \inf_{\ell \in \+L} \frac{1}{m}\sum_{i = 1}^m \ell(x_{t+i})$
		\item can achieve $O\!\left(\sqrt{\frac{|\+L|}{\log n}}\right)$
	\end{itemize}	 
\end{itemize}
\end{frame}

\begin{frame}
\ft{Prediction Algorithm}

\begin{itemize}
\item Input: sequence $x \in X^n$ of length $n = 2^k$
\item[]\pause
\item Choose $k'$ uniformly from $[k]$
\item Choose $t'$ uniformly from $\{0, 2^{k'}, 2\cdot 2^{k'}, 3 \cdot 2^{k'}, \dots, n-2^{k'}\}$ \pause 
\item[]
\item At time $t' + 2^{k'-1}$, output the function value of the most recent $2^{k'-1}$ elements as the prediction for the function value of the next $2^{k'-1}$ elements
\end{itemize}
\end{frame}

\begin{frame}
\ft{Analysis of the Prediction Algorithm}

\begin{lemma}[1]
Suppose $f_m$'s are the mean and $X = [0,1]$. For any $k \geq 1$, the algorithm achieves an expected squared loss of at most $\frac{1}{k}$ on any sequence of length $2^k$.
\end{lemma}

\begin{lemma}[2]
Suppose $f_m$'s are $L$-smooth and $X = [0,1]$. For any $k \geq 1$, the algorithm achieves an expected absolute loss of at most $\frac{L}{\sqrt{k}}$ on any sequence of length $2^k$.
\end{lemma}

\begin{lemma}[3]
Suppose $f_m$'s are con.-con. and bounded in $[0,1]$. For any $k \geq 1$, the algorithm has an expected squared loss of at most $\frac{4}{k}$ on any sequence of length $2^k$.
\end{lemma}

\end{frame}

\begin{frame}
\ft{Lower Bound for Predicting Mean}

\begin{theorem}
For any $k \geq 1$, there exists a distribution $D_k$ of sequences of length $2^k$ such that any prediction algorithm has an expected squared loss of at least $\frac{1}{64k}$.
\end{theorem}
\end{frame}

\begin{frame}
\centering \Large
\textsc{Thank you}\\
\textsc{Q \& A}
\end{frame}
\end{document}