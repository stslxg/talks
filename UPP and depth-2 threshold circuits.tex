\documentclass{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}


%\usepackage{beamerthemesplit}
%\usepackage[orientation=landscape,size=custom,width=16,height=9,scale=0.5,debug]{beamerposter}
%\usetheme{Antibes}
\usecolortheme{spruce}

\newcommand{\ma}[1]{\ensuremath{#1}}

\newcommand{\T}{\ma{\mathsf{T}}}

\renewcommand{\aa}[1]{\mathbf{#1}}
\newcommand{\bra}[1]{\langle #1\rangle}
\newcommand{\sqbra}[1]{[ #1]}
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\eps}{\varepsilon}
\newcommand<>{\red}[1]{{\color#2{red} #1}}
\newcommand<>{\blue}[1]{{\color#2{blue} #1}}
\newcommand<>{\green}[1]{{\color#2[rgb]{0,0.5,0} #1}}

\newcommand{\PP}{\mathbf{P_{/poly}}}
\newcommand{\AlgPP}{\mathbf{AlgP_{/poly}}}
\newcommand{\EXP}{\mathbf{EXP}}
\newcommand{\NEXP}{\mathbf{NEXP}}

\newcommand{\ft}[1]{\frametitle{\textsc{#1}}}
\newcommand{\fst}[1]{\framesubtitle{\textsc{#1}}}

\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}
\newtheorem{remark}{Remark}
\newtheorem{observation}{Observation}


\beamertemplatenavigationsymbolsempty

\title{Unbounded Error Communication\footnote{special thanks is given to Dr. Scheder for sharing his notes on discrepancy}}
\subtitle{and Depth-2 Threshold Circuits}
\author{Talk given by Xuangui Huang}
\date{April 23, 2015}

\begin{document}
\frame{\titlepage}

\begin{frame}
\ft{Unbounded Error Communication}
\begin{definition}
	Let $\Pi$ be a randomized two-player protocol with private coins. We say \blue{$\Pi$ computes $f: X \times Y \rightarrow \{-1,1\}$ with unbounded error} if
	\[\forall (x,y) \in X \times Y,: \Pr[\Pi(x,y) \text{ outputs }f(x,y)] > \frac{1}{2}.\]
\end{definition}
\pause
\begin{itemize}
	\item $C(\Pi) = $ maximun number of bits exchanged over all inputs $(x,y)$.
	\item $R^U(f) = \min_{\Pi \text{ computes }f \text{ with unbounded error}} C(\Pi)$.
\end{itemize}
\end{frame}

\begin{frame}
\ft{Unbounded Error Communication}
\begin{itemize}
	\item We can view the protocol as a tree.
	\item Let $\tau$ be a path from the root to a leaf, we can define functions
	\[p_\tau(x) = \prod_{u \in \tau \text{ owned by Alice}} a_u(x)\]
	\[q_\tau(y) = \prod_{v \in \tau \text{ owned by Bob}} b_v(x).\]
\end{itemize}
\pause
\begin{observation}
For a protocol $\Pi$ computes $f$ with unbounded error, we have 
\[f(x,y) = 1 \Leftrightarrow \sum_{\tau: val(\tau) = 1} p_\tau(x)q_\tau(y) > \frac{1}{2}\] hence $f(x,y) = sign (\sum_\tau p_\tau(x)q_\tau(y)val(\tau))$.
\end{observation}
\end{frame}

\begin{frame}
\[f(x,y) = sign (\sum_\tau p_\tau(x)q_\tau(y)val(\tau))\]
\pause
\begin{theorem}[One Round is Enough]
There is a one-way protocol $\Pi$ of compleixty at most $R^U(f) + 1$ that computes $f$ with unbounded error.
\end{theorem}
\begin{proof}
Consider the following protocol:
\texttt{
\begin{enumerate}
	\item Alice samples a random path $\tau$ with probability $\frac{1}{\sum_\tau p_\tau(x)}p_\tau(x)$ and sends it to Bob.
	\item After receiving $\tau$, Bob outputs $val(\tau)$ with probability $q_\tau(y)$; with the remaining probability, he outputs $-1$ or $1$ uniformly.
\end{enumerate}}
\pause
\[\mathbb{E}[\text{Bob's output}] = \sum_\tau \frac{1}{\sum_\tau p_\tau(x)}p_\tau(x)q_\tau(y)val(\tau).\]
\end{proof}
\end{frame}

\begin{frame}
\[f(x,y) = sign (\sum_\tau p_\tau(x)q_\tau(y)val(\tau))\]
\pause
\begin{itemize}
	\item Let $T$ be the set of all possible paths $\tau$.
	\item Define matrices $P \in \mathbb{R}^{X \times T}$, $Q \in \mathbb{R}^{T \times Y}$ by $P_{x,\tau} := p_\tau(x)$, $Q_{\tau,y}:=q_\tau(y)val(\tau)$. \pause
	\item Let $A = PQ$. Then we have $M_f = sign(A)$ and $rank(A) \leq |T|$ $ \leq 2^{C(\Pi)}$.
\end{itemize}
\pause
\begin{theorem}
There is a matrix $A$ with $M_f = sign(A)$ and $rank(A) \leq 2^{R^U(f)}$. 
\end{theorem}
\begin{definition}
$\text{sign-rank}(M) = \min_{A: sign(A) = M} rank(A)$. 
\end{definition}
\begin{corollary}[Sign-rank Lower Bound]
$R^U(f) \geq \log \text{sign-rank}(M_f)$. 
\end{corollary}
\end{frame}

\begin{frame}
\[f(x,y) = sign (\sum_\tau p_\tau(x)q_\tau(y)val(\tau))\]
\pause
\begin{itemize}
	\item Let $T$ be the set of all possible paths $\tau$.
	\item Define matrices $P \in \mathbb{R}^{X \times T}$, $Q \in \mathbb{R}^{T \times Y}$ by $P_{x,\tau} := p_\tau(x)$, $Q_{\tau,y}:=q_\tau(y)val(\tau)$. \pause
	\item Let $A = PQ$. Then we have $M_f = sign(A)$ and $rank(A) \leq |T|$ $ \leq 2^{C(\Pi)}$.
\end{itemize}
\pause
\begin{theorem}
There is a matrix $A$ with $M_f = sign(A)$ and $rank(A) \leq 2^{R^U(f)}$. 
\end{theorem}
\begin{definition}
$\text{sign-rank}(M) = \min_{A: sign(A) = M} rank(A)$. 
\end{definition}
\begin{corollary}[Sign-rank Lower Bound]
$R^U(f) \geq \log_2 \text{sign-rank}(M_f)$. 
\end{corollary}
\end{frame}

\begin{frame}
\ft{Minimal dimension}
\begin{itemize}
	\item We say that \blue{$f$ can be embedded into $\mathbb{R}^d$} if there are vectors $(\aa{u}^{(x)})_{x \in X}$, $(\aa{v}^{(y)})_{y \in Y} \in \mathbb{R}^d$ such that $f(x, y) = sign(\bra{\aa{u}^{(x)}, \aa{v}^{(y)}})$.
	\item The \blue{minimal dimension} $d(f)$ is defined to be the minimal $k$ such that $f$ can be embedded into $\mathbb{R}^d$.
\end{itemize}
\pause
\begin{theorem}
	$\log_2 d(f) \leq R^U(f) \leq \log_2 d(f) + 2$.
\end{theorem} 
\pause
\begin{proof}
	\[\mathbb{E}[\text{Bob's output}] = \sum_i \frac{|u^x_i|}{\sum_i |u^x_i|} \cdot sign(u^x_i) \cdot sign(v^y_i) \cdot \frac{|v^y_i|}{\sum_i |v^y_i|}.\]
\end{proof}
\end{frame}

\begin{frame}
\ft{Depth-2 Threshold Circuits}
\begin{theorem}
	Let $(f_n)_{n \in \mathbb{N}}$ be a family of Boolean functions $f_n: \red{\{0,1\}^n \times \{0,1\}^n} \rightarrow \{-1,1\}$. Suppose it is computed by depth-2 threshold circuits in which the \red{top} gate is a \blue{linear threshold gate}(with \red{unrestricted} weights). Then the following holds:
	\begin{itemize}
		\item If the bottom level has $s$ \blue{linear threshold gates} using \red{integer} weights of \red{absolute value at most $W$}, then $s = \Omega(\frac{d(f_n)}{nW})$.
		\item If the bottom level has $s$ gates computing \blue{symmetric functions}, then $s = \Omega(\frac{d(f_n)}{n})$.
	\end{itemize}
\end{theorem}
\end{frame}

\begin{frame}
\begin{lemma}
	Let $g: \{0,1\}^n \times \{0,1\}^n \rightarrow \{0,1\}$ be a \blue{threshold function} where for $x,y \in \{0,1\}^n$, $g(x,y) = 1$ iff $\sum_{i=1}^n \alpha_ix_i + \sum_{i=1}^n \beta_iy_i \geq \mu$ for weights \red{$\alpha_i$, $\beta_i$, $\mu \in \mathbb{Z}$}. Then $M_g$ has rank at most $\min\{\sum_{i=1}^n |\alpha_i|, \sum_{i=1}^n |\beta_i|\} +1$.
\end{lemma}
\pause
\begin{proof}
\begin{itemize}
	\item W.l.o.g. let $\sum_{i=1}^n |\alpha_i| <  \sum_{i=1}^n |\beta_i|$.
	\item Let $\alpha_{min} = \min_{x \in \{0,1\}^n} \sum_{i=1}^n \alpha_ix_i$, and $\alpha_{max}$ respectively. 
	\pause
	\item Define the following sets for all $\alpha$ such that $\alpha_{min} \leq \alpha \leq \alpha_{max}$:
	$S_{\alpha,0} := \{(x,y): \sum_{i=1}^n \alpha_ix_i = \alpha \text{ and } \sum_{i=1}^n \beta_iy_i < \mu - \alpha\}$
	$S_{\alpha,1} := \{(x,y): \sum_{i=1}^n \alpha_ix_i = \alpha \text{ and } \sum_{i=1}^n \beta_iy_i \geq \mu - \alpha\}$.
	\item Let $M_{\alpha,0}$ and $M_{\alpha,1}$ be the respective submatrices.
	\pause
	\item Then $M_g = \sum_\alpha (M_{\alpha,0} + M_{\alpha,1})$, \pause hence $rank(M_g) \leq \alpha_{max} - \alpha_{min} + 1 \leq \sum_{i = 1}^n |\alpha_i| +1$.
\end{itemize}
\end{proof}
\end{frame}

\begin{frame}
\begin{lemma}
	Let $f$ be a function computed by depth-2 threshold circuits with the \red{top} gate using \red{unrestricted} weights and each of the \red{bottom} gates using \red{integer} weights of \red{absolute value at most $W$}. Then there is a real matrix $F$ such that $f(x,y) = sign(F(x,y))$ for all $x,y$ and $rank(F) = O(snW)$.
\end{lemma}
\pause
\begin{proof}
\begin{itemize}
	\item Let the top gate of $C$ have weights $\phi_1, \dots, \phi_s$ and threshold $\mu$.
	\item Then we can write $f(x,y) = sign(\sum_{i=1}^s \phi_ig_i(x,y) - \mu)$. \pause
	\item Define $F := \phi_1M_{g_1} + \cdots + \phi_sM_{g_s} - \mu I$. \pause
	\item Then $rank(F) \leq 1 + \sum_{i =1}^s rank(M_{g_i}) \leq 1 + s(1+nW)$.
\end{itemize}
\end{proof}

\end{frame}

\begin{frame}
\centering
\Large
\textsc{Thank You!} \\
\textsc{Q \& A}
\end{frame}
\end{document}

