\documentclass{beamer}

%\usepackage{concrete} % for the Concrete Math text fonts
\usepackage{euler} % for the Concrete Math math fonts	
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}


%\usepackage{beamerthemesplit}
%\usepackage[orientation=landscape,size=custom,width=16,height=9,scale=0.5,debug]{beamerposter}
%\usetheme{Antibes}
\usecolortheme{spruce}

\newcommand{\ma}[1]{\ensuremath{#1}}

\newcommand{\T}{\ma{\mathsf{T}}}

\renewcommand{\aa}[1]{\mathbf{#1}}
\newcommand{\bra}[1]{\langle #1\rangle}
\newcommand{\sqbra}[1]{[ #1]}
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\eps}{\varepsilon}
\newcommand<>{\red}[1]{{\color#2{red} #1}}
\newcommand<>{\blue}[1]{{\color#2{blue} #1}}
\newcommand<>{\green}[1]{{\color#2[rgb]{0,0.5,0} #1}}

\newcommand{\PP}{\mathbf{P_{/poly}}}
\newcommand{\AlgPP}{\mathbf{AlgP_{/poly}}}
\newcommand{\EXP}{\mathbf{EXP}}
\newcommand{\NEXP}{\mathbf{NEXP}}

\newcommand{\ft}[1]{\frametitle{\textsc{#1}}}
\newcommand{\fst}[1]{\framesubtitle{\textsc{#1}}}

\newcommand{\cA}{\mathcal{A}}
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cG}{\mathcal{G}}
\newcommand{\cU}{\mathcal{U}}
\newcommand{\cE}{\mathcal{E}}

\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}
\newtheorem{remark}{Remark}
\newtheorem{observation}{Observation}


\beamertemplatenavigationsymbolsempty

\title{A Discrepancy Lower Bound for Information Complexity}
\author{Talk given by Xuangui Huang}
\date{June 11, 2015}

\begin{document}
\frame{\titlepage}

\begin{frame}
\ft{Motivation}
\begin{itemize}
	\item The (internal) information complexity of $f$ is always \green{upper bounded} by its communication complexity. The converse is not known to be true.
	\item Lower bound techniques for CC do not readily translate into lower bound techniques for IC.
	\begin{itemize}
		\item A low-information protocol is not limited in the amout of communication it uses.
	\end{itemize}
	\pause
	\item In the previous talk, it is shown that given an $k$-round protocol $\pi$ with $IC = I$, we can have a protocal simulating it with $CC =  I + \red{O(\sqrt{kI} + k) + 2k\log(1/\epsilon)}$.
\end{itemize}
\end{frame}

\begin{frame}
\ft{Main Result}
\begin{theorem}[Main Theorem]
Suppose that $IC_\mu(f, \frac{1}{10}) = I_\mu$. Then there exists a protocol $\pi'$ such that
\begin{itemize}
\item $CC(\pi') = O(I_\mu)$
\item $\Pr_{(x,y) \sim \mu} [\pi'(x,y) = f(x,y)] \geq \frac{1}{2} + 2^{-O(I_\mu)}$
\end{itemize}
\end{theorem}
\pause

\begin{theorem}
$IC_\mu(f,\frac{1}{10}) \geq \Omega(\log \frac{1}{Disc_\mu(f)})$.
\end{theorem}
\end{frame}

\begin{frame}
\ft{Main Idea}
\begin{itemize}
\item Given a protocol $\pi$ that realizes the value $I_\mu$, we can simulate it in low communication cost:
\item For each \green{leaf} $\ell$ of the protocol, we have $p_A(\ell)$, $q_A(\ell)$, $p_B(\ell)$, $q_B(\ell)$.
\item $\Pr[\pi_{xy} = \ell] = p_A(\ell) p_B(\ell)$, $\Pr[\pi_{x} = \ell] = p_A(\ell) q_A(\ell)$ and $\Pr[\pi_{y} = \ell] = q_B(\ell) p_B(\ell)$. \pause
\item $I_\mu = I(X;\pi_{XY} | Y) + I(Y;\pi_{XY}|X) = \mathbf{E}_{(x,y) \sim \mu}[\mathbf{D}(\pi_{xy}||\pi_x) + \mathbf{D}(\pi_{xy}||\pi_x)].$
\end{itemize}
\end{frame}

\begin{frame}
\ft{Main Techinal Lemma}
\begin{lemma}[Sampling and Compression Lemma]
\only<1>{Let $\mu$ to be any distribution over a universe $\mathcal{U}$ and let $I \ge 0$ be a parameter known to both $A$ and $B$. Let $\nu_A$ and $\nu_B$ be two distributions over $\mathcal{U}$ such that \blue{$\mathbf{D}(\mu||\nu_A) \le I$ and $\mathbf{D}(\mu||\nu_B) \le I$}. The tow players are each given a pair of real functions \blue{$(p_A, q_A), (p_B, q_B)$} from $\mathcal{U}$ to $[0,1]$ such that $\mu(x) = p_A(x)p_B(x)$, $\nu_A(x) = p_A(x)q_A(x)$, and $\nu_B(x) = p_B(x)q_B(x)$. } \pause Then there is a \blue{two round} sampling protocol $\Pi_1 = \Pi_1(p_A, q_A, p_B, q_B, I)$ such that:
\begin{itemize}
	\item At the end of the protocol, they either outputs $\bot$ (fail), or output \red{$x_A \in \mathcal{U}$ and $x_B \in \mathcal{U}$}.
	\item Let $S$ be the event they succeed, then $S \Rightarrow x_A = x_B$ and $0.9 \cdot 2^{-50(I+1)} \leq \Pr[S] \leq 2^{-50(I+1)}$.
	\item If $\mu_1$ is the distribution of $x_A$ conditioned on $S$, then $|\mu - \mu_1| < \frac{2}{9}$.
\end{itemize}\pause
Furthermore, $\Pi_1$ can be `compressed' to a protocol $\Pi_2$ such that \green{$CC(\Pi_2) = 211I + 1$}, while $|\Pi_1 - \Pi_2| \leq 2^{-59I}$.
\end{lemma}
\end{frame}

\begin{frame}
\ft{Proof of Main Theorem}
\texttt{
\begin{itemize}
\item If $\Pi_1$ fails, then return a random unbiased coin flip.
\item If $\Pi_1$ succeeds, then return the final bit of the transcript $T$, denoted as $T_{out}$.
\end{itemize}}
\pause
\begin{itemize}
\item Take $I$ to be $20I_\mu$.
\item $Z \triangleq \text{``Both $\mathbf{D}(\pi_{xy}||\pi_x) \leq 20I_\mu$ and $\mathbf{D}(\pi_{xy}||\pi_y) \leq 20I_\mu$''}$. Then $\Pr[Z] \geq \frac{19}{20}$.
\item $\delta \triangleq \Pr[\text{the probability that $\Pi_1$ succeeds}] \geq 0.9 \cdot 2^{-50(I+1)}$.
\item $W \triangleq \mathbb{I}[\pi'(x,y) = f(x,y)]$.
\pause
\item If $\Pi_1$ succeed, then $|T - \pi_{xy}| \leq \frac{2}{9}$ thus $\Pr[T_{out} = \pi_{out}] \geq \frac{7}{9}$ and $\Pr[\pi_{out} = f(x,y)] \geq \frac{9}{10}$.
\end{itemize}
\end{frame}

\begin{frame}
\ft{Proof of Main Theorem}

\begin{itemize}
\item $\mathbf{E}[W |Z] = (1-\delta) \cdot \frac{1}{2} + \delta \cdot (\frac{7}{9} - \frac{1}{10}) > \frac{1}{2} + \frac{1}{8} \cdot 2^{-50(I+1)}$.
\item $\mathbf{E}[W | \neg Z] \geq (1-2^{-50(I+1)}) \cdot \frac{1}{2}$.\pause
\item $\mathbf{E}[W] > \frac{1}{2} + \frac{1}{12} \cdot 2^{-1000(I_\mu + 1)}$.
\item Replacing $\Pi_1$ by $\Pi_2$ will decrease the success probability by at most $2^{-59I} \ll \frac{1}{12} \cdot 2^{-50(I+1)}$. 
\end{itemize}
\qed
\end{frame}


\begin{frame}
\ft{Main Techinal Lemma}
\begin{lemma}[Sampling and Compression Lemma]
We have \blue{$\mathbf{D}(\mu||\nu_A) \le I$ and $\mathbf{D}(\mu||\nu_B) \le I$}.Then there is a \blue{two round} sampling protocol $\Pi_1 = \Pi_1(p_A, q_A, p_B, q_B, I)$ such that:
\begin{itemize}
	\item At the end of the protocol, they either outputs $\bot$ (fail), or output \red{$x_A \in \mathcal{U}$ and $x_B \in \mathcal{U}$}.
	\item Let $S$ be the event they succeed, then $S \Rightarrow x_A = x_B$ and $0.9 \cdot 2^{-50(I+1)} \leq \Pr[S] \leq 2^{-50(I+1)}$.
	\item If $\mu_1$ is the distribution of $x_A$ conditioned on $S$, then $|\mu - \mu_1| < \frac{2}{9}$.
\end{itemize}
Furthermore, $\Pi_1$ can be `compressed' to a protocol $\Pi_2$ such that \green{$CC(\Pi_2) = 211I + 1$}, while $|\Pi_1 - \Pi_2| \leq 2^{-59I}$.
\end{lemma}
\end{frame}

\begin{frame}
\ft{Sampling Protocol $\Pi_1$}
\texttt{
\begin{itemize}
	\item A and B publicly sample \blue{$T = |\mathcal{U}|2^{50(I+1)}60I$} points \blue{$(x_i, \alpha_i, \beta_i)$} uniformly distributed in $\mathcal{U} \times \blue{[0, 2^{50(I+1)}] \times [0, 2^{50(I+1)}]}$.
	\item A computes $\cA = \{i \in [T]: \alpha_i \leq p_A(x_i) \text{ and } \beta_i \leq \red{2^{50(I+1)}}q_A(x_i)\}$. \newline B computes $\cB = \{i \in [T]: \only<1>{\red{\alpha_i \leq 2^{50(I+1)}q_B(x_i)} \text{ and }} \beta_i \leq p_B(x_i)\}$.
	\item A sends the element $a = x_i$ where $i = \min \cA$, otherwise outputs $\bot$.
	\item B sends $a$ if $a \in \cB$, otherwise output $\bot$.
\end{itemize}
}\pause
\end{frame}

\begin{frame}
\ft{Sampling Lemma}
\begin{itemize}
	\item Denote the set of \green{``Good''} elements by $\cG \triangleq \{x: 2^{50(I+1)} \nu_A(x) \geq \mu(x) \text{ and } 2^{50(I+1)} \nu_B(x) \geq \mu(x)\}$.\only<3->{Then $\mu[\cG] \geq \frac{24}{25}$.}
\end{itemize}
\only<2>{
\begin{claim}[Bra11]
Suppose that $\mathbf{D}(\mu||\nu) \le I$. Let $\epsilon$ be any parameter, then $\mu[x: 2^{(I+1)/\epsilon} \nu(x) < \mu(x)] \le \epsilon$.
\end{claim}}
\only<4->{
\begin{proposition}
Assuem $\cA$ is not empty. Then for any $x_i \in \cU$, $\Pr[\Pi \text{ outputs }x_i] \le \mu(x_i)2^{-50(I+1)}$. If $x_i \in \cG$, then $\Pr[\Pi \text{ outputs }x_i] = \mu(x_i)2^{-50(I+1)}$.
\end{proposition}
\begin{proposition}
Let $S$ denote the event that $\cA \neq \varnothing$ and $a \in \cB$. Then $0.9 \cdot 2^{-50(I+1)} \le \Pr[S] \le 2^{-50(I+1)}$.
\end{proposition}
\begin{claim}
Let $\mu_1$ be the distribution of $a|S$. Then $|\mu - \mu_1| \leq \frac{2}{9}$.
\end{claim}}
\end{frame}

\begin{frame}
\begin{proposition}
Assuem $\cA$ is not empty. Then for any $x_i \in \cU$, $\Pr[\Pi \text{ outputs }x_i] \le \mu(x_i)2^{-50(I+1)}$. If $x_i \in \cG$, then it is exact.
\end{proposition}
\begin{proof}
\begin{itemize}
\item For $x_i \in \cU$, the probability that $x_i = a$ is $\red{p_A(x_i)q_A(x_i)}$ $ = \nu_A(x_i)$.
\item The probability that $\beta_i \le p_B(a)$ is $\min\{\frac{p_B(x_i)}{2^{50(I+1)}q_A(x_i)},1\}$.
\item Hence $\Pr[\Pi \text{ outputs }x_i] \le p_A(x_i)q_A(x_i)\frac{p_B(x_i)}{2^{50(I+1)}q_A(x_i)} = \mu(x_i) 2^{-50(I+1)}$.
\pause \item If $x_i \in \cG$, then $\frac{p_B(x_i)}{q_A(x_i)} = \frac{\mu(x_i)}{\nu_A(x_i)} \le 2^{50(I+1)}$.
\end{itemize}
\end{proof}
\end{frame}

\begin{frame}
\begin{proposition}
Let $S$ denote the event that $\cA \neq \varnothing$ and $a \in \cB$. Then $0.9 \cdot 2^{-50(I+1)} \le \Pr[S] \le 2^{-50(I+1)}$.
\end{proposition}
\begin{proof}
\begin{itemize}
\item $\Pr[x\in \cA] = \frac{1}{|\cU|2^{50(I+1)}}$, hence $\Pr[\cA = \varnothing] = (1- \frac{1}{|\cU|2^{50(I+1)}})^T < 2^{-60I}$. \pause
\item $\Pr[S] \le \Pr[a \in \cB|\cA \neq \varnothing] = \sum_{x_i \in \cU}\Pr[a=x_i] \Pr[\beta_i \le p_B(a)]$
$\le \sum_{x_i \in \cU} \mu(x_i) 2^{-50(I+1)} = 2^{-50(I+1)}$. \pause
\item $\Pr[S] = \Pr[\cA \neq \varnothing] \Pr[\beta_i \le p_B(a) | \cA \neq \varnothing]$ 
$\ge (1-2^{-60I}) (\sum_{x_i \in \cU}\Pr[a=x_i] \Pr[\beta_i \le p_B(a)])$
$\ge (1-2^{-60I}) (\sum_{x_i \in \red{\cG}}\Pr[a=x_i] \Pr[\beta_i \le p_B(a)])$
$= (1-2^{-60I}) (\sum_{x_i \in \red{\cG}} \mu(x_i) 2^{-50(I+1)}) = (1-2^{-60I}) 2^{-50(I+1)} \mu[\cG]$
$\ge 0.9 \cdot 2^{-50(I+1)}$.
\end{itemize}
\end{proof}
\end{frame}

\begin{frame}
\begin{claim}
Let $\mu_1$ be the distribution of $a|S$. Then $|\mu - \mu_1| \leq \frac{2}{9}$.
\end{claim}
\begin{proof}
\begin{itemize}
\item For $x_i \in \cU$, $\mu_1(x_i) = \Pr[x_i = a|S] \le \frac{\mu(x_i)2^{-50(I+1)}}{\Pr[S]}$ $\le \frac{\mu(x_i)}{0.9} = (1+\frac{1}{9}) \mu(x_i)$.
\item $|\mu - \mu_1| = 2(\sum_{x_i: \mu_1(x_i) \ge \mu(x_i)} \mu_1(x_i) - \mu(x_i)) \le \frac{2}{9}$.
\end{itemize}
\end{proof}
\end{frame}

\begin{frame}
\ft{Main Techinal Lemma}
\begin{lemma}[Sampling and Compression Lemma]
We have \blue{$\mathbf{D}(\mu||\nu_A) \le I$ and $\mathbf{D}(\mu||\nu_B) \le I$}.Then there is a \blue{two round} sampling protocol $\Pi_1 = \Pi_1(p_A, q_A, p_B, q_B, I)$ such that:
\begin{itemize}
	\item At the end of the protocol, they either outputs $\bot$ (fail), or output \red{$x_A \in \mathcal{U}$ and $x_B \in \mathcal{U}$}.
	\item Let $S$ be the event they succeed, then $S \Rightarrow x_A = x_B$ and $0.9 \cdot 2^{-50(I+1)} \leq \Pr[S] \leq 2^{-50(I+1)}$.
	\item If $\mu_1$ is the distribution of $x_A$ conditioned on $S$, then $|\mu - \mu_1| < \frac{2}{9}$.
\end{itemize}
Furthermore, $\Pi_1$ can be `compressed' to a protocol $\Pi_2$ such that \green{$CC(\Pi_2) = 211I + 1$}, while $|\Pi_1 - \Pi_2| \leq 2^{-59I}$.
\end{lemma}
\end{frame}

\begin{frame}
\ft{Compressed Protocol $\Pi_2$}
\texttt{
\begin{itemize}
	\item A and B publicly sample points then computes $\cA$, $\cB$ respectively. 
	\item A and B publicly sample $d = \lceil 211I \rceil$ hash functions $h_1, \dots, h_d$.
	\item If $\cA = \varnothing$, A outputs $\bot$. Otherwise, A chooses the element $a = x_i$ where $i = \min \cA$ and sets $x_A = a$. Then A sends $h_1(a), \dots, h_d(a)$.
	\item B finds the first index $\tau$ such that $\tau \in \cB$ and $h_j(x_\tau) = h_j(a)$ for all $j \in [d]$. B set $x_B = x_\tau$, otherwise outputs $\bot$.
\end{itemize}
}
\end{frame}

\begin{frame}
\ft{Compression Lemma}
\begin{itemize}
\item Let $\cE$ denotes the event that B finds an elment $x_i \neq a$.
\item $\Pr[x \in \cB] \le \frac{1}{|\cU|2^{50(I+1)}}$.
\item $\Pr[\cE] \le \Pr[\exists x_i \text{ s.t. } x_i \in \cB \text{ and } x_i \neq a \text{ and pass the hash test}]$
$\le T \cdot \frac{1}{|\cU|2^{50(I+1)}} \cdot 2^{-d} \le 2^{-60I}$.
\item $|\Pi_2 - \Pi_1| = \Pr[\cE] |[\Pi_2|\cE] - [\Pi_1 | \cE]| \le 2^{-60I}\cdot 2 \le 2^{-59I}$.
\end{itemize}
\pause
\begin{remark}
A can also use \blue{shared} randomness to sample \blue{$b_1, \dots, b_d$} and sends one bit to B indicating whether these hash values $h_1(b_1), \dots, h_d(b_d)$ matching the hashing of $a$. Thus the communication compleixty can be reduced to $O(1)$.
\end{remark}
\end{frame}

\begin{frame}
\centering \Large
\textsc{Thank you}
\end{frame}
\end{document}