\documentclass[12pt]{beamer}


%\usepackage{concrete} % for the Concrete Math text fonts
\usepackage{euler} 
\usepackage{beton}
\renewcommand\familydefault{ccr}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}
\usepackage{multirow}

%\usepackage{beamerthemesplit}
%\usepackage[orientation=landscape,size=custom,width=16,height=9,scale=0.5,debug]{beamerposter}
%\usetheme{Antibes}
\usecolortheme{spruce}

\newcommand{\ma}[1]{\ensuremath{#1}}

\newcommand{\T}{\ma{\mathsf{T}}}

\newcommand{\bra}[1]{\langle #1\rangle}
\newcommand{\sqbra}[1]{[ #1]}
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\eps}{\varepsilon}
\newcommand<>{\red}[1]{{\color#2{red} #1}}
\newcommand<>{\blue}[1]{{\color#2{blue} #1}}
\newcommand<>{\green}[1]{{\color#2[rgb]{0,0.5,0} #1}}

\newcommand{\PP}{\mathbf{P_{/poly}}}
\newcommand{\AlgPP}{\mathbf{AlgP_{/poly}}}
\newcommand{\EXP}{\mathbf{EXP}}
\newcommand{\NEXP}{\mathbf{NEXP}}

\newcommand{\ft}[1]{\frametitle{\textsc{#1}}}
\newcommand{\fst}[1]{\framesubtitle{\textsc{#1}}}

\newcommand{\cA}{\mathcal{A}}
\newcommand{\cB}{\mathcal{B}}
\newcommand{\cG}{\mathcal{G}}
\newcommand{\cU}{\mathcal{U}}
\newcommand{\cE}{\mathcal{E}}
\newcommand{\cF}{\mathcal{F}}
\newcommand{\bZ}{\mathbb{Z}}

\newcommand{\Hol}[1]{\mathrm{Holant}(#1)}
\newcommand{\Pef}[1]{\mathrm{PerfMatch}(#1)}

\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}
\newtheorem{remark}{Remark}
\newtheorem{observation}{Observation}
\newtheorem{conjecture}{Conjecture}

\newcommand{\tuple}[1]{\left(#1\right)}
\newcommand{\tp}{\tuple}
\newcommand{\sharpP}{\#\mathbf{P}} \renewcommand{\P}{\mathbf{P}}

\def\*#1{\mathbf{#1}}
\def\+#1{\mathcal{#1}}
\def\-#1{\mathbb{#1}}

\usepackage{xifthen}

\renewcommand{\Pr}[2][]{ \ifthenelse{\isempty{#1}}
  {\mathbf{Pr}\left[#2\right]} {\mathop{\mathbf{Pr}}_{#1}\left[#2\right]} }
\newcommand{\E}[2][]{ \ifthenelse{\isempty{#1}}
  {\mathbb{E}\left[#2\right]}
  {\mathbb{E}_{#1}\left[#2\right]} }
\newcommand{\Var}[2][]{ \ifthenelse{\isempty{#1}}
  {\mathbf{Var}\left[#2\right]}
  {\mathbf{Var}_{#1}\left[#2\right]} }

\newcommand{\nc}{\newcommand}
\nc{\rnc}{\renewcommand}
\nc{\nev}{\newenvironment}


\newlength{\probwidth}
\setlength{\probwidth}{4cm}


\nc{\nprob}[4][7]{
\begin{center}
  \normalfont\fbox{
	 \addtolength{\probwidth}{#1cm}\parbox{\probwidth}{\textsc{#2}\\\hspace*{1.5em}
     \begin{tabular}[t]{
      rp{#1cm}}\textit{Instance:}&#3 。 \\
      \textit{Problem:}&#4
     \end{tabular}}}
\end{center}}

\beamertemplatenavigationsymbolsempty

\title{The Planted Clique Problem \\ with Small Edge Probability}
\author{Xuangui Huang}
\date{Jan 12, 2017}

\begin{document}
\frame{\titlepage}

\begin{frame}
\ft{Outline}
\begin{enumerate}
	\item \textsc{Background}
	\item \textsc{Generalized Planted Clique}
	\item \textsc{Small Edge Probability}
	\item \textsc{Algorithm for Relatively Small Planted Clique}
	\item \textsc{$\mathbf{AC}^0[q]$ Circuit Lower Bounds}
	\item \textsc{Future Works}
\end{enumerate}
\end{frame}

\begin{frame}
\ft{Background}

\begin{itemize}
	\item \textsc{Max-Clique}
	\begin{itemize}
		\item[{[Karp 72]}] \textbf{NP}-Complete
		\item[{[H\aa stad 96]}] Unless $\textbf{NP} = \textbf{coRP}$, no $\*P$-time algorithm to approximate within a factor $n^{1-\epsilon}$ for any fixed $\epsilon > 0$
		\item[{[Holmerin '00]}] Unless $\textbf{P} = \textbf{NP}$, ...
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\ft{Average-Case Hardness Model}
\begin{itemize}
	\item Erd\"os-R\'enyi Random Graph $\+G(n, \frac{1}{2})$
	\begin{itemize}
		\item every edges are generated independently with probability $\frac{1}{2}$
		\item[]
		\item almost surely \textsc{Max-Clique} is about \blue{$2 \log n$}
		\item Simple randomized algorithm (Algorithm 2-1) to find clique of size $\log n$
		\item[]
		\pause
		\item It is still open whether we have $\*P$-time randomized algorithms to find clique of size $(1 + \epsilon) \log n$
in $\+G(n, \frac{1}{2})$ for $\epsilon > 0$
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\ft{The Planted Clique Model}
\begin{itemize}
	\item What if a large clique is planted into $G \in \+G(n, \frac{1}{2})$ as input and we try to find it out?
	\item A potentially easier variant of \textsc{Max-Clique} in $\+G(n, \frac{1}{2})$
	\pause
	\item What if we decrease the edge probability?
	\item A potentially much easier variant
\end{itemize}
\end{frame}


\begin{frame}
\ft{Generalized Planted Clique Model}
\begin{itemize}
    \item For $k: \-N \to \-N$, $p: \-N \to \-R_+$, where $p(n) = n^{-\alpha(n)}$ satisfies:
    \begin{itemize}
        \item $\alpha(n) = \frac{1}{t(n)}$, where $t: \-N \to \-R_+$ satisfies \blue{$t(n) \leq \log n$} for $n$ sufficiently large
        \item $\lim_{n \to \infty} \alpha(n) = 0$
    \end{itemize}
    \pause
	\item $\*G \sim \+G(n, \red{p(n)}, k(n))$ or $(\*G', \*K) \sim \+G(n, \red{p(n)}, k(n))$:
	\begin{itemize}
		\item $\*G' \in \+G(n, \red{p(n)})$
		\item $\*K$ is a uniformly random subset of $[n]$ of size $k(n)$
		\item $\*G := \*G' + C(\*K)$ is the graph obtained by adding edges to $\*G'$ to make the subgraph induced on $\*K$ a clique
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\ft{Planted Clique Problem}
\nprob{$\mathrm{PC}(p(n), k(n))$}{Graph $\*G \sim \+G(n, p(n), k(n))$}{What is the set $\*K$?}
\nprob{$\mathrm{PC}_D(p(n), k(n))$}{Graph $\*G$ from $\+G(n, p(n))$ or $\+G(n, p(n), k(n))$}{$\*G \sim \+G(n, p(n), k(n))$?}
\end{frame}


\begin{frame}
\ft{Generalized Planted Clique Model}
\begin{itemize}
    \item For $k: \-N \to \-N$, $p: \-N \to \-R_+$, where $p(n) = n^{-\alpha(n)}$ satisfies:
    \begin{itemize}
        \item $\alpha(n) = \frac{1}{t(n)}$, where $t: \-N \to \-R_+$ satisfies \blue{$t(n) \leq \log n$} for $n$ sufficiently large
        \item $\lim_{n \to \infty} \alpha(n) = 0$
    \end{itemize}
	\item Note that $n^{-\frac{1}{\log n}} = \frac{1}{2}$
\end{itemize}
\end{frame}

\begin{frame}
\ft{Generalized Planted Clique Model}
\fst{with Small Edge Probability}
\begin{itemize}
    \item For $k: \-N \to \-N$, $p: \-N \to \-R_+$, where $p(n) = n^{-\alpha(n)}$ satisfies:
    \begin{itemize}
        \item $\alpha(n) = \frac{1}{t(n)}$, where $t: \-N \to \-R_+$ satisfies \red{$t(n) = o(\log n)$}
        \item $\lim_{n \to \infty} \alpha(n) = 0$
    \end{itemize}
    \item Now $p(n) = o(1)$
    \item[]
    \pause
    \item If $(\*G', \*K) \sim \+G(n, p(n), k(n))$, then $\E{\omega(\*G')} = O(t(n))$ (Theorem 3.2)
    \item The planted clique is the unique maximum clique
\end{itemize}
\end{frame}

\begin{frame}
\ft{Generalized Planted Clique Problem}
\fst{Universal Quasi-Polynomial Algorithm}
\begin{itemize}
    \item Theorem 3.5 and Corollary 3.6 prove that we can recover the whole planted clique given a small subset of the planted clique using Algorithm 3-2
    \item By enumerating all $4 \log n$-sized subset of vertices then applying Algorithm 3-2 with some checking (Algorithm 3-3), we can find the planted clique in time $n^{\Theta(\log n)}$
\end{itemize}
\end{frame}

\begin{frame}
\ft{$\*P$-Time Algorithms for Planted Clique}
\fst{$\*G \sim \+G(n, \frac{1}{2}, k(n))$}
For different $k(n)$:
\begin{tabular}{|c|c|c|}
\hline
$\Omega(\sqrt{n \log n})$ & Highest degree vertices & \blue{[Ku\v cera 95]} \\ \hline
\multirow{7}{*}{$\Omega(\sqrt{n})$} & \multirow{2}{*}{Spectral Method} & \blue{[Alon 98]} \\
& & \blue{[McSherry '01]} \\ \cline{2-3}
& \multirow{2}{*}{Combinatorial Method} & \blue{[Feige '10]} \\
& & \blue{[Dekel '11]} \\ \cline{2-3}
& Lov\'asz Theta Function & \blue{[Feige '00]} \\ \cline{2-3}
& Nuclear Norm Min & \blue{[Ames '11]} \\ \cline{2-3}
& Belief Propagation & \blue{[Deshpande '13]} \\ \hline
\end{tabular}
\end{frame}


\begin{frame}
\ft{Algorithm for Relatively Small Planted Clique}
\begin{theorem}
For a sufficiently large constant $c
\in \mathbb{N}$, with probability at least
$\frac{2}{3}$ we can find the planted clique of size $k(n)
= \red{c\sqrt{n \cdot p(n) \cdot \tp{1-p(n)}}}$ in $\+G(n, p(n), k(n))$, if $p(n) =
n^{-\alpha(n)}$ satisfies the following conditions:
\begin{itemize}
\item $\alpha(n) = \frac{1}{t(n)}$, where $t: \-N \to \-R_+$ satisfies \red{$t(n) = o(\log n)$}
\item $\lim_{n \to \infty} \alpha(n) = 0$
\end{itemize}
\end{theorem}
Note that now $k(n) = o(\sqrt{n})$
\end{frame}

\begin{frame}
\ft{Algorithm for Relatively Small Planted Clique}
\begin{itemize}
\item Algorithm 4-4: very simple
\begin{itemize}
    \item Delete lowest degree vertex until the graph becomes a clique
    \item Add back common neighbours
\end{itemize}
\pause
\item Analysis: convoluted, stage by stage
\begin{itemize}
    \item We will reach a step where number of vertices outside the planted clique is $\frac{1}{c} \cdot k(n)$, while number of vertices inside is at least $\frac{4}{5} \cdot k(n)$
    \item Then all vertices outside will be removed and all vertices inside will remain
    \item We will only add those vertices inside the planted clique
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\ft{$\mathbf{AC}^0[q]$ Circuit Lower Bounds}
\begin{itemize}
\item $\mathbf{AC}^0[q]$: constant-depth, polynomial-size, unbounded fan-in boolean circuits with $\mathrm{MOD}_q$ gates for prime $q$
\pause
\item Tried to give $\mathbf{AC}^0[q]$ circuit lower bounds for $\mathrm{PC}_D(p(n), k(n))$ using Razborov-Smolensky method:
\begin{itemize}
    \item proved that there are \blue{low degree} polynomial approximating $\mathbf{AC}^0[q]$ circuit over the planted clique distribution (Theorem 5.10)
    \item but failed to move further
\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\ft{Future Works}
\begin{itemize}
    \item Better Algorithms
    \begin{itemize}
        \item for smaller $k(n)$
        \item improve success probability to $1-o(1)$
    \end{itemize}
    \item Circuit Lower Bounds
	\item Hardness result for very small $p(n)$
	\begin{itemize}
		\item reductions from not-that-small $p(n)$ to very small $p(n)$
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}
\centering \Large
\textsc{Thank you}\\
\textsc{Q \& A}
\end{frame}
\end{document}