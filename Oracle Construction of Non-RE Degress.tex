\documentclass{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}


\usepackage{beamerthemesplit}
\usetheme{Warsaw}

\newcommand{\ma}[1]{\ensuremath{#1}}

\newcommand{\la}{\langle}
\newcommand{\ra}{\rangle}

\newcommand{\Zero}{\mathbf{0}}
\newcommand{\Zerop}{\mathbf{0'}}

\newcommand{\A}{\mathbf{a}}
\newcommand{\B}{\mathbf{b}}
\newcommand{\C}{\mathbf{c}}
\newcommand{\D}{\mathbf{d}}

\newcommand{\dom}{\mathrm{dom}~}
\newcommand{\concat}{\widehat{\phantom{a}}}


\newcommand{\bra}[1]{\langle #1\rangle}
\newcommand{\sqbra}[1]{[ #1]}
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\eps}{\varepsilon}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\blue}[1]{{\color{blue} #1}}

\newcommand{\ft}[1]{\frametitle{\textsc{#1}}}
\newcommand{\fst}[1]{\framesubtitle{\textsc{#1}}}

\title{Oracle Construction of Non-R.E. Degrees}
\author{Xuangui Huang}

\begin{document}
\frame{\titlepage}
\begin{frame}
    \ft{Overview}
    \fst{A More Basic Problem}
    \begin{itemize}
        \item \textsc{Post's Problem}: whether there exists an \red{r.e.} degree between $\Zero$ and $\Zerop$? 
        \item A more basic problem is whether there exist \red{any} degrees between $\Zero$ and $\Zerop$.  \pause
            \begin{itemize}
                \item Positive answer!
                \item Kleene and Post, 1954, "The upper semi-lattice of degrees of recursive unsolvability" 
                \item By constructing Turing incomparable sets $A, B \leq_T \varnothing'$ using \red{diagonalisation method}
            \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{Overview}
    \fst{Diagonalisation Method}
    \begin{itemize}
        \item We have an infinite sequence $\{R_e\}_{e \in \omega}$ of conditions called \blue{requirements} to satisfy.
        \item The desired sets, or their characteristic functions are constructed stage by stage.
        \item At each stage of the construction we will meet a single requirement, and this requirement will be satisfied from then on.
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{Overview}
    \fst{Oracle Construction of Non-R.E. Degrees}
    \begin{itemize}
        \item \blue{$C$-recursive oracle construction}
            \begin{itemize}
                \item In this chapter the construction in each stage will be \red{nonrecursive}, requiring some oracle $C$, such as $\varnothing'$.
            \end{itemize}
        \item Therefore the sets constructed in this chapter are not r.e. \pause
        \item We will extend the constructed function at each stage.
            \begin{itemize}
                \item \blue{finite extension} construction: $\dom f_s$ is finite for all $s$
                \item \blue{infinite extension} construction: otherwise
            \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{Overview}
    \begin{enumerate}
        \item[1] A Pair of Incomparable Degrees Below $\Zerop$
            \begin{itemize}
                %\item construct $\A, \B \leq_T \varnothing'$ such that $\A | \B$
                \item finite extension $\varnothing'$-oracle construction
            \end{itemize}
        \item[3] Inverting the Jump
            \begin{itemize}
                %\item given $\B \geq 0'$, construct $\A$ with $\A' = \A \cup \Zerop = \B$
                \item finite extension $B$-recursive construction
            \end{itemize}
        \item[2] Avoiding Cones of Degrees
            \begin{itemize}
                %\item given $\B \geq 0$, construct $\A < \B'$ with $\A | \B$
                \item finite extension $B'$-recursive construction
            \end{itemize}
        \item[4] Upper and Lower Bounds for Degrees
            \begin{itemize}
                %\item given ascending sequence $\{\A_n\}_{n \in \omega}$, construct exact pair $\B$ and $\C$
                \item infinite extension $((A^{[<s]})' \oplus A_s)$-recursive construction
            \end{itemize}
    \end{enumerate}
\end{frame}

\begin{frame}
    \ft{A Pair of Incomparable Degrees Below $\Zerop$}
    \begin{theorem}[1.2]
        There exist degrees $\A,\B \leq \Zerop$ such that $\A | \B$, i.e. $\A$ is incomparable with $\B$, $\A \not\leq_T \B$ and $\B \not\leq_T \A$.
    \end{theorem}
    \begin{corollary}
        The above $\A,\B$ satisfy that $\Zero < \A, \B < \Zerop$.
    \end{corollary}
\end{frame}

\begin{frame}
    \ft{Finite Extension $\varnothing'$-oracle Construction}
    \begin{itemize}
        \item Construct $f_s$ and $g_s$ at each stage $s$, let $c_A = \bigcup_s f_s$ and $c_B = \bigcup_s g_s$.
        \item $f_s$ and $g_s$ are finite binary strings viewed as partial functions.
        \item $f_s \subset f_{s+1}$ and $g_s \subset f_{s+1}$ hence $c_A$ and $c_B$ are total functions. \pause
        \item Requirements:
        \begin{equation*}
            R_e \colon A \neq \phi_e^B
        \end{equation*}
        \begin{equation*}
            S_e \colon B \neq \phi_e^A
        \end{equation*}
        \pause
        \item As usual, at \blue{stage $0$} we initiate with $f_0 = g_0 = \varnothing$.
    \end{itemize}
\end{frame}

\begin{frame}
    \ft{Finite Extension $\varnothing'$-oracle Construction}
    \fst{Satisfying $R_e$ and $S_e$}
    \begin{itemize}
        \item At \blue{stage $s+1 = 2e + 1$}, we satisfy $R_e \colon A \neq \phi_e^B$: 
            \begin{itemize}
                \item Let $n = |f_s| = \mu x(x \notin \dom f_s)$
                \item Using a $\varnothing'$-oracle test whether
                \begin{equation} \label{eqn:1}
                    (\exists t)(\exists \sigma)(g_s \subset \sigma \wedge \red{\phi_{e,t}^\sigma(n)} \downarrow)
                \end{equation}
                (Note that $g_s \subset \sigma$ is recursive on $\sigma$)
                    \begin{itemize}
                        \item True: Choose the least $\la \sigma, t \ra$ satisfying the matrix of (\ref{eqn:1}). Set $g_{s+1} = \sigma$ and $f_{s+1} = f_s \concat (1 - \phi_{e,t}^\sigma(n))$
                        \item False: $f_{s+1} = f_s \concat 0$ and $g_{s+1} = g_s \concat 0$ 
                    \end{itemize}
            \end{itemize}
        \pause
        \item At \blue{stage $s+1 = 2e + 2$}, we satisfy $S_e \colon B \neq \phi_e^A$: similarly with the roles of $f_s$ and $g_s$ interchanged.
    \end{itemize}
\end{frame}

\begin{frame}
    \ft{Finite Extension $\varnothing'$-oracle Construction}
    \fst{Correctness}
    \begin{itemize}
        \item At each stage $s$,
            \begin{itemize}
                \item $|f_s| \geq s$ and $|g_s| \geq s$, hence $c_A$ and $c_B$ are total;
                \item in either cases, \[ f_{s+1}(n) \not\simeq \phi_e^{g_{s+1}}(n)\] since $f_{s+1} \subset c_A$ and $g_{s+1} \subset c_B$, \[c_A(n) \not\simeq \phi_e^{c_B}(n).\]
            \end{itemize}
            \qed
            \pause
        \item Remark: indeed we don't have to do anything when the test fails, since there are infinitely many stages $s$ with $f_s \subset f_{s+1}$.
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{Generalized Results}
    \begin{theorem}[1.3]
        For any degree $\C$, there are degrees $\A, \B$ such that $\C \leq \A, \B \leq \C'$ and $\A | \B$.
    \end{theorem}
    \pause
    \begin{definition}[1.4]
    A countable sequence of sets $\{A_i\}_{i \in \omega}$ is \blue{recursively independent} if for each $i$, $A_i \not\leq_T \oplus \{ A_j \colon j \neq i\}$.
    \end{definition}
    \begin{theorem}[1.5]
    There exists a recursively independent sequence of sets $\{A_i\}_{i \in \omega}$ each recursive in $\varnothing'$.
    \end{theorem}
\end{frame}

\begin{frame}
    \ft{Inverting the Jump}
    \begin{itemize}
        \item Any Turing jump is above $\Zerop$.
        \item The Turing jump map has range contained in $\{\B \colon \B \geq \Zerop\}$.
        \item The following theorem shows that this map is onto. 
    \end{itemize}
    \begin{theorem}[3.1]
        For every degree $\B \geq \Zerop$, there is a degree $\A$ such that $\A' = \A \cup \Zerop = \B$.
    \end{theorem}
\end{frame}

\begin{frame}
    \ft{Finite Extension $B$-recursive Construction}
    \begin{itemize}
        \item Fix $B \in \B \geq \Zerop$, we must construct $A$ with \[A' \equiv_T A \oplus \varnothing' \equiv_T B\].
        \item We will construct $c_A = \bigcup_s f_s$ using a finite extension $B$-recursive construction.
    \end{itemize}
\end{frame}

\begin{frame}
    \ft{Finite Extension $B$-recursive Construction}
    \begin{itemize}
        \item At \blue{stage $0$}, set $f_0 = \varnothing$.
        \item At \blue{stage $s+1 = 2e+1$}, (we decide whether $e \in A'$) we satisfy the following requirement: \[ R_e \colon (\exists \sigma \subset c_A)(\phi_e^\sigma(e) \downarrow \vee (\forall \tau \supseteq \sigma, \phi_e^\tau(e) \uparrow)).\]
        Using a $\varnothing'$-oracle test whether
        \begin{equation} \label{eqn:2}
            (\exists t)(\exists \sigma)(f_s \subset \sigma \wedge \phi_{e,t}^\sigma(e) \downarrow)
        \end{equation}
        \begin{itemize}
            \item True: let $f_{s+1} = $ the least such $\sigma$.
            \item False: set $f_{s+1} = f_s$.
        \end{itemize} \pause
        \item At \blue{stage $s+1 = 2e+2$}, (we code $B(e)$ into $A$) Let $f_{s+1} = f_s \concat B(e)$.
    \end{itemize}
\end{frame}

\begin{frame}
    \ft{Correctness}
    \begin{itemize}
        \item $|f_{2e}| \geq e$, hence $c_A$ is total.
        \item The construction is $B$-recursive:
            \begin{itemize}
                \item at odd stage we use a $\varnothing'$-oracle,
                \item at even stage we use a $B$-oracle.
            \end{itemize} \pause
        \item We just have to prove the following conditions are met:
            \begin{itemize}
                \item $A' \leq_T B$:
                    \begin{itemize}
                        \item To decide whether $e \in A'$, $B$-recursively compute $f_{2e}$, then $B$-recursively test whether (\ref{eqn:2}) holds.
                    \end{itemize}
                \item $B \leq_T A \oplus \varnothing'$:
                    \begin{itemize}
                        \item To compute $B(e)$, just compute $f_s$ till stage $2e+2$. $f_s$ is $(A \oplus \varnothing')$-recursive for all $s$.
                    \end{itemize}
            \end{itemize}
    \end{itemize}
    \qed
\end{frame}

\begin{frame}
    \ft{More Results}
    \begin{corollary}[3.3]
        For every $n \geq 1$ and every degree $\C$, 
        \[F_n(\C) \colon (\forall \B)(\B \geq \C^{(n)} \Rightarrow (\exists \A)(\A \geq \C \wedge     \A^{(n)} = \A \cup \C^{(n)} = \B))\]
    \end{corollary}
\end{frame}

\begin{frame}
    \ft{More Results}
    \begin{theorem}[3.4]
        For all degree $\B \geq \Zerop$, there exists degrees $\A_0, \A_1$ such that 
        $\A_0 | \A_1$ and \[
                \A'_0 = \A_0 \cup \Zerop = \B = \A_1 \cup \Zerop = \A'_1.
        \]
    \end{theorem}
    \pause
    \begin{corollary}[3.5]
        There is a degree $\A$ such that \[ \Zero < \A < \Zerop \wedge \A' = \Zerop\]
    \end{corollary}
    Turing jump map is not $1:1$.
\end{frame}

\begin{frame}
    \ft{Avoiding Cones of Degrees}
    \begin{theorem}[2.1]
        For every degree $\B > \Zero$ there exists a degree $\A < \B'$ such that $\A | \B$.
    \end{theorem}
    \begin{itemize}
        \item $\A$ must avoid the \blue{lower cone} of degrees $\{ \D \colon \D \leq \B \}$ and the \blue{upper cone} $\{ \D \colon \D \geq \B \}$. \pause
        \item We have the same requirements $\{R_e\}_{e \in \omega}$ and $\{S_e\}_{e \in \omega}$ to satisfy as in the first construction.
        \item The lower cone is easier to avoid than what we've done in the first construction, while the upper cone needs more tricky requirements.
    \end{itemize}
\end{frame}

\begin{frame}
    \ft{Finite Extension $B'$-recursive Construction}
    \fst{Satisfying $R_e$ and $S_e$}
    \begin{itemize}
        \item At \blue{stage $0$}, set $f_0 = \varnothing$.
        \item At \blue{stage $s+1 = 2e + 1$}, we satisfy $R_e \colon A \neq \phi_e^B$: let $n = |f_s|$, using a $B'$-oracle test whether $\phi_e^B(n) \downarrow$:
            \begin{itemize}
                \item True: $f_{s+1} = f_s \concat (1 -\phi_e^B(n))$
                \item False: $f_{s+1} = f_s \concat 0$
            \end{itemize} \pause
        \item At \blue{stage $s+1 = 2e + 2$}, we satisfy $S_e \colon B \neq \phi_e^A$: using a $\varnothing'$-oracle test whether
            \begin{equation} \label{eqn:3}
                (\exists \sigma, \tau)(\exists x,y,z)(\exists t) (f_s \subset \sigma, \tau \wedge \phi_{e,t}^\sigma(x) \downarrow = y \neq z = \phi_{e,t}^\tau(x) \downarrow).
            \end{equation}
            \begin{itemize}
                \item True: then one of $y,z$ must differ from $B(x)$, choose $f_{s+1}$ to be the first $\sigma \supset f_s$ such that $\phi_e^\sigma(x) \downarrow \neq B(x)$
                \item False: let $f_{s+1} = f_s \concat 0$
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \ft{Correctness}
    \begin{itemize}
        \item The construction is $B'$-recursive.
        \item Even if test (\ref{eqn:3}) fails, requirement $S_e \colon B \neq \phi_e^A$ is satisfied: 
        \[ (\forall f \supset f_s) (\phi_e^f = g \mbox{ is total } \Rightarrow g \mbox{ is recursive}).\]
        \begin{itemize}
            \item To recursively compute $g(x)$, find the least $\sigma \supset f_s$ with $\phi_e^\sigma(x) \downarrow$, then $\phi_e^{c_A}(x)$ must be the same value.
        \end{itemize}
        Hence for such $f$, $\phi_e^f \neq B$ since $B >_T \phi$.
    \end{itemize}
    \qed
\end{frame}

\begin{frame}
    \ft{More Result}
    \begin{theorem}[2.2]
        Let $\{ \B_n\}_{n \in \omega}$ be any countable sequence of nonrecursive degrees, then there exist pairwise incomparable degrees $\{ \A_n\}_{n \in \omega}$ such that $\A_n | \B_m$ for all $n,m \in \omega$.
    \end{theorem}
\end{frame}

\begin{frame}
    \ft{Upper and Lower Bounds for Degrees}
    \begin{itemize}
        \item Any finite set of degrees has a least upper bound.
        \item We will show this is not true for greatest lower bound.
    \end{itemize}
    \pause
    \begin{definition}
        A sequence of degrees $\{ \A_n\}_{n \in \omega}$ is \blue{ascending} if $\A_n < \A_{n+1}$ for all $n$.
    \end{definition}
\end{frame}

\begin{frame}
    \ft{Upper and Lower Bounds for Degrees}
    \begin{theorem}[4.2]
        For any ascending sequence $\{ \A_n\}_{n \in \omega}$ of degrees there exists upper bounds $\B$ and $\C$ (called \blue{exact pair}) such that 
        \[(\forall \D) ((\D \leq \B \wedge \D \leq \C) \Rightarrow (\exists n) (\D \leq \A_n)).\]
    \end{theorem}
    \pause
    Its corollaries seem to be more natural in description.
    \begin{corollary}[4.4]
        There are degrees $\B$ and $\C$ with no greatest lower bound.
    \end{corollary}
    \begin{corollary}[4.3]
        An ascending sequence has no least upper bound.
    \end{corollary}
\end{frame}

\begin{frame}
    \ft{Notations}
    \begin{definition}[4.5]
        For any set $A \subseteq \omega$ define the \blue{$y$-section} of $A$ as 
        \[ A^{\sqbra{y}} = \{ \bra{x,y} \colon \bra{x,y} \in A\}, \]
        and define 
        \[ A^{[<y]} = \{ \bra{x,z} \colon \bra{x,z} \in A \wedge z < y\}.\]
    \end{definition}
    \begin{definition}[4.6]
        A set $B \subset A$ is a \blue{thick} subset of $A$ if for every $y$ the following thickness requirement is met:
        \[ T_y \colon B^{[y]} =^* A^{[y]} ,\]
        where  $X =^* Y$ denotes that the size of their symmetric difference $|(X - Y) \cup (Y - X)|$ is finite.
    \end{definition}
\end{frame}

\begin{frame}
    \ft{Notations}
    \begin{definition}[4.7]
        Two partial functions $\phi$ and $\psi$ are \blue{compatible} (written \blue{compat($\phi$,$\psi$)}) if $\phi(x) = \psi(x)$ for all $x$ such that $\phi(x) \downarrow$ and $\psi(x) \downarrow$.
    \end{definition}
\end{frame}

\begin{frame}
    \ft{Infinite Extension $((A^{[<s]})' \oplus A_s)$-recursive Construction}
    Fix a sequence of sets $\{A_i\}_{i \in \omega}$ with $A_i \in \A_i$, Let
    \[ A = \{ \bra{x,i} | x \in A_i\} .\]
    We will use an infinite extension construction to construct $f_s$ and $g_s$, namely $B$ and $C$. 
\end{frame}

\begin{frame}
    \ft{Infinite Extension $((A^{[<s]})' \oplus A_s)$-recursive Construction}
    At \blue{stage $s + 1$}:
    \begin{enumerate}
        \item[Step $1$] for $\bra{e,i} = s$, we satisfy the following requirement:
            \[ R_{\bra{e,i}} \colon \phi_e^B = \phi_i^C = h \mbox{ is total } \Rightarrow (\exists y)( h \leq_T A_y) .\]
            We test whether
            \begin{align} \label{eqn:4}
                (\exists \sigma, \tau)(\exists x)(\exists t)&(compat(f_s, \sigma) \wedge compat(g_s, \tau) \notag \\
                & \wedge \phi_{e,t}^\sigma(x) \downarrow \neq \phi_{i,t}^\tau(x) \downarrow)
            \end{align}
            \begin{itemize}
                \item True: let $\sigma, \tau$ be the first such strings, let $\hat{f} = f_s \cup \sigma$, $\hat{g} = g_s \cup \tau$.
                \item False: let $\hat{f} = f_s$, $\hat{g} = g_s$.
            \end{itemize}
    \end{enumerate}
\end{frame}
\begin{frame}
    \ft{Infinite Extension $((A^{[<s]})' \oplus A_s)$-recursive Construction}
    At \blue{stage $s + 1$}:
    \begin{enumerate}
        \item[Step $2$] We satisfy the thickness requirement $T^B_s$ and $T^C_s$ for $B$ and $C$: 
        \[ T^B_s \colon: B^{[s]} =^* A^{[s]} \]
        \[ T^C_s \colon: C^{[s]} =^* A^{[s]} \]
        For all $x \in \omega^{[s]} - \dom \widehat{f}$ define $f_{s+1}(x) = A(x)$, and define $f_{s+1}(x) = \widehat{f}(x)$ for all $x \in \dom \widehat{f}$. Similarly for $g_{s+1}$.
    \end{enumerate}
\end{frame}
\begin{frame}
    \ft{Correctness}
    \begin{itemize}
        \item We can prove inductively the following claims, hence $A_s \equiv_T B^{[s]} \leq_T B$, and likewise for $C$:
            \begin{itemize}
                \item on $\omega^{[<s]}$, $f_s$ and $g_s$ are defined
                \item $B^{[s]} =^* C^{[s]} =^* A^{[s]}$
                \item $\dom f_s - \omega^{[<s]} =^* \varnothing =^* \dom g_s - \omega^{[<s]}$
            \end{itemize}
        \item Step 1 requires an $(A^{[<s]})'$ oracle
            \begin{itemize}
                \item $compat(f_s, \sigma)$ is an $A^{[<s]}$-recursive relation on $\sigma$, since $f_s \equiv_T A^{[<s]} \equiv_T g_s$
            \end{itemize}
        \item Even if (\ref{eqn:4}) fails, if $\phi^B_e = \phi^C_i = h$ is total, then $h \leq_T A^{[<s]} \leq_T A_s$ for $s = \bra{e,i}$:
            \begin{itemize}
                \item to $A^{[<s]}$-recursively compute $h(x)$, find the least $\sigma$ such that $compat(f_s,\sigma)$ and $\phi_e^\sigma(x) \downarrow$, then $\phi^B_e(x)$ must be the same value.
            \end{itemize}
            \qed
    \end{itemize}
\end{frame}

\begin{frame}
    \ft{Summary}
    \begin{enumerate}
        \item[1] A Pair of Incomparable Degrees Below $\Zerop$
            \begin{itemize}
                \item construct $\A, \B \leq_T \varnothing'$ such that $\A | \B$
                \item finite extension $\varnothing'$-oracle construction
            \end{itemize}
        \item[3] Inverting the Jump
            \begin{itemize}
                \item given $\B \geq 0'$, construct $\A$ with $\A' = \A \cup \Zerop = \B$
                \item finite extension $B$-recursive construction
            \end{itemize}
        \item[2] Avoiding Cones of Degrees
            \begin{itemize}
                \item given $\B > 0$, construct $\A < \B'$ with $\A | \B$
                \item finite extension $B'$-recursive construction
            \end{itemize}
        \item[4] Upper and Lower Bounds for Degrees
            \begin{itemize}
                \item given ascending sequence $\{\A_n\}_{n \in \omega}$, construct exact pair $\B$ and $\C$
                \item infinite extension $((A^{[<s]})' \oplus A_s)$-recursive construction
            \end{itemize}
    \end{enumerate}
\end{frame}
\begin{frame}
 \begin{center}
   Thank you!
 \end{center}
\end{frame}
\end{document}

