\documentclass[aspectratio=169]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}


%\usepackage{beamerthemesplit}
%\usepackage[orientation=landscape,size=custom,width=16,height=9,scale=0.5,debug]{beamerposter}
%\usetheme{Antibes}
\usecolortheme{spruce}

\newcommand{\ma}[1]{\ensuremath{#1}}

\newcommand{\T}{\ma{\mathsf{T}}}

\newcommand{\bra}[1]{\langle #1\rangle}
\newcommand{\sqbra}[1]{[ #1]}
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\eps}{\varepsilon}
\newcommand<>{\red}[1]{{\color#2{red} #1}}
\newcommand<>{\blue}[1]{{\color#2{blue} #1}}
\newcommand<>{\green}[1]{{\color#2[rgb]{0,0.5,0} #1}}

\newcommand{\ft}[1]{\frametitle{\textsc{#1}}}
\newcommand{\fst}[1]{\framesubtitle{\textsc{#1}}}

\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}
\newtheorem{remark}{Remark}

\defbeamertemplate*{title page}{customized}[1][]
{
  \usebeamerfont{title}\inserttitle\par
  \bigskip
  \usebeamerfont{subtitle}\insertsubtitle\par
  \bigskip
  \bigskip
  \usebeamerfont{author}\blue{\insertauthor}\par
  \usebeamerfont{institute}\blue{\insertinstitute}\par
  \usebeamerfont{date}\blue{\insertdate}\par
}
\beamertemplatenavigationsymbolsempty

\title{Derandomization}
\subtitle{Under Non-uniform and Uniform assumptions}
\author{Talk given by Xuangui Huang}
\date{March 18, 2015}

\begin{document}
\frame{\titlepage}
\begin{frame}
\centering
\Large
\textsc{Review}
\end{frame}

\begin{frame}
	\ft{Pseudorandom Generators}
	\fst{From Average-Case Hardness}
	Last lecture mainly talks about how to construct a pseudorandom generator from a function that is hard on average case.
\end{frame}

\begin{frame}
	\ft{Pseudorandom Generators}
	\begin{definition}[Pseudorandom Distribution]
		A distribution $R$ over $\{0,1\}^m$ is \blue{$(S,\epsilon)$-pseudorandom} for some $S \in \mathbb{N}$ and $\epsilon > 0$ if for every circuit $C$ of size at most $S$ \[\vert \Pr_{x \in_{R} \{0,1\}^m}[C(x) = 1] - \Pr_{x \in_{U_m} \{0,1\}^m}[C(x)=1]\vert < \epsilon \]
		where $U_m$ denotes the uniform distribution over $\{0,1\}^m$.
	\end{definition}
\end{frame}

\begin{frame}
	\ft{Pseudorandom Generators}
	\begin{definition}[Pseudorandom Generators]
		Let $S: \mathbb{N} \rightarrow \mathbb{N}$ be some \red{time-constructible} and \red{non-decreasing} function. A \red{$2^n$-time} computable function $G: \{0,1\}^* \rightarrow \{0,1\}^*$ is an \blue{$S(\ell)$-pseudorandom generator} if:
		\begin{itemize}
			\item $|G(z)| = S(|z|)$ for every $z \in \{0,1\}^*$
			\item for every $\ell \in \mathbb{N}$ the distribution $G(U_{\ell})$ is $(S(\ell)^{\green{3}}, \green{\frac{1}{10}})$-pseudorandom.
		\end{itemize}
		\centering\includegraphics[scale=0.2]{pseudorandom-generator.png}
	\end{definition}
\end{frame}

\begin{frame}
	\ft{Pseudorandom Generators}
	\fst{From Average-Case Hardness}
	\begin{definition}[$\rho$-Average Case Hardness]
		For $f: \{0,1\}^* \rightarrow \{0,1\}$ and $\rho \in [0,1]$, we define the \blue{$\rho$-average case hardness} of $f$ as 
		\begin{align*}
			\mathtt{H_{avg}^{\rho}}(f)(n) &\triangleq \max\{S|\Pr_{x \in_U \{0,1\}^n}[C(x) = f_n(x)] < \rho, \forall C \text{ with } |C| \leq S\} \\
			&= \min\{S|\Pr_{x \in_U \{0,1\}^n}[C(x) = f_n(x)] \geq \rho, \exists C \text{ with } |C| \leq S\} - 1.
		\end{align*}
	\end{definition}
	\begin{definition}[Average-Case Hardness]
		For $f: \{0,1\}^* \rightarrow \{0,1\}$, we define the \blue{average-case hardness} of $f$ as 
		\[\mathtt{H_{avg}}(f)(n) = \max\{S|\mathtt{H_{avg}^{\red{\frac{1}{2}+\frac{1}{S}}}}(f)(n) \geq S \} .\]
	\end{definition}
\end{frame}

\begin{frame}
	\ft{Pseudorandom Generators}
	\fst{From Average-Case Hardness}
	\begin{theorem}[PRGs From Average-Case Hardness]
		Let $S: \mathbb{N} \rightarrow \mathbb{N}$ be \red{time-constructible} and \red{non-decreasing}. If there exists $f \in \mathbf{DTIME}(\red{2^{O(n)}})$ such that $\mathtt{H_{avg}}(f)(n) \geq S(n)$ for every $n$, then there exists an 
		\green{$S(\delta \ell)^{\delta}$}-pseudorandom generator for some constant $\delta > 0$.
	\end{theorem}
\end{frame}

\begin{frame}
	\ft{Derandomization}
	\fst{Using Pseudorandom Generators}
	\begin{lemma}[20.3]
		Suppose there exists an $S(\ell)$-pseudorandom generator $G$ for a time-constructible and nondecreasing $S: \mathbb{N} \rightarrow \mathbb{N}$. Then for every polynomial-time computable function $\ell: \mathbb{N} \rightarrow \mathbb{N}$, we have $\mathbf{BPTIME}(S(\ell(n))) \subseteq \mathbf{DTIME}(2^{c\ell(n)})$ for some constant $c$.
	\end{lemma}
	\only<3>{
	\begin{definition}[$\mathbf{BPTIME}$]
		A language $L$ is in $\mathbf{BPTIME}(S(\ell(n))$ if there is a \red{deterministic} algorithm $A$ that on input $x \in \{0,1\}^n$ runs in time $c'S(\ell(n))$ for some constant $c'$, and satisfies 
		\[\Pr_{r \in_R \{0,1\}^m}[A(x,r) = L(x)] \geq \frac{2}{3},\] where $m \leq c'S(\ell(n))$ and we define $L(x) = 1$ if $x \in L$ and $L(x) = 0$ otherwise.
	\end{definition}}
	\only<4-6>{
	\begin{proof}
		\begin{itemize}
			\item Enumerate all $z \in \{0,1\}^{\ell(n)}$, compute $A(x,G(z))$, and output the \red{majority} answer.
			\item<6-> By hard-wiring answers for small $n$, we can prove $L \in \mathbf{DTIME}(2^{c\ell(n)})$. 
		\end{itemize}
		\only<5->{
		\begin{claim}
			\blue{For $n$ sufficiently large}, the fraction of $z$ such that $A(x,G(z)) = L(x)$ is at least $\frac{2}{3} - \frac{1}{10}$.
		\end{claim}
		}
	\end{proof}
	}
	\only<7->{
		\begin{claim}
			\blue{For $n$ sufficiently large}, the fraction of $z$ such that $A(x,G(z)) = L(x)$ is at least $\frac{2}{3} - \frac{1}{10}$.
		\end{claim}
		\begin{proof}
			\begin{itemize}
				\item For contradiction, suppose there exists an infinite sequence of ``bad'' $x$'s.
				\item<8-> Use Cook-Levin Theorem to construct a circuit computing the function $r \mapsto A(x,r)$, \only<8>{where $x$ is hard-wired, }\only<9->{with size $O(S(\ell(n))) \log O(S(\ell(n))) \leq S(\ell(n))^3$, a distinguisher!}
			\end{itemize}
		\end{proof}
	}
\end{frame}

\begin{frame}
	\ft{Derandomization}
	\fst{Using Pseudorandom Generators}
	\begin{lemma}[20.3]
		Suppose there exists an $S(\ell)$-pseudorandom generator $G$ for a time-constructible and nondecreasing $S: \mathbb{N} \rightarrow \mathbb{N}$. Then for every polynomial-time computable function $\ell: \mathbb{N} \rightarrow \mathbb{N}$, we have $\mathbf{BPTIME}(S(\ell(n))) \subseteq \mathbf{DTIME}(2^{c\ell(n)})$ for some constant $c$.
	\end{lemma}
	\begin{corollary}[20.4]
		\begin{enumerate}
			\item If there exists a $2^{\frac{\ell}{c}}$-pseudorandom generator for some constant $c \in \mathbb{N^+}$ then $\mathbf{BPP} = \mathbf{P}$.
			\item If there exists a $2^{\ell^{\frac{1}{c}}}$-pseudorandom generator for some constant $c \in \mathbb{N^+}$ then $\mathbf{BPP} \subseteq \mathbf{QuasiP} = \mathbf{DTIME}(2^{polylog(n)})$.
			\item If for every $c > 1$ there exists an $\ell^c$-pseudorandom generator, then $\mathbf{BPP} \subseteq \mathbf{SUBEXP} = \cap_{\epsilon > 0} \mathbf{DTIME}(2^{n^{\epsilon}})$.
		\end{enumerate}
	\end{corollary}
\end{frame}

\begin{frame}
	\ft{Derandomization}
	\fst{Under Worst-Case Hardness}
	\begin{theorem}[20.7]
		\begin{enumerate}
			\item If there exists $f \in \mathbf{DTIME}(2^{O(n)})$ and $c \in \mathbb{N^+}$ such that $\mathtt{H_{wrs}}(f) \geq 2^{\frac{n}{c}}$, then $\mathbf{BPP} = \mathbf{P}$.
			\item If there exists $f \in \mathbf{DTIME}(2^{O(n)})$ and $c \in \mathbb{N^+}$ such that $\mathtt{H_{wrs}}(f) \geq 2^{n^{\frac{1}{c}}}$, then $\mathbf{BPP} \subseteq \mathbf{QuasiP} = \mathbf{DTIME}(2^{polylog(n)})$.
			\item If there exists $f \in \mathbf{DTIME}(2^{O(n)})$ and $\mathtt{H_{wrs}}(f) \geq n^{\red{\omega(1)}}$, then $\mathbf{BPP} \subseteq \mathbf{SUBEXP} = \cap_{\epsilon > 0} \mathbf{DTIME}(2^{n^{\epsilon}})$.
		\end{enumerate}
	\end{theorem}
	\pause
	\begin{remark}
		We can replace $\mathbf{E}$ with $\mathbf{EXP} = \mathbf{DTIME}(2^{poly(n)})$ in $2$ and $3$ above: \pause
			\begin{itemize}
				\item For $f \in \mathbf{DTIME}(2^{n^{c'}})$, just let $g$ be the function that on input $x$ outputs $f$ applied to the first $|x|^{\frac{1}{c}}$ bits of $x$.
			\end{itemize} 
	\end{remark}
\end{frame}


\begin{frame}
	\ft{Derandomization}
	\fst{Under A Uniform Assumption}
	\begin{itemize}
		\item Previous result uses \blue{circuit lower bound} assumptions, which are notoriously hard to prove. 
		\item<2-> We can get a non-trivial derandomization of $\mathbf{BPP}$ under a \red{uniform} hardness assumption, namely $\mathbf{BPP} \neq \mathbf{EXP}$.
		\only<3>{
		\begin{itemize}
			\item Impagliazzo R, Wigderson A. \textit{Randomness vs. time: de-randomization under a uniform assumption}, FOCS '98.
		\end{itemize}}
		\only<4->{
		\item Randomness vs. Time: the intuition that randomness is a resource basically incomparable to time is wrong:
		\begin{itemize}
			\item Either there is a non-trivial deterministic simulation of $\mathbf{BPP}$, or $\mathbf{BPP} \neq \mathbf{EXP}$.
			\item Either time can non-trivially substitute for randomness, or randomness can non-trivially substitute for time.
		\end{itemize}
		}
	\end{itemize}
\end{frame}

\begin{frame}
	\ft{Derandomization}
	\fst{Under A Uniform Assumption}
	\begin{theorem}[20.16]
		Suppose that $\mathbf{BPP} \neq \mathbf{EXP}$. Then for every $L \in \mathbf{BPP}$ there exists a subexponential($2^{n^{o(1)}}$) time deterministic algorithm $A$ such that for infinitely many $n$'s 
		\[\Pr_{x \in_R \{0,1\}^n}[A(x) = L(x)] \geq 1 - \frac{1}{n} .\] 
	\end{theorem}
\end{frame}

\begin{frame}
\centering
\Large
\textsc{Review} \\
\textsc{Nisan-Wigderson Construction}
\end{frame}

\begin{frame}
	\ft{Unpredictability Implies Pseudorandomness}
	\begin{theorem}[Yao's Theorem]
		Let $Y$ be a distribution over $\{0,1\}^m$, $S > 10m$ and $\epsilon > 0$. If for every circuit $C$ of size at most \red{$2S$} and every $i \in [m]$ we have
		\[ \Pr_{R \in_Y \{0,1\}^m}[C(R_1, \cdots, R_{i-1})=R_i] \leq \red{\frac{1}{2} + \frac{\epsilon}{m}},\]
		then $Y$ is $(S, \epsilon)$-pseudorandom.
	\end{theorem}
\end{frame}

\begin{frame}
	\ft{The Nisan-Wigderson Construction}
	\begin{definition}[NW Generator]
		Let $\mathcal{I} = \{I_1, \cdots, I_{\red{m}}\}$ be a family of subsets of $[\ell]$ with \red{$|I_j| = n$} for every $j$, and let $f:\{0,1\}^{\red{n}} \rightarrow \{0,1\}$. The \blue{$(\mathcal{I},f)$-NW generator} is the function $\mathtt{NW}^f_{\mathcal{I}} : \{0,1\}^{\ell} \rightarrow \{0,1\}^{\red{m}}$ defined as 
		\[\mathtt{NW}^f_{\mathcal{I}}(z) = f(z_{I_1})\circ f(z_{I_2}) \cdots \circ f(z_{I_m}),\]
		where for $z \in \{0,1\}^{\ell}$ and $I \subseteq [\ell]$, $z_I$ denotes the restriction of $z$ to the coordinates in $I$. 
	\end{definition}
	\centering \includegraphics[scale=0.2]{NW-generator.png}
\end{frame}

\begin{frame}
	\ft{The Nisan-Wigderson Construction}
	If $f$ is a hard function and $\mathcal{I}$ is a design with good parameters, then $\mathtt{NW}^f_{\mathcal{I}}(U_{\ell})$ is indeed a pseudorandom distribution by  the following lemma:
	\begin{lemma}[Pseudorandomness Using the NW Generator]
		If $\mathcal{I}$ is an $(\ell,n,d)$-design with $|\mathcal{I}|=2^{\frac{d}{10}}$ and $f:\{0,1\}^n \rightarrow \{0,1\}$ satisfies \red{$\mathtt{H_{avg}}(f) > 2^{2d}$}, then the distribution $\mathtt{NW}^f_{\mathcal{I}}(U_{\ell})$ is $(\frac{\mathtt{H_{avg}}(f)}{10}, \frac{1}{10})$-pseudorandom.
	\end{lemma}
\end{frame}

\begin{frame}
	\ft{The Nisan-Wigderson Construction}
	\begin{lemma}[Pseudorandomness Using the NW Generator]
		If $\mathcal{I}$ is an $(\ell,n,d)$-design with $|\mathcal{I}|=2^{\frac{d}{10}}$ and $f:\{0,1\}^n \rightarrow \{0,1\}$ satisfies \red{$\mathtt{H_{avg}}(f) = S > 2^{2d}$}, then the distribution $\mathtt{NW}^f_{\mathcal{I}}(U_{\ell})$ is $(\frac{S}{10}, \frac{1}{10})$-pseudorandom.
	\end{lemma}
	\begin{proof}
		For contradiction, suppose that there exists a circuit $C$ of size $\frac{S}{5}$ and a number $i \in [2^{\frac{d}{10}}]$ \only<1-3>{ such that}\only<4->{, let $Z_1$ and $Z_2$ be two \red{independent} random variables with \red{$Z_1 = Z_{I_i}$} and \red{$Z_2 = Z_{[\ell]\setminus I_i}$}}\only<5->{ then we have}\only<6->{ a string \red{$z_2 \in \{0,1\}^{\ell-n}$} }\only<5-7>{ such that}\only<8>{, and there exists a circuit $B$ of size \red{$2^{\frac{d}{10}}\cdot d2^d + \frac{S}{5} < S$} such that}
		\only<1-2>{\[\Pr_{R=\mathtt{NW}^f_{\mathcal{I}}(U_{\ell})}[C(R_1, \cdots, R_{i-1}) = R_i] > \frac{1}{2} + \frac{1}{10\cdot 2^{\frac{d}{10}}}.\]}
		\only<2>{\begin{definition}[NW Generator]
				\[\mathtt{NW}^f_{\mathcal{I}}(z) = f(z_{I_1})\circ f(z_{I_2}) \cdots \circ f(z_{I_m}),\]
	\end{definition}}
			\only<3-4>{\[\Pr_{\red{Z \in_{U_{\ell}} \{0,1\}^{\ell}}}[\red{C(f(Z_{I_1}), \cdots, f(Z_{I_{i-1}})) = f(Z_{I_i})}] > \frac{1}{2} + \frac{1}{10\cdot 2^{\frac{d}{10}}}.\]}
			\only<5>{\[\Pr_{\red{Z_1 \sim U_n, Z_2 \sim U_{\ell-n}} }[\red{C(f_1(Z_1,Z_2), \cdots, f_{i-1}(Z_1,Z_2)) = f(Z_1)}] > \frac{1}{2} + \frac{1}{10\cdot 2^{\frac{d}{10}}},\]
			where $f_j$ applies $f$ to the coordinates of $Z_1$ corresponding to $I_j \cap I_i$ and the coordinates of $Z_2$ corresponding to $I_j \setminus I_i$.}
			\only<6-7>{\[\Pr_{\red{Z_1 \sim U_n}} [C(f_1(Z_1,\red{z_2}), \cdots, f_{i-1}(Z_1,\red{z_2})) = f(Z_1)] > \frac{1}{2} + \frac{1}{10\cdot 2^{\frac{d}{10}}}.\]}
			\only<7>{For $j \neq i$, $|I_j \cap I_i| \leq d$, hence the function $Z_1 \longmapsto f_j(Z_1, z_2)$ can be computed by a \red{$d2^d$-sized circuit}.}
			\only<8>{\[\Pr_{Z_1 \sim U_n} [B(Z_1) = f(Z_1)] > \frac{1}{2} + \frac{1}{10\cdot 2^{\frac{d}{10}}} > \frac{1}{2} + \frac{1}{S}.\]}
	\end{proof}
\end{frame}

\begin{frame}
	\ft{Structure of the Proof}
	\begin{itemize}
		\item A presumably \blue{hard function} $f$ is used to construct a \blue{pseudorandom generator} $G$. 
		\item Equivalently, one starts with a hypothetical \blue{distinguisher} (\blue{predictor}) for $G$, constructs from it an \blue{efficient method} to compute $f$, obtaining a contradiction.
	\end{itemize}
\end{frame}

\begin{frame}
	\ft{Main Obstacles}
	``Why wasn't this paper written in 1988?''
	\begin{itemize}
		\item In the non-uniform version, both are \red{circuits}, and only an \red{existence} proof of the efficient method from the distinguisher is required.
		\item In the construction, we need values of $f$ at \red{many} (indeed can be made into \red{random}) points.
		\item[]
		\pause
		\item In the uniform case, both are \red{probabilistic Turing Machines}, you should have an uniformly way to obtain a efficient algorithm to compute $f$.
		\item But it seems impossible to obtain such values efficiently and uniformly for arbitrary function in $\mathbf{EXP}$.
	\end{itemize}
\end{frame}

\begin{frame}
	\ft{Main Ideas}
	\begin{itemize}
		\item Instead, we will show that a distinguisher can be used to learn how to compute the hard function from examples:
		\item[] If an NW-generator based on any $f \in \mathbf{EXP}$ is not pseudo-random, then a circuit for $f$ can be constructed by a probabilistic Turing Machine using $f$ as an oracle.
		\item[]
		\pause
		\item And then we can eliminate the use of the oracle by using a very special $f$, namely the permanent function:
		\[\mathsf{perm}(A) = \sum_{\sigma \in S_n}\prod_{i = 1}^n a_{i, \sigma(i)},\] where $A$ is an $n \times n$ matrix.
	\end{itemize}
\end{frame}

\begin{frame}
	\begin{theorem}[20.16]
		Suppose that $\mathbf{BPP} \neq \mathbf{EXP}$. Then for every $L \in \mathbf{BPP}$ there exists a subexponential($2^{n^{o(1)}}$) time deterministic algorithm $A$ such that for infinitely many $n$'s 
		\[\Pr_{x \in_R \{0,1\}^n}[A(x) = L(x)] \geq 1 - \frac{1}{n} .\] 
	\end{theorem}
	\begin{proof}[Proof Sketch]
		\begin{itemize}
			\item First we may assume $\mathbf{EXP} \subseteq \mathbf{P}_{/Poly}$. \only<3->{ Then $\mathbf{EXP} \subseteq \mathbf{PH}$ hence $\mathbf{EXP} = \mathbf{PH}$}
			\only<2>{
			\item Otherwise there is some problem in $\mathbf{EXP}$ with \red{super-polynomial} circuit complexity, then we can build a pseudorandom generator similarly.
			\begin{theorem}[20.7]
				\begin{enumerate}
					\item[3] If there exists $f \in \mathbf{EXP}$ and $\mathtt{H_{wrs}}(f) \geq n^{\red{\omega(1)}}$, then $\mathbf{BPP} \subseteq \mathbf{SUBEXP} = \cap_{\epsilon > 0} \mathbf{DTIME}(2^{n^{\epsilon}})$.
				\end{enumerate}
			\end{theorem}
			}
			\only<3-4>{
				\begin{itemize}
					\item By Valiant's Theorem, $\mathsf{perm}$ is $\mathbf{\#P}$-complete.
					\item By Toda's Theorem, $\mathbf{PH} \subseteq \mathbf{P}^{\mathbf{\#P}}$.
				\end{itemize}
			}
			\only<4->{
			\item Therefore $\mathsf{perm}$ is $\mathbf{EXP}$-complete under polynomial-time reductions.
			\item We will prove than if the conclusion doesn't hold, then $\mathsf{perm} \in \mathbf{BPP}$, obtaining a contradiction.
			}
		\end{itemize}
	\end{proof}
\end{frame}

\begin{frame}
	\begin{theorem}[20.16]
		Suppose that $\mathbf{BPP} \neq \mathbf{EXP}$. Then for every $L \in \mathbf{BPP}$ there exists a subexponential($2^{n^{o(1)}}$) time deterministic algorithm $A$ such that for infinitely many $n$'s 
		\[\Pr_{x \in_R \{0,1\}^n}[A(x) = L(x)] \geq 1 - \frac{1}{n} .\] 
	\end{theorem}
	\begin{proof}[Proof Sketch]
		\begin{itemize}
			\item For the sake of contradiction, suppose there is an algorithm $A$ with $L(A) \in \mathbf{BPP}$ whose derandomization using a generator $G$ with super-polynomial output size fails noticeably for all but finitely many input lengths.
		\end{itemize}
	\end{proof}
\end{frame}

\begin{frame}
	\begin{proof}[Proof Sketch]
		\begin{itemize}
			\item For the sake of contradiction, suppose there is an algorithm $A$ with $L(A) \in \mathbf{BPP}$ whose derandomization using a generator $G$ with super-polynomial output size fails noticeably for all but finitely many input lengths.
			\item Then there is a \red{probabilistic polynomial-time algorithm} that on input $1^n$ will find a \blue{distinguisher} $D_n$ with probability at least $\frac{1}{n}$.
			\pause
			\item The failure probability can be reduced into at most $\frac{1}{n^2}$.
			\pause
			\item Plugging this into the proof of pseudorandomness for the generator $G$, this means that there exists a \red{probabilistic polynomial-time} algorithm $T$ that can output a $poly(n)$-sized circuit computing $\mathrm{perm_n}$ given oracle access to $\mathrm{perm_n}$.
			\item<5-> $\mathrm{perm}$ is \blue{downward self-reducible}: $\mathrm{prem}(A) = \sum_{i =1}^{n} a_{1,i}\mathrm{perm}(A_{1,i})$.
			\item<6-> So we can compute \red{inductively} the circuits $C_1, \cdots, C_n$.
		\end{itemize}
	\end{proof}
\end{frame}

\begin{frame}
\centering
\Large
\textsc{Thank You!} \\
\textsc{Q \& A}
\end{frame}
\end{document}

