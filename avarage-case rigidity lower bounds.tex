\documentclass[12pt,aspectratio=169]{beamer}
%\documentclass[12pt]{beamer}
%\usepackage[utf8]{inputenc}

%\usepackage{concrete} % for the Concrete Math text fonts
\usepackage{euler}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}
\usepackage{multirow}

%\usepackage{beamerthemesplit}
%\usepackage[orientation=landscape,size=custom,width=16,height=9,scale=0.5,debug]{beamerposter}
%\usetheme{Antibes}
%\usecolortheme{spruce}
\usecolortheme{seahorse}
\usecolortheme{rose}

\newcommand{\ma}[1]{\ensuremath{#1}}


\newcommand<>{\red}[1]{{\color#2{red} #1}}
\newcommand<>{\blue}[1]{{\color#2{blue} #1}}
\newcommand<>{\green}[1]{{\color#2[rgb]{0,0.5,0} #1}}

\newcommand{\ft}[1]{\frametitle{#1}}
\newcommand{\fst}[1]{\framesubtitle{\textsc{#1}}}


\makeatletter
\setbeamertemplate{theorem begin}{
	\normalfont
	\begin{\inserttheoremblockenv}{}{
		\inserttheoremheadfont
		\usebeamercolor[fg]{block title}
		\inserttheoremname
		%\inserttheoremnumber
		\ifx\inserttheoremaddition\@empty\else\ (\inserttheoremaddition)\fi%
		\inserttheorempunctuation
	}%
	\hspace{0.3mm}
}
\setbeamertemplate{theorem end}{\end{\inserttheoremblockenv}}

\setbeamertemplate{block begin}
{
  \par\vskip\medskipamount%
%  \begin{beamercolorbox}[colsep*=.75ex]{block title}
%    \usebeamerfont*{block title}\insertblocktitle%
%  \end{beamercolorbox}%
%  {\parskip0pt\par}%
%  \ifbeamercolorempty[bg]{block title}
%  {}
%  {\ifbeamercolorempty[bg]{block body}{}{\nointerlineskip\vskip-0.5pt}}%
  \usebeamerfont{block body}%
  \begin{beamercolorbox}[colsep*=.75ex,vmode]{block body}%
    \ifbeamercolorempty[bg]{block body}{\vskip-.25ex}{\vskip-.75ex}\vbox{}%
}
\setbeamertemplate{block end}
{\end{beamercolorbox}\vskip\smallskipamount}

\makeatother

\settowidth{\leftmargini}{\usebeamertemplate{itemize item}}
\addtolength{\leftmargini}{\labelsep}

\setbeamertemplate{itemize item}[circle]
\setbeamerfont*{itemize/enumerate body}{size=\normalsize}
\setbeamerfont*{itemize/enumerate subbody}{parent=itemize/enumerate body}
\setbeamerfont*{itemize/enumerate subsubbody}{parent=itemize/enumerate body}

\beamertemplatenavigationsymbolsempty

%\theoremstyle{definition}
\newtheorem{thm}{Thm}
\newtheorem{defn}{Def}
\newtheorem{lem}{Lem}
\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}
\newtheorem{remark}{Remark}
\newtheorem{observation}{Observation}
\newtheorem{conjecture}{Conjecture}

%########## Temporary Math Commands ############
\newcommand{\tp}[1]{\left(#1\right)}
\renewcommand{\epsilon}{\varepsilon}
\newcommand{\logeps}{\log(1/\epsilon)}
\renewcommand{\tilde}{\widetilde}
\newcommand{\weight}[1]{\vert\!\vert\!\vert #1 \vert\!\vert\!\vert}
\newcommand{\CNF}{\mathsf{CNF}}
\newcommand{\DNF}{\mathsf{DNF}}
\newcommand{\OR}{\mathsf{OR}}
\newcommand{\AND}{\mathsf{AND}}
\newcommand{\AC}{\mathsf{AC}}
\newcommand{\TC}{\mathsf{TC}}
\newcommand{\SYM}{\mathsf{SYM}}
\newcommand{\EXACT}{\mathsf{EXACT}}
\newcommand{\NNV}{\mathtt{NumGadget}}
\newcommand{\CB}{\mathtt{CountBit}}
\def\*#1{\mathbf{#1}}
\def\+#1{\mathcal{#1}}
\def\-#1{\mathbb{#1}}
%###############################################

\title{Average-Case Rigidity Lower Bounds}
\subtitle{CSR 2021}
\author{\textbf{Xuangui Huang} \and Emanuele Viola\\ Northeastern University}
\date{July 1, 2021}

\begin{document}
\frame{\titlepage}

%\begin{frame}
%\ft{Outline}
%\begin{itemize}
%\item Background
%\item Our Work
%\item Proof Sketch
%\item Recent Development
%\end{itemize}
%\end{frame}

\begin{frame}
\ft{Background: Algorithmic Method for Lower Bounds}
\blue{[Williams '10]}: Algorithmic method, $\mathbf{NEXP} \not\subseteq \mathbf{ACC}^0$

\only<1>{Fast circuit analysis algorithm $\Rightarrow$ circuit lower bounds}

\pause
\medskip

\begin{columns}[T]
\begin{column}{.48\textwidth}
\textbf{Average-case hardness}
$(1/2-\epsilon)$-hard against size-$s$ $\mathbf{AC}^0[\oplus]$
\begin{itemize}
\item \blue{[Chen and Ren '20]}: $\epsilon = 1/s$ for sub-half-exponential $s$
\item \blue{[Viola '20]}: $\epsilon = polylog(s)/n$
\pause
\item \blue{[Chen, Lyu, and Williams '20]}: $\epsilon = 1/s$ for $s= 2^{n^{o(1)}}$
\end{itemize}

\end{column}
\hfill
\pause
\begin{column}{.48\textwidth}
\textbf{More general model}

$\Omega(1)$-hard against low-rank matrices

\begin{itemize}
\item \blue{[Alman and Chen '19]}: 
rank-$2^{n^{1/4 - \delta}}$

\item \blue{[Viola '20]}:
rank-$2^{n / \Omega(\log n)}$ 

for matrices given by polynomials

\pause
\item \blue{[Bhangale, Harsha, Paradise, and Tal '20]}:
rank-$2^{n / \Omega(\log n)}$
\end{itemize}
\end{column}%
\end{columns}

\pause
\medskip
\begin{itemize}
\item Our work: simultaneously generalizes \blue{[CLW'20]} \& \blue{[BHPT'20]}
\end{itemize}
\end{frame}

%\begin{frame}
%\ft{Background: Algorithmic Method for Rigidity Lower Bounds}
%\begin{defn}
%$f(x,y)$ for $|x| = |y| = n$ is $(1/2 - \epsilon)$-hard for rank-$\rho$:
%
%$\Pr_{x,y}[M_{x,y} \neq f(x,y)]\geq 1/2 - \epsilon$ for any rank-$\rho$ $M$ over $\mathbb{F}_2$
%\end{defn}
%\pause
%\begin{itemize}
%\item \blue{[Alman and Chen '19]}: 
%$\epsilon = \Omega(1)$ for rank-$2^{n^{1/4 - \epsilon}}$
%
%\item \blue{[Viola '20]}:
%fancy PCP, rank-$2^{n / polylog(n)}$ for matrices given by polynomials
%
%Q: construct suitable PCP?
%
%\pause
%\medskip
%\item \blue{[Bhangale, Harsha, Paradise, and Tal '20]}: Rectangular PCP,
%rank-$2^{n / \Omega(\log n)}$
%
%
%\pause
%\bigskip
%\item Q: improve $\epsilon$, strong average-case hardness
%
%\pause
%\item Our work: simultaneously generalizes \blue{[CLW'20]} \& \blue{[BHPT'20]}
%\end{itemize}
%\end{frame}


%\begin{frame}
%\ft{Background: Rigid Matrix}
%$f(x,y)$ for $|x| = |y| = n$, $\delta$-hard for rank-$\rho$:
%
%$\Pr_{x,y}[M_{x,y} \neq f(x,y)]\geq \delta$ for any rank-$\rho$ $M$ over $\mathbb{F}_2$
%
%\pause
%\bigskip
%\begin{itemize}
%	\item Valiant \blue{[Val'77]}:
%	\begin{itemize}
%		\item $2^{-n(1 - \epsilon)}$-hard for rank-$2^n / \log n$ $\Rightarrow$ linear-size log-depth linear circuit lower bounds
%		\item random $f$: $\Omega((1 - \rho/2^n)^2 / n)$-hard for rank-$\rho$ w.h.p
%	\end{itemize}
%	\item Explicit Construction?
%\end{itemize}
%
%\end{frame}



\begin{frame}
\ft{Our result: Average-Case Rigidity Lower Bounds}
\begin{thm}
$\exists f : \{0,1\}^n \times \{0,1\}^n \to \{-1,1\} \in \mathbf{E}^\mathbf{NP}$:
\[
\forall \text{ rank-}2^r~ 2^n \times 2^n \text{ matrix } M,~\Pr_{x,y}[M_{x,y} \neq f(x,y)] \geq \frac{1}{2} - 2^{-\sqrt{n / \Omega(r)}},
\]
for all sufficiently large $n$.
\end{thm}
\pause
\begin{itemize}
	\item $(1/2 - 2^{-n^{\Omega(1)}})$-hard for rank-$2^{n^{0.99}}$: generalizing \blue{[CLW'20]}
	\pause
	\item $\Omega(1)$-hard for rank-$2^{n/\Omega(\log n)}$: recovering \blue{[BHPT'20]}
\end{itemize}
\end{frame}

\begin{frame}
\ft{Outline for the rest of the talk}
\begin{enumerate}
\item Main proof idea
\item Brief review of \blue{[BHPT'20]}
\item Extending \blue{[BHPT'20]}
\end{enumerate}
\end{frame}

\begin{frame}
\ft{Main Proof Idea}
\begin{itemize}
\item Standard hardness amplification:
\begin{itemize}
	\item Yao's XOR lemma: $f^{\oplus k}(x_1, \dots, x_k) = f(x_1) \oplus \cdots \oplus f(x_k)$
	
	 f $\Omega(1)$-hard \only<2->{for $\mathsf{MAJ}\circ \mathcal{C}$} $\Rightarrow$ $f^{\oplus k}$ $(1/2 - 2^{-\Omega(k)})$-hard \only<2->{for $\mathcal{C}$}
	 
	 \pause
	 \medskip
	 \item \blue{[Grinberg, Shaltiel, and Viola '18]}: requires Majority
\end{itemize}

\pause
\medskip
\item \blue{[CLW'20]}: non-standard XOR lemma, based on Levin's proof

``$\Omega(1)$-hard" for real sums of $\mathcal{C}$ suffices
\end{itemize}

\end{frame}

\begin{frame}
\ft{Main Proof Idea}
\begin{enumerate}
\item Adapt \blue{[CLW'20]}:

	``$\Omega(1)$-hard" for sums of rank-$2^r$
	$\Rightarrow$ $f^{\oplus k}$ $(1/2 - 2^{-\Omega(k)})$-hard for rank-$2^r$

\pause
\medskip
\item Generalize \blue{[BHPT'20]}:

``$\Omega(1)$-hard" for sums of rank-$2^r$
\end{enumerate}
\pause
\begin{defn}
\emph{$m$-sum of rank-$2^r$ matrices $Q$} $ = C \sum_{i = 1}^m b_i \cdot M^{(i)}$,

where $C \in \mathbb{Q}$, $b_i \in \{-1,1\}$, $M^{(i)}$ has rank $2^r$.
\end{defn}

\pause

\begin{defn}
$f$ $\Omega(1)$-hard: correlation $\mathbb{E}_{x,y}[f(x,y)Q_{x,y}] \leq O(1)$
\end{defn}
Note: $Q$ bounded
\end{frame}


\begin{frame}
\ft{$\Omega(1)$-hardness for sums of rank-$2^r$}
\begin{thm}
$\exists f: \{0,1\}^{n + O(\log n)} \to \{-1,1\} \in \mathbf{E}^\mathbf{NP}$:

$\Omega(1)$-hard for any $m$-sum of rank-$2^r$ matrices, $m \leq 2^{n/\Omega(r)}$.
\end{thm}

\pause
\bigskip
\begin{thm}[\blue{[BHPT'20]}]
$\exists f: \{0,1\}^{n + O(\log n)} \to \{-1,1\} \in \mathbf{E}^\mathbf{NP}$:

$\Omega(1)$-hard for rank-$2^r$ matrices, $r \leq n/\Omega(\log n)$.
\end{thm}
\begin{itemize}
\item algorithmic method
\item rectangular PCP
\item fast evaluation of expectations of boolean functions over rank-$2^r$
\end{itemize}
\end{frame}

\begin{frame}
\ft{Proof Sketch of \blue{[BHPT'20]}}
\begin{thm}[\blue{[BHPT'20]}]
$\exists f: \{0,1\}^{n + O(\log n)} \to \{-1,1\} \in \mathbf{E}^\mathbf{NP}$:

$\Omega(1)$-hard for rank-$r$ matrices, $r \leq n/\Omega(\log n)$.
\end{thm}
\begin{proof}
Let unary $L \in \mathbf{NTIME}(2^n) \setminus \mathbf{NTIME}(2^n/n)$

Find $f$: $\Omega(1)$-close to rank-$2^r$ $\Rightarrow$ $L \in  \mathbf{NTIME}(2^n/n)$

\end{proof}
\end{frame}

\begin{frame}
\ft{Probabilistically Checkable Proof (PCP)}

PCP Verifier $V$ for unary $L \in \mathbf{NTIME}(2^n)$:

Proof $\pi$, \emph{acceptance probability}: $\Pr_{R \in \{0,1\}^r}[V^\pi(1^n;R) = 1]$
\pause
\begin{itemize}

\item Perfect Completeness, $O(1)$-Soundness

\item Short proof: $2^{n+O(\log n)}$

\item Efficient V: runs in time $2^{O(\tau n)}$, making $O(1)$ queries
\end{itemize}
\end{frame}

\begin{frame}
\ft{Proof Sketch of \blue{[BHPT'20]}}
\begin{thm}[\blue{[BHPT'20]}]
$\exists f: \{0,1\}^{n + O(\log n)} \to \{-1,1\} \in \mathbf{E}^\mathbf{NP}$:

$\Omega(1)$-hard for rank-$2^r$ matrices, $r \leq n/\Omega(\log n)$.
\end{thm}
\begin{proof}
Let unary $L \in \mathbf{NTIME}(2^n) \setminus \mathbf{NTIME}(2^n/n)$

$f:$ lexico-first PCP proof of $L$ w/ accept. prob. $= 1$


\medskip
$f$ $\Omega(1)$-close to rank-$2^r$ $\Rightarrow$ $L \in  \mathbf{NTIME}(2^n/n)$:
\pause
\begin{itemize}
\item Algorithm $A$ to decide if $1^n \in L$:
	\begin{enumerate}
		\item Guess $\pi$: rank-$2^r$ $\approx f$
		\item Compute acceptance probability of $\pi$ to decide
	\end{enumerate}

\pause
\item  Correctness of $A$?
\item  Step 2 $\in \mathbf{TIME}(2^n/n)$ ?
\end{itemize}
\end{proof}
\end{frame}

\begin{frame}
\ft{Proof Sketch of \blue{[BHPT'20]}}
\begin{thm}[\blue{[BHPT'20]}]
$\exists f: \{0,1\}^{n + O(\log n)} \to \{-1,1\} \in \mathbf{E}^\mathbf{NP}$:

$\Omega(1)$-hard for rank-$2^r$ matrices, $r \leq n/\Omega(\log n)$.
\end{thm}
\begin{proof}
Let unary $L \in \mathbf{NTIME}(2^n) \setminus \mathbf{NTIME}(2^n/n)$

$f:$ lexico-first \red{smooth} PCP proof of $L$ w/ accept. prob. $= 1$

\medskip
$f$ $\Omega(1)$-close to rank-$2^r$ $\Rightarrow$ $L \in  \mathbf{NTIME}(2^n/n)$:
\begin{itemize}
\item Algorithm $A$ to decide if $1^n \in L$:
	\begin{enumerate}
		\item Guess $\pi$: rank-$2^r$ $\approx f$
		\item Compute acceptance probability of $\pi$ to decide
	\end{enumerate}

\only<1>{\item Smooth PCP: each location is equally likely to be queried

$\pi \approx f \Rightarrow$ accept. prob. of $\pi$ $\approx$ accept. prob. of $f$}
\only<2->{\item Smoothness, Soundness \& Completeness $\Rightarrow$ correctness of $A$
}
\only<3->{\item Step 2 $\in \mathbf{TIME}(2^n/n)$?}
\end{itemize}
\end{proof}
\end{frame}

\begin{frame}
\ft{Step 2 $\in \mathbf{TIME}(2^n/n)$}
\begin{itemize}
\item Rectangular PCP \blue{[BHPT'20]}:
\begin{itemize}
\item View $\pi$ as squared matrix
\item query locations are generated rectangularly
\item $\pi$ $\in$ rank-$2^r$ $\Rightarrow$ query results indexed by $R$ $\in$ rank-$2^r$

\end{itemize}

\pause
\medskip
\item accept. prob. of $\pi = \mathbb{E}_R[V(\text{query results})]$
\begin{thm}[\blue{[BHPT'20]}]
Given $M^{(1)}, \dots, M^{(k)}$: rank-$2^r$, $r \leq n/\Omega(\log n)$,

poly $p$ on $k=O(1)$ vars.

Can compute $\mathbb{E}_{i,j}\left[p\left(M^{(1)}_{i,j}, M^{(2)}_{i,j}, \dots, M^{(k)}_{i,j}\right)\right]$
in time $O(2^n/n)$.
\end{thm}


\pause
\begin{lem}[\blue{[AC'19]}]
Given $A \in \mathbb{F}_2^{N \times 2^r}$, $B \in \mathbb{F}_2^{2^r \times N}$:

Can compute \# of 1's in $AB$ in time $N^{2 - \Omega(1/r)}$ if $2^r = N^{o(1)}$.
\end{lem}


\end{itemize}
\end{frame}

\begin{frame}
\ft{Proof Sketch of \blue{[BHPT'20]}}
\begin{thm}[\blue{[BHPT'20]}]
$\exists f: \{0,1\}^{n + O(\log n)} \to \{-1,1\} \in \mathbf{E}^\mathbf{NP}$:

$\Omega(1)$-hard for rank-$2^r$ matrices, $r \leq n/\Omega(\log n)$.
\end{thm}
\begin{proof}
Let unary $L \in \mathbf{NTIME}(2^n) \setminus \mathbf{NTIME}(2^n/n)$

$f:$ lexico-first \red{rectangular} PCP proof of $L$ w/ accept. prob. $= 1$

\medskip
$f$ $\Omega(1)$-close to rank-$2^r$ $\Rightarrow$ $L \in  \mathbf{NTIME}(2^n/n)$:
\begin{itemize}
\item Algorithm $A$ to decide if $1^n \in L$:
	\begin{enumerate}
		\item Guess $\pi$: rank-$2^r$ $\approx f$
		\item Compute acceptance probability of $\pi$ to decide
	\end{enumerate}
\item Smoothness, Soundness \& Completeness $\Rightarrow$ correctness of $A$
\item Rectangular Property + Fast Evaluation $\Rightarrow$ Step 2 $\in \mathbf{TIME}(2^n/n)$
\end{itemize}
\end{proof}
\end{frame}



\begin{frame}
\ft{$\Omega(1)$-hardness for $m$-sums of rank-$2^r$}
\begin{thm}
$\exists f: \{0,1\}^{n + O(\log n)} \to \{-1,1\} \in \mathbf{E}^\mathbf{NP}$:

$\Omega(1)$-hard for any $m$-sum of rank-$2^r$ matrices, $m \leq 2^{n/\Omega(r)}$.
\end{thm}

\begin{itemize}
\item algorithmic method
\item rectangular PCP
\pause
\item fast evaluation of expectations of boolean functions over \red{sums of rank-$2^r$}
\end{itemize}

\begin{thm}
Given $M^{(1)}, \dots, M^{(k)}$: \red{$m$-sums of rank-$2^r$,  $m \leq 2^{n/\Omega(r)}$},

poly $p$ on $k=O(1)$ vars.

Can compute $\mathbb{E}_{i,j}\left[p\left(M^{(1)}_{i,j}, M^{(2)}_{i,j}, \dots, M^{(k)}_{i,j}\right)\right]$
in time $O(2^n/n)$.
\end{thm}

\end{frame}

\begin{frame}
\ft{Proof Sketch}
\begin{proof}
Let unary $L \in \mathbf{NTIME}(2^n) \setminus \mathbf{NTIME}(2^n/n)$

$f:$ lexico-first rectangular PCP proof of $L$ w/ accept. prob. $= 1$

\medskip
$f$ $\Omega(1)$-close to \red{$m$-sum of rank-$2^r$} $\Rightarrow$ $L \in  \mathbf{NTIME}(2^n/n)$:
\begin{itemize}
\item Algorithm $A$ to decide if $1^n \in L$:
	\begin{enumerate}
		\item Guess $\pi$: \red{$m$-sum of rank-$2^r$} $\approx f$
		\item Compute ``acceptance probability'' of $\pi$ to decide
	\end{enumerate}
	
\only<2>{\item Rectangular Property: $\pi \in$ sum of rank-$2^r$ $\Rightarrow$ query results $\in$ sums of rank-$2^r$
}
\only<3->{\item Rectangular Property + (New) Fast Evaluation $\Rightarrow$ Step 2 $\in \mathbf{TIME}(2^n/n)$}
\only<4->{\item Correctness of $A$: $\pi$ might be far from boolean!}
\end{itemize}
\end{proof}
\end{frame}

\begin{frame}
\ft{Technicality: Validity Tests \blue{[CLW'20]}}
\begin{itemize}
\item Ensure that $\pi$ is (almost-)bounded \& close to boolean

\pause
\medskip
\item $\pi$ is (almost-)bounded \& close to boolean + Smoothness

$\Rightarrow$ ``acceptance probability'' of $\pi$ $\approx$ acceptance probability of $f$

$+$ Soundedness \& Completeness $\Rightarrow$ Correctness of $A$

\pause
\medskip
\item New Fast Evaluation: $\in \mathbf{TIME}(2^n / n)$
\end{itemize}
\end{frame}

\begin{frame}
\ft{Proof Sketch}
\begin{proof}
Let unary $L \in \mathbf{NTIME}(2^n) \setminus \mathbf{NTIME}(2^n/n)$

$f:$ lexico-first rectangular PCP proof of $L$ w/ accept. prob. $= 1$

\medskip
$f$ $\Omega(1)$-close to \red{$m$-sum of rank-$2^r$} $\Rightarrow$ $L \in  \mathbf{NTIME}(2^n/n)$:
\begin{itemize}
\item Algorithm $A$ to decide if $1^n \in L$:
	\begin{enumerate}
		\item Guess $\pi$: \red{$m$-sum of rank-$2^r$} $\approx f$
		\item \red{Run validity tests: $\pi$ is (almost-)bounded \& close to boolean}
		\item Compute ``acceptance probability'' of $\pi$ to decide
	\end{enumerate}
	
\item Rectangular Property + (New) Fast Evaluation $\Rightarrow$ Step 3 $\in \mathbf{TIME}(2^n/n)$
\item Step 2 $\in \mathbf{TIME}(2^n/n)$ \& Correctness of $A$
\end{itemize}
\end{proof}
\end{frame}


\begin{frame}
\ft{Explicit Average-Case Rigid Matrices: Full Proof}

\begin{enumerate}
\item $f \in \mathbf{E}^\mathbf{NP}$: $\Omega(1)$-hard for $2^{n/\Omega(r)}$-sum of rank-$2^r$

\pause
\medskip
\item Adapted from \blue{[CLW'20]}: XOR lemma
\begin{lem}
$f$ $\Omega(1)$-hard for any $O(n2^k)$-sums of rank-$2^r$

$\Rightarrow$ $f^{\oplus k}$ $(1/2 - 2^{-\Omega(k)})$-hard for rank-$2^r$.
\end{lem}

\pause

$f^{\oplus k}$ has $n' = kn$ inputs:

$\left(1/2 - 2^{-\sqrt{n' / \Omega(r)}}\right)$-hard for rank-$2^r$


\end{enumerate}
\end{frame}


\begin{frame}
\ft{Conclusion \& Future Direction}
\begin{itemize}
\item Our result: $\left(1/2 - 2^{-\sqrt{n / \Omega(r)}}\right)$-hardness for rank-$2^r$

\pause
\medskip
\item Independently, Chen and Lyu \blue{[CL'21]}:
\begin{itemize}
	\item $\left(1/2 - 2^{-(n / \Omega(r))^{2/3}}\right)$-hardness for rank-$2^r$
	\pause
	\item same proof + (partially-)derandomized XOR lemma:

	input length $\sqrt{k}n$ instead of $kn$
	\item difference in page length $> 50$
\end{itemize}

\pause
\medskip
\item $\left(1/2 - 2^{-n^{1+\Omega(1)}/r}\right)$-hardness for rank-$2^r$ 

$\Rightarrow$ new data-structure lower bounds \blue{[Vio'20]}
\end{itemize}
\end{frame}


\begin{frame}
\centering
Thanks!

Q \& A
\end{frame}
\end{document}