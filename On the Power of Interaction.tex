\documentclass{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}


\usepackage{beamerthemesplit}
%\usetheme{Antibes}
\usecolortheme{beaver}

\newcommand{\ma}[1]{\ensuremath{#1}}

\newcommand{\T}{\ma{\mathsf{T}}}

\newcommand{\bra}[1]{\langle #1\rangle}
\newcommand{\sqbra}[1]{[ #1]}
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\eps}{\varepsilon}
\newcommand{\red}[1]{{\color{red} #1}}
\newcommand{\blue}[1]{{\color{blue} #1}}

\newcommand{\ft}[1]{\frametitle{\textsc{#1}}}
\newcommand{\fst}[1]{\framesubtitle{\textsc{#1}}}

\title{On the Power of Interaction\\ (1986)}
\author{Xuangui Huang}

\begin{document}
\frame{\titlepage}
\begin{frame}
    \ft{Introduction}
    \fst{Babai's Result}
    \begin{itemize}
        \item Babai showed that the finite levels of the Arthur-Merlin hierarchy collapse to the second level. That is, for all $k > 2$, 
        \[ AM[k] = AM[2].\]
        \item Moreover, \[ AM[2] \subset \Pi_2 .\] \pause
        \item All these results can be relativized.
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{Introduction}
    \fst{Babai's Conjecture}
    \begin{itemize}
        \item However, Babai's proof method does not extend to an unbounded number of levels. \pause
        \item But he still conjectured that for some $k$, \[ AM[Poly] \subset \Sigma_k.\]
        \item And even \[ AM[Poly] = AM[2] .\]
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{Introduction}
    \fst{Limitation of Diagonalization}
    \begin{itemize}
        \item The result of this paper indicates that if we want to prove such collapse results, \red{we cannot use any proof method that relativizes}, including diagonalization method. \pause
    \end{itemize}
    \begin{theorem}[8.2]
        For any \red{unbounded} function $f(n)$ there is an oracle $B$ such that \[AM^B[f(n)] \not\subset PH^B .\]
    \end{theorem}
    \pause
    \begin{corollary}
        \[ AM^B[f(n)] \not\subset AM^B[2] .\]
    \end{corollary}
\end{frame}
\begin{frame}
    \ft{Outline of The Proof}
    \begin{itemize}
        \item Obviously we have to construct a language in $AM^B[f(n)]$ that is not in $PH^B$.
        \item More precisely, we will define a language $L(B)$ which is dependent on $B$, then construct $B$ carefully to meet the above requirement. \pause
        \item[ ]
        \item We will use circuits lower bounds to prove.
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{Outline of The Proof}
    \begin{itemize}
        \item We will use circuits lower bounds to prove. \pause
        \begin{itemize}
            \item $\Sigma^{p,B}_i$-machines have simple circuit characterizations: constant depth, small size.
            \item To formulate $AM^B[f(n)]$, we use \T -circuit. \pause
            \item Then we will prove small size constant depth circuits cannot simulate \T -circuits under certain conditions.
        \end{itemize} \pause
        \item At last we can use the above result to construct $B$ carefully, making $L(B)$ in $AM^B[f(n)]$ but not in $PH^B$.
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{Notation in Circuits}
    We will identify an oracle $B$ with a \red{set of Boolean variables $y_z^B$} defined for every $z \in \Sigma^*$ by setting $y_z^B = 1$ iff $z \in B$.
\end{frame}
\begin{frame}
    \ft{Notation in (AM) Games}
    We call the \red{value} of a (AM) game as the probability that Arthur accepts.
\end{frame}
\begin{frame}
    \ft{The Definition of $L(B)$}
    \fst{The Defining Game}

    The language $L(B)$ is defined with respect to the following AM game, called \red{the defining game}: \newline
    
    On input $1^n$, the game has $f(n)$ interactions starting with Arthur. At the $i$-th interaction the following happens:
    \begin{enumerate}
        \item Arthur sends $\sqrt[]{n}$ random bits, denoted $a_i$.
        \item Merlin responds by $\sqrt[]{n}$-bit strings, denoted $b_i$.
    \end{enumerate}
    Arthur accepts if the string $a_1b_1\cdots a_{f(n)}b_{f(n)} \in B$.
\end{frame}
\begin{frame}
    \ft{The Definition of $L(B)$}
    \begin{definition}
        The language \red{L(B)} is an unary language such that for all $n$, $1^n \in L(B)$ if the value of the defining game is $\geq 2/3$.
    \end{definition}
    \pause
    \begin{itemize}
        \item L(B) is not necessarily in $AM^B[f(n)]$.
        \item However we can construct $B$ carefully such that the value of the defining game is either $\leq 1/3$ or $\geq 2/3$. 
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{Oracle Games and AM Circuits}
    \begin{definition}
        A \red{weak AM game} with oracle $B$ is an AM game with oracle $B$ in which Arthur makes only one oracle query (at the end of the protocol).
    \end{definition}
    \pause
    \begin{itemize}
        \item Without loss of generality we can assume all interactions in an AM game are of the same length and also that the number of interaction only depend on the length of the input.
    \end{itemize}
\end{frame}

\begin{frame}
    \ft{Oracle Games and AM Circuits}
    \fst{AM Circuit}
    New gates: A gates and M gates. \newline
    \begin{itemize}
        \item A gate: take the average of its input
        \item M gate: take the maximum value of its input \pause
        \item input, output of gates: rational numbers
    \end{itemize} \pause
    \begin{definition}
        An \red{$A_d^l$ circuit} is a $2^l$-ary tree of height $2d$ where nodes of even height are A gates and nodes of odd height are M gates.
    \end{definition}
\end{frame}

\begin{frame}
    \ft{Oracle Games and AM Circuits}
    \fst{AM Circuit}
    New gates: A gates and M gates. \newline
    \begin{itemize}
        \item A gate: take the average of its input
        \item M gate: take the maximum value of its input \pause
        \item input, output of gates: rational numbers
    \end{itemize} \pause
    \begin{definition}
        An \red{$A_d^l$ circuit} is a $2^l$-ary tree of height $2d$ where nodes of even height are A gates and nodes of odd height are M gates.
    \end{definition}
\end{frame}

\begin{frame}
    \ft{Oracle Games and AM Circuits}
    \begin{lemma}[4.1]
        Let $G^B$ be a weak AM game with oracle $B$ which has $d$ interactions of length $l$. \blue{For every $x$} there is an \blue{assignment} to the inputs of $A_d^l$ (from $\{0,1\} \cup (y^B_z)_{z \in \{0,1\}^*}$) such that for every $B$ the value of $G^B$ on $x$ is equal to the output of $A^l_d$.
    \end{lemma}
    \pause
    \begin{proof}
        Mark the leaves of the $A^l_d$ tree respectively.
    \end{proof}
    
    \pause
    \begin{itemize}
        \item The value of the defining game on $1^n$ is equal to the output of $A^{\sqrt[]{n}}_{f(n)}$ on the $y_z^B$'s with $|z|=2f(n)\sqrt[]{n}$.

    \end{itemize}
\end{frame}

\begin{frame}
    \ft{Oracle Games and \T -Circuits}
    \fst{\T -Circuits}

    New gates: $\T_r$ \red{threshold} gates and $\vee$ gates. \\
    New symbol: $\dagger$ .\newline
    \begin{itemize}
        \item input, output of gates: $\{0,1,\dagger\}$.
        \item $\T_r$ gate ($r > 1/2$): 
            \[ T_r = \left\{ \begin{array}{ll}
                                 1, & \mbox{ if there are a $r$ fraction of $1$'s in the input;} \\
                                 0, & \mbox{ if there are a $r$ fraction of $0$'s in the input;} \\
                                 \dagger, & \mbox{ otherwise.} \end{array} \right.
            \]
        \item $\vee$ gate: 
        \[ \vee = \left\{ \begin{array}{ll}
                                 1, & \mbox{ if there exists a $1$ in the input;} \\
                                 \dagger, & \mbox{ if there exists a $\dagger$ but no $1$;} \\
                                 0, & \mbox{ if all inputs are $0$.} \end{array} \right.
            \]
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{Oracle Games and \T -Circuits}
    \fst{\T -Circuits}
    \begin{definition}
        An \red{$\T_{r,d}^l$ circuit} is a $2^l$-ary tree of height $2d$ where nodes of even height are $\T_r$ gates and nodes of odd height are $\vee$ gates.
    \end{definition}
    \pause
    \begin{lemma}[4.3]
        \[\begin{array}{ccc}
            \T^l_{r,d}(x) = 1 & \Rightarrow & A^l_d(x) \geq r^d \\
            \T^l_{r,d}(x) = 0 & \Rightarrow & A^l_d(x) \leq 1 - r^d
        \end{array}\]
    \end{lemma}
\end{frame}
\begin{frame}
    \ft{Oracle Games and \T -Circuits}
    \begin{lemma}[4.4]
    For $r$ of the form $1-o(1/f(n))$, if \[ \forall n, T^{\sqrt{n}}_{r,f(n)}(y^B_z) = 0 \text{ or } 1 \] then \[ L(B) \in AM^B[f(n)] \] and the \T -circuit evaluate $L(B)$ correctly.
    \end{lemma}
    \pause
    \begin{itemize}
        \item For the remainder of this talk we will use $l = \sqrt{n}$ and $r = 1-2^{-n^{1/16}}$ and write $T_{f(n)}$ as shorthand for $T^l_{r,f(n)}$.
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{Limitation of Constant Depth Circuits}
    In the following we will show that constant depth circuits cannot compute $\T_{f(n)}$.
    \begin{lemma}[8.1]
    For all $j$ and $p$, any unbounded function $f(n)$ and all circuit families $\{C_n\}_{n\in \omega}$ of depth $j$ and size $4^{n^p}$ there exists $n$ large enough such that there is a setting of input variables such that $C_n(x) \neq \T_{f(n)}(x) = 0 \text{ or } 1$.
    \end{lemma}
    \pause
    \begin{itemize}
     \item  We will prove it using \red{random restrictions $R_{k,n}$}.
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{Random Restriction $R_{n,k}$}
    \begin{definition}
        A \red{restriction} $\rho$ is a function of the variables $x_i$ to the set $\{0,1,*\}$. $\rho(x_i) = 0(1)$ means we give value $0(1)$ to $x_i$, while $\rho(x_i) = *$ means that we keep it as a variable.
    \end{definition}
    \pause
    \begin{definition}
        We denote $F \upharpoonright_\rho$ the function we get by applying restriction $\rho$ on $F$.
    \end{definition}
\end{frame}
\begin{frame}
    \ft{Random Restriction $R_{n,k}$}
    In \red{random restrictions $R_{k,n}$}, we
    \begin{itemize}
        \item partition the variables into groups calles \red{$k$-block}
        \item label each node probabilistically
        \item ...
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{The Effect of $R_{k,n}$ on Constant Depth Circuits}
    \begin{lemma}[6.1]
    Let $H$ be a function computed by a circuit of depth $j$, size $2^{n^r}$, and bottom fanin $n^r$. For $k \geq 3r+2$ and $\rho \in R_{k,n}$,
    \begin{align*}
         Pr[\text{ $H \upharpoonright_{\tau\rho}$ can be written as a circuit of depth $j-1$}& \\
         \text{ and bottom fanin $n^r$}& ] > 1 - 2^{-n^r}.
    \end{align*}
    \pause
    \begin{itemize}
        \item With a high probability small constant depth circuits with a random restriction can be written as small circuits with \blue{depth one less}.
        \pause
        \item A constant number of restrictions \blue{completely determine} the constant depth circuit.
    \end{itemize}
    \end{lemma}
\end{frame}
\begin{frame}
    \ft{The Effect of $R_{k,n}$ on \T -Circuits}
    \begin{lemma}[6.2]
    For any polynomial $d$ and integer $n$ such that $d(n) > k$, for $\rho \in R_{k,n}$,
    \begin{align*}
         Pr[\T_{d-k} = \T_d \upharpoonright_{\tau\rho} ] \text{ is very high}.
    \end{align*}
    \pause
    \begin{itemize}
        \item With a high probability a \T -circuit with a random restriction can be written as  a \T -circuit with \blue{a constant fewer levels}.
        \pause
        \item For large enough $n$, a \blue{constant} number of restrictions \blue{cannot determine} the \T -circuit.
    \end{itemize}
    \end{lemma}
\end{frame}
\begin{frame}
    \ft{Limitation of Constant Depth Circuits}
    For $n$ large enough,
    \begin{itemize}
        \item A constant number of restrictions \blue{completely determine} the constant depth circuit.
        \item For large enough $n$, a constant number of restrictions \blue{cannot determine} the \T -circuit.
    \end{itemize} \pause
    \begin{lemma}[8.1]
    For all $j$ and $p$, any unbounded function $f(n)$ and all circuit families $\{C_n\}_{n\in \omega}$ of depth $j$ and size $4^{n^p}$ there exists $n$ large enough such that there is a setting of input variables such that $C_n(x) \neq \T_{f(n)}(x) = 0 \text{ or } 1$.
    \end{lemma}
\end{frame}
\begin{frame}
    \ft{Relatived Complexity and Circuits}
    \begin{lemma}[3.1]
        Let \blue{$M^B$} be a $\Sigma^{p,B}_i$ oracle alternating machine which runs in time $t$ on \blue{input $x$}. Then for every $x$ there is a depth $i+2$ circuit $C$ of size $4^t$ which has a subset of $y^B_z$ as inputs such that for every oracle $B$, $M^B$ accepts $x$ iff $C$ outputs $1$ on inputs $y^B_z$.
    \end{lemma} \pause
    \begin{itemize}
        \item Furst M., Saxe J. and Sipser M., "Parity, Circuits and the Polynomial Time Hierarchy", FoCS 1981.
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{The Construction of $B$ (Simplified)}
    \begin{itemize}
        \item Let $M_1^B, M_2^B, \cdots $ be an enumeration of all alternating TM which hace a \blue{bounded number of alternation} and run in \blue{polynomial time}.
        \item Let $\mathcal{C}_1, \mathcal{C}_2, \cdots$ be the corresponding \blue{families of circuits} given by lemma 3.1. \pause
        \item We will ensure that there is a setting of the variables $y^B_z$ such that for all the above circuit families $\{C_n\}$ there exists an $n$ such that $C_n(y^B_z) \neq \T_{f(n)}(y^B_z) = 0 \mbox{ or } 1$. \pause
        \begin{itemize}
            \item By diagonalization.
        \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{The Construction of $B$ (Simplified)}
    \begin{itemize}
        \item Stage $0$: Take $n_0$ large enough for all bounds to hold. Set $y_z^B$ arbitrarily to $0$ for $|z| < 2n_0$. \pause
        \item Stage $i$: 
        \begin{itemize}
            \item Let $n_i$ be the smallest number great than $n_{i-1}$ which satisfies Lemma 8.1 for $\mathcal{C}_i$.
            \item Arbitrary set all $y^B_z$ with $2n_{i-1} < |z| < 2n_i$ to $0$.
            \item Use Lemma 8.1 to set $y^B_z$ with $|z| = 2n_i$ such that $C_n(x) \neq \T_{f(n)}(x) = 0 \text{ or } 1$.
        \end{itemize}
    \end{itemize}
\end{frame}
\begin{frame}
    \ft{The Results}
    \begin{theorem}
        For any \red{unbounded} function $f(n)$ there is an oracle $B$ such that \[AM^B[f(n)] \not\subset PH^B .\]
    \end{theorem} \pause
    \begin{theorem}
        For any \red{polynomial bounded polynomial time computable} function $f(n)$ and any $g(n) = o(f(n))$, there is an oracle $B$ such that
        \[AM^B[f(n)] \not\subset \Sigma^B_{g(n)}.\]
    \end{theorem}
\end{frame}
\begin{frame}
 \begin{center}
   Thank you!
 \end{center}
\end{frame}
\end{document}

