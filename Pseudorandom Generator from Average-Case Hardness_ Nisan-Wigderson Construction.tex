\documentclass[aspectratio=169]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}


%\usepackage{beamerthemesplit}
%\usepackage[orientation=landscape,size=custom,width=16,height=9,scale=0.5,debug]{beamerposter}
%\usetheme{Antibes}
\usecolortheme{spruce}

\newcommand{\ma}[1]{\ensuremath{#1}}

\newcommand{\T}{\ma{\mathsf{T}}}

\newcommand{\bra}[1]{\langle #1\rangle}
\newcommand{\sqbra}[1]{[ #1]}
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\eps}{\varepsilon}
\newcommand<>{\red}[1]{{\color#2{red} #1}}
\newcommand<>{\blue}[1]{{\color#2{blue} #1}}
\newcommand<>{\green}[1]{{\color#2[rgb]{0,0.5,0} #1}}

\newcommand{\ft}[1]{\frametitle{\textsc{#1}}}
\newcommand{\fst}[1]{\framesubtitle{\textsc{#1}}}

\newtheorem{proposition}{Proposition}

\defbeamertemplate*{title page}{customized}[1][]
{
  \usebeamerfont{title}\inserttitle\par
  \bigskip
  \usebeamerfont{subtitle}\insertsubtitle\par
  \bigskip
  \bigskip
  \usebeamerfont{author}\blue{\insertauthor}\par
  \usebeamerfont{institute}\blue{\insertinstitute}\par
  \usebeamerfont{date}\blue{\insertdate}\par
}
\beamertemplatenavigationsymbolsempty

\title{Pseudorandom Generator \\
From Average-Case Hardness}
\subtitle{Nisan-Wigderson Construction}
\author{Talk given by Xuangui Huang}
\date{January 23, 2015}

\begin{document}
\frame{\titlepage}
\begin{frame}
	\ft{Background}
	\fst{Derandomization}
	\begin{itemize}
		\item For distributed algorithms and cryptography, \blue{randomization} is \red<2->{proven} to be necessary to achieve certain tasks or achieve them efficiently.
		\item<2-> It's natural to conjecture that at least for some problems, \blue{randomization} is \red{inherently} necessary.
		\bigskip
		\item<3-> However, \red{under very reasonable complexity assumptions}, there is in fact a way to \blue{derandomization} \red{every} probabilistic algorithm of $\mathbf{BPP}$ with only polynomial overhead, thus making $\mathbf{BPP} = \mathbf{P}$.
		\begin{itemize}
			\item<4-> Albert Einstein: ``God does not play dice with the universe."
		\end{itemize}
	\end{itemize} 
\end{frame}

\begin{frame}
	\ft{Pseudorandom Generators}
	The main tools we use to derandomize probabilistic algorithms are \blue{pseudorandom generators}. \pause
	\begin{definition}[Pseudorandom Distribution]
		A distribution $R$ over $\{0,1\}^m$ is \blue{$(S,\epsilon)$-pseudorandom} for some $S \in \mathbb{N}$ and $\epsilon > 0$ if for every circuit $C$ of size at most $S$ \[\vert \Pr_{x \in_{R} \{0,1\}^m}[C(x) = 1] - \Pr_{x \in_{U_m} \{0,1\}^m}[C(x)=1]\vert < \epsilon \]
		where $U_m$ denotes the uniform distribution over $\{0,1\}^m$.
	\end{definition}
\end{frame}

\begin{frame}
	\ft{Pseudorandom Generators}
	\begin{definition}[Pseudorandom Generators]
		Let $S: \mathbb{N} \rightarrow \mathbb{N}$ be some \red{time-constructible} and \red{non-decreasing} function. A \red{$2^n$-time} computable function $G: \{0,1\}^* \rightarrow \{0,1\}^*$ is an \blue{$S(\ell)$-pseudorandom generator} if:
		\begin{itemize}
			\item $|G(z)| = S(|z|)$ for every $z \in \{0,1\}^*$
			\item for every $\ell \in \mathbb{N}$ the distribution $G(U_{\ell})$ is $(S(\ell)^{\green{3}}, \green{\frac{1}{10}})$-pseudorandom.
		\end{itemize} \pause
		\centering\includegraphics[scale=0.2]{pseudorandom-generator.png}
	\end{definition}
\end{frame}

\begin{frame}
	\ft{Pseudorandom Generators}
	\fst{From Average-Case Hardness}
	\pause
	\begin{definition}[$\rho$-Average Case Hardness]
		For $f: \{0,1\}^* \rightarrow \{0,1\}$ and $\rho \in [0,1]$, we define the \blue{$\rho$-average case hardness} of $f$ as 
		\begin{align*}
			\mathtt{H_{avg}^{\rho}}(f)(n) &\triangleq \max\{S|\Pr_{x \in_U \{0,1\}^n}[C(x) = f_n(x)] < \rho, \forall C \text{ with } |C| \leq S\} \\
			&= \min\{S|\Pr_{x \in_U \{0,1\}^n}[C(x) = f_n(x)] \geq \rho, \exists C \text{ with } |C| \leq S\} - 1.
		\end{align*}
	\end{definition}
	\begin{definition}[Average-Case Hardness]
		For $f: \{0,1\}^* \rightarrow \{0,1\}$, we define the \blue{average-case hardness} of $f$ as 
		\[\mathtt{H_{avg}}(f)(n) = \max\{S|\mathtt{H_{avg}^{\red{\frac{1}{2}+\frac{1}{S}}}}(f)(n) \geq S \} .\]
	\end{definition}
\end{frame}

\begin{frame}
	\ft{Pseudorandom Generators}
	\fst{From Average-Case Hardness}
	\begin{theorem}[PRGs From Average-Case Hardness]
		Let $S: \mathbb{N} \rightarrow \mathbb{N}$ be \red{time-constructible} and \red{non-decreasing}. If there exists $f \in \mathbf{DTIME}(\red{2^{O(n)}})$ such that $\mathtt{H_{avg}}(f)(n) \geq S(n)$ for every $n$, then there exists an 
		\only<1>{\green{$S(\delta \ell)^{\delta}$}-pseudorandom generator for some constant $\delta > 0$.}
		\only<2>{\green{$S'(\ell)$}-pseudorandom generator where \green{$S'(\ell) = S(n)^{\epsilon}$} for some constant $\epsilon > 0$ and $n$ satisfying \green{$n \geq \epsilon\sqrt{\ell\log S(n)}$} .}
	\end{theorem}
\end{frame}

\begin{frame}
	\ft{Toy Examples}
	\begin{itemize}
		\item We will start with two ``toy examples", showing how to use a hard function to construct a pseudorandom generator whose output is only \red{one bit longer} than its input, \only<2->{and then \red{two bits}.}
		\item<3-> These constructions can give us insight into the connection between hardness and (pseudo-)randomness.
		\item<3-> The main tool we are going to use to prove the correctness of our constructions is Yao's theorem.
	\end{itemize}
\end{frame}

\begin{frame}
	\ft{Unpredictability Implies Pseudorandomness}
	\begin{theorem}[Yao's Theorem]
		Let $Y$ be a distribution over $\{0,1\}^m$, $S > 10m$ and $\epsilon > 0$. If for every circuit $C$ of size at most \red{$2S$} and every $i \in [m]$ we have
		\[ \Pr_{R \in_Y \{0,1\}^m}[C(R_1, \cdots, R_{i-1})=R_i] \leq \red{\frac{1}{2} + \frac{\epsilon}{m}},\]
		then $Y$ is $(S, \epsilon)$-pseudorandom.
	\end{theorem}
\end{frame}

\begin{frame}
	\ft{Stretching By One Bit}
	\begin{theorem}[One-Bit Generator]
		Suppose that there exists $f \in \mathbf{DTIME}(2^{O(n)})$ with $\mathtt{H_{avg}}(f)(n) \geq \green{n^4}$, then there exists an $(\ell+1)$-pseudorandom generator $G$.
	\end{theorem}
	\pause
	\begin{proof}
		\begin{itemize}
			\item We set $G(z) = z \circ f(z)$ for every $z \in \{0,1\}^\ell$.
			\item<3-> To show $G(U_{\ell})$ is $((\ell+1)^3, \frac{1}{10})$-pseudorandom by contradiction using Yao's theorem, suppose there exists a circuit $C$ of size $\red<3>{2(\ell+1)^3} < \ell^4$ and a number \only<3>{$i \in [\ell+1]$}\only<4->{\red{$i = \ell + 1$}} such that 
				\only<3-4>{\[\Pr_{R=G(U_{\ell})}[C(R_1, \cdots, R_{i-1}) = R_i] > \frac{1}{2} + \frac{1}{10(\ell+1)}.\]}
				\only<5>{\[\Pr_{\red{R \in_U \{0,1\}^{\ell}}}[\red{C(R) = f(R)}] > \frac{1}{2} + \frac{1}{10(\ell+1)} > \frac{1}{2} + \frac{1}{\ell^4} .\]}
		\end{itemize}
	\end{proof}
\end{frame}

\begin{frame}
	\ft{Stretching By Two Bit}
	\begin{theorem}[Two-Bit Generator]
		Suppose that there exists $f \in \mathbf{DTIME}(2^{O(n)})$ with $\mathtt{H_{avg}}(f)(n) \geq \green{n^4}$, then there exists an $(\ell+2)$-pseudorandom generator $G$.
	\end{theorem}
	\pause
	\begin{proof}
		\begin{itemize}
			\item We set $G(z) = z_1\cdots z_{\frac{\ell}{2}} \circ f(z_1,\cdots,z_{\frac{\ell}{2}}) \circ z_{\frac{\ell}{2}+1}\cdots z_{\ell} \circ f(z_{\frac{\ell}{2}+1},\cdots,z_{\ell})$.
			\item<3-> For contradiction using Yao's theorem, suppose there exists a circuit $C$ of size $2(\ell+2)^3 < \ell^4$ and a number \only<3>{$i \in [\ell+2]$}\only<4->{\red{$i = \ell + 2$}} such that 
				\only<3-4>{\[\Pr_{R=G(U_{\ell})}[C(R_1, \cdots, R_{i-1}) = R_i] > \frac{1}{2} + \frac{1}{10(\ell+1)}.\]}
				\only<5>{\[\Pr_{\red{R,R' \in_U \{0,1\}^{\frac{\ell}{2}}}}[\red{C(R \circ f(R) \circ R') = f(R')}] > \frac{1}{2} + \frac{1}{10(\ell+1)} .\]}
		\end{itemize}
	\end{proof}
\end{frame}
\begin{frame}
	\ft{The Averaging Principle}
	\begin{proposition}[The Averaging Principle]
		If $A$ is some event depending on two \red{independent} random variables $X$, $Y$, then there exists some $x$ in the range of $X$ such that
		\[\Pr_Y[A(x,Y)] \geq \Pr_{X,Y}[A(X,Y)].\]
	\end{proposition}
\end{frame}
\begin{frame}
	\ft{Stretching By Two Bit}
	\begin{theorem}[Two-Bit Generator]
		Suppose that there exists $f \in \mathbf{DTIME}(2^{O(n)})$ with $\mathtt{H_{avg}}(f)(n) \geq \green{n^4}$, then there exists an $(\ell+2)$-pseudorandom generator $G$.
	\end{theorem}
	\begin{proof}
		\begin{itemize}
			\item We set $G(z) = z_1\cdots z_{\frac{\ell}{2}} \circ f(z_1,\cdots,z_{\frac{\ell}{2}}) \circ z_{\frac{\ell}{2}+1}\cdots z_{\ell} \circ f(z_{\frac{\ell}{2}+1},\cdots,z_{\ell})$.
			\item \only<1>{For contradiction using Yao's theorem, suppose there exists a circuit $C$ of size $2(\ell+2)^3 < \ell^4$ and a number $i = \ell + 2$ such that}
			\only<2->{Then there exists a \red{string $r \in \{0,1\}^{\frac{\ell}{2}}$} }\only<3>{, there exists a circuit \red{$D$ of size $2(\ell+2)^3 < (\frac{\ell}{2})^4$} }\only<2->{such that}
				\only<1>{\[\Pr_{R,R' \in_U \{0,1\}^{\frac{\ell}{2}}}[C(R \circ f(R) \circ R') = f(R')] > \frac{1}{2} + \frac{1}{10(\ell+1)} .\]}
				\only<2>{\[\Pr_{\red{R' \in_U \{0,1\}^{\frac{\ell}{2}}}}[C(r \circ f(r) \circ R') = f(R')] > \frac{1}{2} + \frac{1}{10(\ell+1)} .\]}
				\only<3>{\[\Pr_{R' \in_U \{0,1\}^{\frac{\ell}{2}}}[\red{D(R') = f(R')}] > \frac{1}{2} + \frac{1}{10(\ell+1)} > \frac{1}{2}+ \frac{1}{(\frac{\ell}{2})^4}.\]}
		\end{itemize}
	\end{proof}
\end{frame}
\begin{frame}
	\ft{Beyond Two Bits}
	\begin{itemize}
		\item The previous constructions of generators can be generalized into 
		\[G(z_1, \cdots, z_{\ell}) = z^1 \circ f(z^1) \circ z^2 \circ f(z^2) \cdots \circ z^k \circ f(z^k),\] obtaining an $(\ell+k)$-pseudorandom generator for a constant $k$. \pause
		\item The new idea is that we will take $z^1, \cdots, z^k$ to be \red{partly dependent} by using \blue{combinatorial designs}.
		\pause
		\item This will allow us to take $k$ so large that we can drop the inputs from the generator's output and use only $f(z^1)\circ f(z^2)\cdots \circ f(z^k)$.
	\end{itemize}
\end{frame}

\begin{frame}
	\ft{The Nisan-Wigderson Construction}
	\begin{definition}[NW Generator]
		Let $\mathcal{I} = \{I_1, \cdots, I_{\red{m}}\}$ be a family of subsets of $[\ell]$ with \red{$|I_j| = n$} for every $j$, and let $f:\{0,1\}^{\red{n}} \rightarrow \{0,1\}$. The \blue{$(\mathcal{I},f)$-NW generator} is the function $\mathtt{NW}^f_{\mathcal{I}} : \{0,1\}^{\ell} \rightarrow \{0,1\}^{\red{m}}$ defined as 
		\[\mathtt{NW}^f_{\mathcal{I}}(z) = f(z_{I_1})\circ f(z_{I_2}) \cdots \circ f(z_{I_m}),\]
		where for $z \in \{0,1\}^{\ell}$ and $I \subseteq [\ell]$, $z_I$ denotes the restriction of $z$ to the coordinates in $I$. 
	\end{definition}
	\pause
	\centering \includegraphics[scale=0.2]{NW-generator.png}
\end{frame}

\begin{frame}
	\ft{The Nisan-Wigderson Construction}
	\fst{Combinatorial Designs}
	In order for the generator to produce pseudorandom outputs, the family of subsets used should come from the following \blue{combinatorial designs}:
	\begin{definition}[Combinatorial Designs]
		Let $d,n,\ell$ satisfy $\ell > n > d$. A family $\mathcal{I} = \{I_1, \cdots, I_m\}$ of subsets of $[\ell]$ is an \blue{$(\ell, n,d)$-design} if:
		\begin{itemize}
			\item $|I_j| =n$ for every $j$
			\item $|I_j \cap I_k| \leq d$ for every $j \neq k$.
		\end{itemize}
	\end{definition}
	\pause
	\begin{lemma}[Construction of Combinatorial Designs]
		There is an algorithm $A$ that on input $\langle \ell, d, n \rangle$ where $n > d$ and \red{$\ell > \frac{10n^2}{d}$}, runs for \red{$2^{O(\ell)}$} steps and outputs an $(\ell,n,d)$-design $\mathcal{I}$ containing \red{$2^{\frac{d}{10}}$} subsets of $[\ell]$. 
	\end{lemma}
	\pause
	\begin{itemize}
		\item Greedily search at each step, which won't get stuck using a probabilistic argument.
	\end{itemize}
\end{frame}

\begin{frame}
	\ft{The Nisan-Wigderson Construction}
	If $f$ is a hard function and $\mathcal{I}$ is a design with good parameters, then $\mathtt{NW}^f_{\mathcal{I}}(U_{\ell})$ is indeed a pseudorandom distribution by  the following lemma:
	\begin{lemma}[Pseudorandomness Using the NW Generator]
		If $\mathcal{I}$ is an $(\ell,n,d)$-design with $|\mathcal{I}|=2^{\frac{d}{10}}$ and $f:\{0,1\}^n \rightarrow \{0,1\}$ satisfies \red{$\mathtt{H_{avg}}(f) > 2^{2d}$}, then the distribution $\mathtt{NW}^f_{\mathcal{I}}(U_{\ell})$ is $(\frac{\mathtt{H_{avg}}(f)}{10}, \frac{1}{10})$-pseudorandom.
	\end{lemma}
\end{frame}

\begin{frame}
	\ft{The Nisan-Wigderson Construction}
	\begin{lemma}[Pseudorandomness Using the NW Generator]
		If $\mathcal{I}$ is an $(\ell,n,d)$-design with $|\mathcal{I}|=2^{\frac{d}{10}}$ and $f:\{0,1\}^n \rightarrow \{0,1\}$ satisfies \red{$\mathtt{H_{avg}}(f) = S > 2^{2d}$}, then the distribution $\mathtt{NW}^f_{\mathcal{I}}(U_{\ell})$ is $(\frac{S}{10}, \frac{1}{10})$-pseudorandom.
	\end{lemma}
	\begin{proof}
		For contradiction, suppose that there exists a circuit $C$ of size $\frac{S}{5}$ and a number $i \in [2^{\frac{d}{10}}]$ \only<1-3>{ such that}\only<4->{, let $Z_1$ and $Z_2$ be two \red{independent} random variables with \red{$Z_1 = Z_{I_i}$} and \red{$Z_2 = Z_{[\ell]\setminus I_i}$}}\only<5->{ then we have}\only<6->{ a string \red{$z_2 \in \{0,1\}^{\ell-n}$} }\only<5-7>{ such that}\only<8>{, and there exists a circuit $B$ of size \red{$2^{\frac{d}{10}}\cdot d2^d + \frac{S}{5} < S$} such that}
		\only<1-2>{\[\Pr_{R=\mathtt{NW}^f_{\mathcal{I}}(U_{\ell})}[C(R_1, \cdots, R_{i-1}) = R_i] > \frac{1}{2} + \frac{1}{10\cdot 2^{\frac{d}{10}}}.\]}
		\only<2>{\begin{definition}[NW Generator]
				\[\mathtt{NW}^f_{\mathcal{I}}(z) = f(z_{I_1})\circ f(z_{I_2}) \cdots \circ f(z_{I_m}),\]
	\end{definition}}
			\only<3-4>{\[\Pr_{\red{Z \in_{U_{\ell}} \{0,1\}^{\ell}}}[\red{C(f(Z_{I_1}), \cdots, f(Z_{I_{i-1}})) = f(Z_{I_i})}] > \frac{1}{2} + \frac{1}{10\cdot 2^{\frac{d}{10}}}.\]}
			\only<5>{\[\Pr_{\red{Z_1 \sim U_n, Z_2 \sim U_{\ell-n}} }[\red{C(f_1(Z_1,Z_2), \cdots, f_{i-1}(Z_1,Z_2)) = f(Z_1)}] > \frac{1}{2} + \frac{1}{10\cdot 2^{\frac{d}{10}}},\]
			where $f_j$ applies $f$ to the coordinates of $Z_1$ corresponding to $I_j \cap I_i$ and the coordinates of $Z_2$ corresponding to $I_j \setminus I_i$.}
			\only<6-7>{\[\Pr_{\red{Z_1 \sim U_n}} [C(f_1(Z_1,\red{z_2}), \cdots, f_{i-1}(Z_1,\red{z_2})) = f(Z_1)] > \frac{1}{2} + \frac{1}{10\cdot 2^{\frac{d}{10}}}.\]}
			\only<7>{For $j \neq i$, $|I_j \cap I_i| \leq d$, hence the function $Z_1 \longmapsto f_j(Z_1, z_2)$ can be computed by a \red{$d2^d$-sized circuit}.}
			\only<8>{\[\Pr_{Z_1 \sim U_n} [B(Z_1) = f(Z_1)] > \frac{1}{2} + \frac{1}{10\cdot 2^{\frac{d}{10}}} > \frac{1}{2} + \frac{1}{S}.\]}
	\end{proof}
\end{frame}
\begin{frame}
	\begin{lemma}[Construction of Combinatorial Designs]
		There is an algorithm $A$ that on input $\langle \ell, d, n \rangle$ where $n > d$ and \red{$\ell > \frac{10n^2}{d}$}, runs for \red{$2^{O(\ell)}$} steps and outputs an $(\ell,n,d)$-design $\mathcal{I}$ containing \red{$2^{\frac{d}{10}}$} subsets of $[\ell]$. 
	\end{lemma}

	\begin{lemma}[Pseudorandomness Using the NW Generator]
		If $\mathcal{I}$ is an $(\ell,n,d)$-design with $|\mathcal{I}|=2^{\frac{d}{10}}$ and $f:\{0,1\}^n \rightarrow \{0,1\}$ satisfies \red{$\mathtt{H_{avg}}(f) = S > 2^{2d}$}, then the distribution $\mathtt{NW}^f_{\mathcal{I}}(U_{\ell})$ is $(\frac{S}{10}, \frac{1}{10})$-pseudorandom.
	\end{lemma}

	\begin{theorem}[PRGs From Average-Case Hardness]
		Let $S: \mathbb{N} \rightarrow \mathbb{N}$ be \red{time-constructible} and \red{non-decreasing}. If there exists $f \in \mathbf{DTIME}(\red{2^{O(n)}})$ such that $\mathtt{H_{avg}}(f)(n) \geq S(n)$ for every $n$, then there exists an 
\green{$S'(\ell)$}-pseudorandom generator where \green{$S'(\ell) = S(n)^{\epsilon}$} for some constant $\epsilon > 0$ and $n$ satisfying \green{$n \geq \epsilon\sqrt{\ell\log S(n)}$} .
	\end{theorem}
\end{frame}

\begin{frame}[plain]
	\begin{theorem}[PRGs From Average-Case Hardness]
		Let $S: \mathbb{N} \rightarrow \mathbb{N}$ be \red{time-constructible} and \red{non-decreasing}. If there exists $f \in \mathbf{DTIME}(\red{2^{O(n)}})$ such that $\mathtt{H_{avg}}(f)(n) \geq S(n)$ for every $n$, then there exists an 
\green{$S'(\ell)$}-pseudorandom generator where \green{$S'(\ell) = S(n)^{\epsilon}$} for some constant $\epsilon > 0$ and $n$ satisfying \green{$n \geq \epsilon\sqrt{\ell\log S(n)}$} .
	\end{theorem}
	\begin{proof}
		On input $z \in \{0,1\}^{\ell}$, our generator will operates as follows:
		\begin{itemize}
			\item Set $n$ to be the \red{largest} number such that \red{$\ell > \frac{100n^2}{\log S(n)}$}. \only<1-3>{Then $\ell \leq \frac{100(n+1)^2}{\log S(n+1)} \leq \frac{200n^2}{\log S(n)}$, and hence \red{$n \geq \sqrt{\frac{\ell \log S(n)}{200}}$\only<3->{$\geq \frac{1}{40}\sqrt{\ell \log S(n)}$}}.}
			\item Set \red{$d = \frac{\log S(n)}{4}$}. 
			\only<2>{\begin{lemma}[Construction of Combinatorial Designs]
		There is an algorithm $A$ that on input $\langle \ell, d, n \rangle$ where $n > d$ and \red{$\ell > \frac{10n^2}{d}$}, runs for \red{$2^{O(\ell)}$} steps and outputs an $(\ell,n,d)$-design $\mathcal{I}$ containing \red{$2^{\frac{d}{10}}$} subsets of $[\ell]$. 
		\end{lemma}}
			\only<3->{
			\item Run the algorithm $A$ to obtain an $(\ell, n, d)$-design $\mathcal{I} = \{I_1, \cdots, I_{2^{\frac{d}{10}}}\}$.
			\item Output the $S(n)^{\frac{1}{40}} = 2^{\frac{d}{10}}$ bits of $\mathtt{NW}^f_{\mathcal{I}(z)}$.
			}
		\end{itemize}
		\only<4>{This generator makes $2^{\frac{d}{10}}$ invocations of $f$, taking a total $2^{O(n)+d}$ steps, which can be bounded by $2^\ell$ by reducing $n$ by a constant factor.}
		\only<5>{Moreover, $S(n) > S(n)^{\frac{1}{2}} = 2^{2d}$.}
		\only<6>{\begin{lemma}[Pseudorandomness Using the NW Generator]
		If $\mathcal{I}$ is an $(\ell,n,d)$-design with $|\mathcal{I}|=2^{\frac{d}{10}}$ and $f:\{0,1\}^n \rightarrow \{0,1\}$ satisfies \red{$\mathtt{H_{avg}}(f) = S > 2^{2d}$}, then the distribution $\mathtt{NW}^f_{\mathcal{I}}(U_{\ell})$ is $(\frac{S}{10}, \frac{1}{10})$-pseudorandom.
	\end{lemma}}
			\only<7>{$\frac{S(n)}{10} \geq S(n)^{\frac{1}{10}} > S(n)^{\frac{3}{40}} = [S'(\ell)]^3$.}
	\end{proof}
\end{frame}

\begin{frame}
\centering
\Large
Thank you! \\
Q \& A
\end{frame}
\end{document}

