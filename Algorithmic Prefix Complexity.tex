\documentclass{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}


\usepackage{beamerthemesplit}
\usetheme{Warsaw}

\newtheorem{rem}{Remark}
\newtheorem*{Thm}{Theorem}
\newtheorem*{Def}{Definition}
\newtheorem*{Fact}{Fact}
\newtheorem*{Claim}{Claim}
\newtheorem*{Cor}{Corollary}
\newtheorem*{Conj}{Conjecture}
\newtheorem*{Lem}{Lemma}
\newtheorem*{Prop}{Proposition}
\newtheorem{Prob}{Problem}
\newtheorem*{Exam}{Example}
\newtheorem{Rem}{Remark}
\newtheorem*{Not}{Notation}
\newtheorem*{assumption}{Assumption}

% MATH -----------------------------------------------------------
\newcommand{\bra}[1]{\langle #1\rangle}
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\Real}{\mathbb R}
\newcommand{\eps}{\varepsilon}
\newcommand{\To}{\longrightarrow}
\newcommand{\BX}{\mathbf{B}(X)}
\newcommand{\A}{\mathcal{A}}
\newcommand{\CommentS}[1]{}

\newcommand{\la}{\leftarrow}
\newcommand{\La}{\Leftarrow}
\newcommand{\ra}{\rightarrow}
\newcommand{\Ra}{\Rightarrow}
\newcommand{\cP}{\cal{P}}

\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}

\title{An Introduction to Algorithmic Prefix Complexity}
\author{Xuangui Huang}
\institute{Shanghai Jiao Tong University}
\date{\today}

\begin{document}
\frame{\titlepage}
\section*{Outline}
\begin{frame}
    \frametitle{Outline}
    \tableofcontents
\end{frame}

\section{Motivation}
\begin{frame}
    \frametitle{Motivation}
    We have seen the `Algorithmic Plain Complexity' last week. However, 
    \begin{itemize}
        \item<1-2> it would be more pleasing if the complexity of $xy$ were never less than that of $x$;
        \item<1-> it is natural to restrict effective descriptions to prefix-codes.
    \end{itemize}
    \pause
    Hence, `Algorithmic Prefix Complexity'!
    \pause
\end{frame}

\section{Partial Recursive Prefix Functions}
\begin{frame}
    \frametitle{Partial Recursive Prefix Functions}
    In plain complexity, we use `partial recursive function'. In prefix complexity, we use the following one:
    \pause
    \begin{Def}[3.1.1]
    A \textrm{partial recursive prefix function} $\phi \colon \{0,1\}^* \to \mathcal{N}$ is a partial recursive (p. r.) function s.t. \\
    if $p$ is a proper prefix of $q$, then at most one of $\phi(p)$ and $\phi(q)$ is defined.
    \end{Def}
\end{frame}

\begin{frame}
    \frametitle{Complexity}
    Thus we have the following `prefix' version of complexity:
    \begin{Def}[`prefix' version of 2.1.1]
    Any p. r.  prefix function $\psi$, together with $p$ and $y$, s.t. $\psi(\bra{y,p}) = x$, is a \textrm{description} of $x$. The \textrm{Complexity $C_{\psi}$ of $x$ conditional to $y$} is defined by 
    \[C_{\psi}(x \vert y) = \min \{l(p) \colon \psi(\bra{y,p}) = x \} ,\]
    and $C_{\psi}(x \vert y) = \infty$ if no such $p$ exists.
    \end{Def}
\end{frame}

\section{The Invariance Theorem}
\subsection{The Theorem}
\begin{frame}
    \frametitle{The Invariance Theorem}
    \begin{Thm}[3.1.1]
    There exists an additively optimal p. r. prefix function $\psi_0$ s.t. \\
        $\forall$ p. r. prefix function $\psi$, $\exists$ constant $c_{\psi}$, s.t. 
        \[\forall x, y \in \mathcal{N}, ~C_{\psi_0}(x \vert y) \leq C_{\psi}(x \vert y) + c_{\psi}.\]
    \end{Thm}
\end{frame}

\subsection{Proof Motivation}
\begin{frame}
    \frametitle{Proof Motivation}
    \begin{itemize}
        \item Similarly as before, we need to construct a universal p.r. prefix function, \\
        or, an effective enumeration of p.r. prefix functions. \\
        \pause
        \item Fix an effective enumeration of Turing Machines $T_1, T_2, \ldots$, \\
        we will effectively construct \emph{prefix machines} $T'_1, T'_2, \ldots$, \\
        which will computes all and only the p.r. prefix functions.
    \end{itemize}
\end{frame}

\subsection{The Construction}
\begin{frame}
    \frametitle{Property of our construction}
    \begin{itemize}
        \item For Turing Machine $T$, the prefix machine $T'$ we constructed will read the input from left to right in one direction.
        \pause
        \item Besides, its input is a potentially infinite binary input sequence $b_1b_2\ldots$.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Construction of $T'$ given $T$}
    The computation of $T'$ on input $b_1b_2\ldots$ is as follow:
    \begin{enumerate}
        \item[Step 1] Set $p \leftarrow \epsilon$.
        \item[Step 2] Dovetailing:
            \begin{itemize}
                \item Let $q_j$ is the $j$-th string of $\{0,1\}^*$
                \item Execute the following commands stage by stage:
                    \begin{itemize}
                        \item In stage $i$, simulate one step of the computation of $T(pq_j)$ for all $j \leq i$
                        \item If in the above simulation, there exists $T(pq_j) \downarrow$, choose the first $q_j$ as $q$, go to step 3
                    \end{itemize}
            \end{itemize}
        \item[Step 3]
            \begin{itemize}
                \item If $q = \epsilon$ Then output $T(p)$ and halt
                \item Else:
                    \begin{itemize}
                        \item read $b \leftarrow \text{ next input bit }$, set $p \leftarrow pb$
                        \item go to step 2
                    \end{itemize}
            \end{itemize}
    \end{enumerate}
\end{frame}
\subsection{Proof}
\begin{frame}
    \frametitle{Halting Inputs, or Programs}
    \begin{Def}
        For an input sequence $b_1b_2\ldots$, the \textrm{halting input}, or \textrm{program}, of $T'$ is its initial segment $b_1b_2\ldots b_m$ s.t. $T'$ halts after reading $b_m$ before reading $b_{m+1}$.
    \end{Def}
    \pause
    \begin{Fact}
        For a fixed input sequence $b_1b_2\ldots$, the program of $T'$ is unique. 
    \end{Fact}
\end{frame}
\begin{frame}
    \frametitle{Effective Enumeration of p. r. Prefix Functions}
    \begin{itemize}
        \item Hence, the programs of $T'_1, T'_2, \ldots$ induces an effective enumeration of p. r. prefix functions.
        \pause
        \item More precisely, with slight modification of the construction of $T'$, we can have machine $T''$ read finite input and compute exactly p. r. prefix function.
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Proof}
    \begin{Thm}[3.1.1]
    There exists an additively optimal p. r. prefix function $\psi_0$ s.t. \\
        $\forall$ p. r. prefix function $\psi$, $\exists$ constant $c_{\psi}$, s.t. 
        \[\forall x, y \in \mathcal{N}, ~C_{\psi_0}(x \vert y) \leq C_{\psi}(x \vert y) + c_{\psi}.\]
    \end{Thm}
    \pause
    Proof: \\
    Let $\psi_0$ be the p. r. prefix function computed by a universal prefix machine $U$ s.t. 
   \[\forall y,p \in \mathcal{N}, U(\bra{y,\bra{n,p}}) = T''_n(\bra{y,p}),\]
   Then 
   $C_{\psi_0}(x \vert y) \leq C_{\psi_n}(x \vert y) + (2l(n)+1)$
    \qed
\end{frame}

\section{Examples}
\begin{frame}
    \frametitle{Complexity}
Since $\forall \text{ additively optimal p. r. prefix functions } \psi, \psi'$, we have $\abs{C_{\psi}(x \vert y) - C_{\psi'}(x \vert y)} \leq c_{\psi, \psi'}$, we can fix one of them as the standard reference $\psi_0$.

    \pause
    \begin{Def}
        The \textrm{ prefix complexity of $x$ conditional to $y$} is \[K(x \vert y) = C_{\psi_0}(x \vert y).\]
        The \textrm{ (unconditional) prefix complexity of $x$} is \[K(x) = K(x \vert \epsilon).\]
    \end{Def}
\end{frame}
\begin{frame}
    \frametitle{Advantage of Just Decoding Prefix-codes}
    \begin{itemize}
        \item Define $K(x,y) = K(\bra{x,y})$
        \item Now we can directly concatenate two descriptions to describe $\bra{x,y}$, \pause therefore $K$ is \emph{subadditive}:
        \[K(x,y) \leq K(x) + K(y) + O(1) \]
        \pause
        \item Similarly, $K(xy) \leq K(x) + K(y) + O(1)$
    \end{itemize}
\end{frame}
\begin{frame}
    \frametitle{Examples}
    \begin{itemize}
        \item $\forall x, K(x) \leq C(x) + K(C(x)) + O(1)$
    \end{itemize}
    \pause
    Proof: \\
        If $p$ is a shortest program (on TM) for $x$ with $l(p) = C(x)$, \\
        and $q$ is a shortest program (on prefix machines) for $l(p)$ with $l(q) = K(l(p))$, \\
        then $qp$ is a program for $x$ on some prefix machine. \qed
\end{frame}
\begin{frame}
    \frametitle{Examples}
    \begin{itemize}
        \item $\forall x, K(x) \leq C(x) + K(C(x)) + O(1)$
        \item $\forall x,y, C(x \vert y) \leq K(x \vert y) \leq C(x \vert y) + 2\log C(x\vert y)$
    \end{itemize}
    \pause
    Proof: \\
        If $p$ is a shortest program (on TM) for $x$ conditional to $y$ with $l(p) = C(x \vert y)$, \\
        then $\overline{l(p)}p$ is a program for $p$ on some prefix machine. \qed
\end{frame}
\begin{frame}
    \frametitle{Examples}
    \begin{itemize}
        \item $\forall x, K(x) \leq C(x) + K(C(x)) + O(1)$
        \item $\forall x,y, C(x \vert y) \leq K(x \vert y) \leq C(x \vert y) + 2\log C(x\vert y)$
        \item $K(x) \leq K(x \vert l(x)) + K(l(x)) + O(1) \leq K(x \vert l(x)) + \log^* l(x) + l(l(x)) + l(l(l(x))) + \ldots + O(1)$
    \end{itemize}
\end{frame}

\begin{frame}
 \begin{center}
   Thank you!
 \end{center}
\end{frame}
\end{document}

