\documentclass[aspectratio=169]{beamer}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{enumerate}
\usepackage{geometry}
\usepackage{hyperref}
\usepackage{indentfirst}


%\usepackage{beamerthemesplit}
%\usepackage[orientation=landscape,size=custom,width=16,height=9,scale=0.5,debug]{beamerposter}
%\usetheme{Antibes}
\usecolortheme{spruce}

\newcommand{\ma}[1]{\ensuremath{#1}}

\newcommand{\T}{\ma{\mathsf{T}}}

\newcommand{\bra}[1]{\langle #1\rangle}
\newcommand{\sqbra}[1]{[ #1]}
\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
\newcommand{\abs}[1]{\left\vert#1\right\vert}
\newcommand{\set}[1]{\left\{#1\right\}}
\newcommand{\eps}{\varepsilon}
\newcommand<>{\red}[1]{{\color#2{red} #1}}
\newcommand<>{\blue}[1]{{\color#2{blue} #1}}
\newcommand<>{\green}[1]{{\color#2[rgb]{0,0.5,0} #1}}

\newcommand{\PP}{\mathbf{P_{/poly}}}
\newcommand{\AlgPP}{\mathbf{AlgP_{/poly}}}
\newcommand{\EXP}{\mathbf{EXP}}
\newcommand{\NEXP}{\mathbf{NEXP}}

\newcommand{\ft}[1]{\frametitle{\textsc{#1}}}
\newcommand{\fst}[1]{\framesubtitle{\textsc{#1}}}

\newtheorem{proposition}{Proposition}
\newtheorem{claim}{Claim}
\newtheorem{remark}{Remark}

%\defbeamertemplate*{title page}{customized}[1][]
%{
%  \usebeamerfont{title}\inserttitle\par
%  \bigskip
%  \usebeamerfont{subtitle}\insertsubtitle\par
%  \bigskip
%  \bigskip
%  \usebeamerfont{author}\blue{\insertauthor}\par
%  \usebeamerfont{institute}\blue{\insertinstitute}\par
%  \usebeamerfont{date}\blue{\insertdate}\par
%}
\beamertemplatenavigationsymbolsempty

\title{\textsc{Secret Sharings in AC$^0$}}
\subtitle{``Bounded Indistinguishability and the Complexity of Recovering Secrets" \\
by Bogdanov, Ishai, Viola, and Williamson}
\author{Talk given by Xuangui Huang}
\date{Dec. 5, 2017}

\begin{document}
\frame{\titlepage}
\begin{frame}
	\ft{Introduction}
	\begin{itemize}
	\item AC$^0$: constant-depth polynomial-size unbounded-fan-in boolean circuits
	\item ``Secret Sharing in AC$^0$": sharing \& reconstruction is in AC$^0$
	\pause
	\item $\oplus_n \not\in$ AC$^0$
	\item Shamir Secret Sharing is not in AC$^0$.
	\end{itemize}
\end{frame}

\begin{frame}
	\ft{Outline}
	\tableofcontents
\end{frame}

\section{Background Knowledge: Bounded-Indistinguishability \& Approximate Degree}
\begin{frame}
	\ft{Background Knowledge}
	\fst{Definitions}
	\begin{itemize}
	\item Two distributions $\mu$ and $\nu$ over $\Sigma^n$ are \blue{$k$-wise indistinguishable} if their projections to any $k$ symbols are identical
	\item[]
	\item A function $f\colon \Sigma^n \to \{0,1\}$ is \blue{$\epsilon$-fooled by $k$-wise indistinguishablity} if $f$ cannot distinguish with advantage $\epsilon$ between any two $k$-wise indistinguishable distributions $\mu$
	\item[]
	\item The \blue{$\epsilon$-approximate degree} of a function $f\colon \{0,1\}^n \to \{0,1\}$ is defined to be the smallest degree of $p \colon \{0,1\}^n \to \mathbb{R}$ s.t. $|f(x) - p(x)| \leq \epsilon$ for every $x \in \{0,1\}$. 
	\end{itemize}
\end{frame}

\begin{frame}
	\ft{Background Knowledge}
	\fst{Connection between Bounded-Indistinguishabiliy \& Approximate Degree}
	\begin{theorem}
	For every $n$, $k$, and $\epsilon \in (0,1)$, and $f\colon \{0,1\}^n \to \{0,1\}$, the following are equivalent:
	\begin{enumerate}
		\item $f$ is \red{not} $\epsilon$-fooled by $k$-wise indistinguishability;
		\item The $\epsilon/2$-approximate degree of $f$ is \red{bigger} than $k$.
	\end{enumerate}
	\end{theorem}
	\pause
	\begin{proof}[Proof Sketch]
	By duality of linear programming, \blue{2.} is equivalent to
	\begin{enumerate}
		\item[3.] There exists a ``\blue{dual-witness}'' function $g\colon \{0,1\}^n \to \mathbb{R}$ s.t.
		\begin{itemize}
			\item $\sum_{x} g(x)f(x) > \epsilon /2$,
			\item $\sum_x |g(x)| = 1$,
			\item $\sum_x g(x) \prod_{i \in S} x_i = 0$ for every $S \in {[n] \choose \leq k}$.				\end{itemize}
	\end{enumerate}
	\end{proof}
\end{frame}
\section{Secret Sharing in AC$^0$}
\begin{frame}
	\ft{Bit Secret Sharing over $\{0,1\}$}
	A pair of $k$-wise indistinguishable distributions $(\mu, \nu)$ on $\{0,1\}^n$, together with a function that \red{can} tell the two distributions apart with advantage $\epsilon$, can be viewed as a bit secret sharing scheme:
	\begin{itemize}
		\item shares of $0$ and $1$ are samples of $\mu$ and $\nu$, respectively
		\item $f$ is the reconstruction function,
		\pause
		\item any $k$ parties cannot recover the secret,
		\item $n$ parties can use $f$ to recover the secret with advantage $\epsilon$.
	\end{itemize}
\end{frame}

\begin{frame}
	\ft{Bit Secret Sharing over $\{0,1\}$}
	\centering
	\begin{tabular}{|c|c|c|c|}
	\hline
	& $k$ & Reconstruction Function & Reconstruction Advantage \\ \hline
	1) & $\Omega(\sqrt{\delta n})$ & \green{OR} & $1-\delta$ \\ \hline
	2) & $\Omega(n)$ & \red{Majority} & constant \\ \hline
	3) & $\Omega((n/ \log n)^{2/3})$ & \green{Element Distinctness} & constant \\ \hline
	4) & $\Omega(n^{1/3})$ & \green{AND$_{n^{1/3}} \circ $ OR$_{n^{2/3}}$} & $1$ \\ \hline
	5) & $\Omega(\sqrt{n})$ & \green{some AC$^0$ function} & $1$ \\ \hline
	\end{tabular}
	\pause
	\begin{theorem}[Sampling the Shares in AC$^0$]
		For schemes 1) - 4), there exists pairs of circuit families of constant depth and size polynomial in $n$ and $\log (1/\epsilon)$ that sample distributions within statistical distance $\epsilon$ of $\mu$ and $\nu$.
	\end{theorem}
	\begin{proof}[Proof Sketch]
		Use the explicit constructions of dual-witnesses $g$.
	\end{proof}
\end{frame}

\section{Improvement of Secrecy \& Reconstruction Parameter}
\begin{frame}
	\ft{Trading Alphabet Size for Secrecy $k$}
	\fst{A General Method to Compose Secret Sharing Schemes}
	\begin{definition}
	A $n \times m$ bipartite graph $G$ with left degree $d$ is a \blue{$(k, \epsilon)$ disperser} if ant subset of $[n]$ of size $k$ has at least $(1-\epsilon)n$ neighbor in $[m]$. 
	\end{definition}
	\pause	
	
	\blue{Construction} of secret sharing scheme R over $\{0,1\}^n$ given an $(n,k)$ secret sharing scheme L over $\{0,1\}$:
	\begin{itemize}
		\item parties of L and R are associated to the left and right vertices;
		\item To share a bit in R, sample shares $s(\ell)\in \{0,1\}$ for $\ell \in L$;
		\item For each of the $d$ edges $e_1, \dots, e_d$ incident to $\ell$, choose a bit $s(e_i)$ at random conditioned on $s(e_1) \oplus \cdots \oplus s(e_d) = s(\ell)$;
		\item Then the share $s(r)$ for $r \in R$ is concatenation of the edge-shares $s(e)$ over its incident edges $e$.
	\end{itemize}
	To reconstruct, do it in reverse.		
\end{frame}
\begin{frame}
	\ft{Trading Alphabet Size for Secrecy $k$}
	\begin{lemma}
		If $G$ is a $(k, \epsilon)$ disperser graph and $L$ is a $(n,k)$ secret sharing scheme then R is a $(m, (1-\epsilon) m)$ secret sharing scheme with the same reconstruction advantage. 
	\end{lemma}
	Using disperser of $d = O(\log n/\epsilon)$ and $k = n^{1/3}$, and scheme 4) we get a good secrete sharing scheme over $\{0,1\}^n$ in AC$^0$.
\end{frame}

\begin{frame}
	\ft{Reducing the Number of Parties for Reconstruction}
	Using a different disperser, we can prove
	\begin{theorem}
		For every pair of constants $0\leq \sigma < \rho \leq 1$ and sufficient large $n$ there exists a $(m, \sigma m, \rho m)$ bit secret sharing over $\{0,1\}^{poly(n)}$ in AC$^0$ with advantage exponentially close to $1$.
	\end{theorem}
\end{frame}
\section{Limitation}
\begin{frame}
	\ft{Negative Results}
	\begin{itemize}
	\item Upper bound of the approximate-degree of AC$^0$ means upper bound of secrecy of sceret sharing with AC$^0$ reconstruction.
	\end{itemize}
	\begin{claim}
		Every function $f\colon\{0,1\}^n \to \{0,1\}$ that has size $s$ depth $d$ circuit has $n^{-h/2}$-approximate degree $n-h$ for $h = \Omega(n/poly \log n)$.
	\end{claim}
	\begin{proof}[Proof Sketch]
		Remove monomials of degree $>n-h$. The Fourier coefficients $\hat{F}(S)$ is small.
	\end{proof}
\end{frame}



\begin{frame}
\centering
\Large
\textsc{Thank You!} \\
\textsc{Q \& A}
\end{frame}
\end{document}

